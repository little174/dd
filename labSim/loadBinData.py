import pandas
import glob
import os
from pathlib import Path
root = 'binData'


def importBinDataOnDB(file_path, checkLog = True):
    print(f'start loading {file_path}')

    eventlog = load_from_storage(file_path)
    eventlog = eventlog.sort_values(["case_id", "event_id"], ascending=(True, True))
    if checkLog:
        num_cases = len(eventlog.case_id.unique().tolist())
        num_events = len(eventlog.event_id.to_list())
        print("Number of events: {}\nNumber of cases: {}".format(num_events, num_cases))

        print(list(eventlog))
        uniqueEvents = eventlog.event_id.unique().tolist()
        print(f'unique events are {len(uniqueEvents)}')
        print('info')
        assert(len(uniqueEvents) == num_events)
    return eventlog

def importBinData(file_path, checkLog = False):
    print(f'start loading {file_path}')
    # on notebook, uncomment
    #eventlog = load_from_storage(file_path)
    eventlog = pandas.read_csv(file_path, sep=',', skipinitialspace=True)
    #eventlog = eventlog.sort_values(["case_id", "event_id"], ascending=(True, True))
    if checkLog:
        num_cases = len(eventlog.case_id.unique().tolist())
        num_events = len(eventlog.event_id.to_list())
        print("Number of events: {}\nNumber of cases: {}".format(num_events, num_cases))

        print(list(eventlog))
        uniqueEvents = eventlog.event_id.unique().tolist()
        print(f'unique events are {len(uniqueEvents)}')

    return eventlog

def mapping_row_process(row, mapping):
    date = row.date
    id = row.id
    mapping[id] = date

def importMapping(file_path):
    eventlog = pandas.read_csv(file_path, sep=',', skipinitialspace=True)
    return  eventlog

def importMappingOnDB(file_path):
    eventlog = load_from_storage(file_path)
    return  eventlog

# Get a list of all the file paths that ends with .txt from in specified directory
def loadDataOnDB():

  job_bin = {}
  log = importMapping('mapping')
  mapping = {}
  log.apply(mapping_row_process, mapping=mapping, axis=1)
  #print(mapping)
  fileNames = log.id.values.tolist()
  for file in fileNames:
    #print(file)
    job_bin[mapping[file]] = importBinData(str(file))
  return job_bin

# Get a list of all the file paths that ends with .txt from in specified directory

def loadData():
    parentDirectory = os.getcwd()
    files = os.path.join(parentDirectory, root, '*.csv')
    filePaths = glob.glob(files)

    job_bin = {}
    mapping,_ = load_mapping()

    for filePath in filePaths:
        if 'mapping.csv' in filePath:
            continue
        else:
            print(filePath)
            head, tail = os.path.split(filePath)
            x = int(Path(filePath).stem)
            job_bin[mapping[x]] = importBinData(file_path=filePath)
            #print(job_bin[mapping[x]].info())

    print(mapping)
    return job_bin


def load_mapping():
    parentDirectory = os.getcwd()
    files = os.path.join(parentDirectory, root, 'mapping.csv')
    filePaths = glob.glob(files)

    mapping = {}
    for filePath in filePaths:
        if 'mapping.csv' in filePath:
            log = importMapping(filePath)
            log.apply(mapping_row_process, mapping=mapping, axis=1)
    return mapping, log

# Iterate over the list of filepaths & remove each file.
