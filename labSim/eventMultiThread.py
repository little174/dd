from sampleModel import *
import random
import os
import time
import glob
from pm4py.visualization.petri_net.variants.token_decoration_frequency import *
from pm4py.visualization.petri_net import visualizer as pn_visualizer

from pm4py.util import xes_constants

import threading

startDayOffset = 0
case_id_key = xes_constants.DEFAULT_TRACEID_KEY
activity_key = xes_constants.DEFAULT_NAME_KEY
timestamp_key = datetime.now()

seconds_per_hour = 60 * 60

shop_start_time = 7 * seconds_per_hour
shop_end_time = 18 * seconds_per_hour
day_in_seconds = seconds_per_hour * 24
useBusinessHours = False
onLocal = False
useFakeDuration = True
useDensity = False
time_bin_type = collections.namedtuple('time_bin', 'start finish')
root = 'binData'
allResults = {}
print('curernt', os.getcwd())
parentDirectory = os.getcwd()

filePath = os.path.join(parentDirectory, root, '*.csv')

# Get a list of all the file paths that ends with .txt from in specified directory
fileList = glob.glob(filePath)
# Iterate over the list of filepaths & remove each file.
for filePath in fileList:
    try:
        os.remove(filePath)
    except:
        print("Error while deleting file : ", filePath)





fig_counter = 0
frames = []


def save_marking(net, initial_marking, final_marking):
    global fig_counter
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    file = 'marking_' + str(fig_counter) + '.png'
    fig_counter = fig_counter + 1
    # pn_visualizer.save(gviz, file)
    frames.append(gviz)


from animate import *


def show_frames(nbFrames = 5):
    for idx, frame in enumerate(frames):
        matplotlib_view(frame)
        if idx >= nbFrames:
            return



def generate_event_history( index, start_time, netToUse, results, data:history_data):
    case_event_id_generator = getEventId(int(index * 1e3))

    rework_counter = 0
    net, initial_marking, tokenMarking, metaData = netToUse(data)
    caseDurationInfo = metaData[durationInfo]
    selectedCategory = data.speciality
    #selectedCategory = 'a'
    final_marking = Marking()
    # initial_marking[p_2] = 1
    reworkCountDict = {}

    # gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    # pn_visualizer.view(gviz)
    marking = copy(initial_marking)
    # gviz = pn_visualizer.apply(net, marking, final_marking)
    # pn_visualizer.view(gviz)
    case_num = 'Case %d' % index

    counter = 0
    save_marking(net, marking, final_marking)
    isComplete = True
    subtrails = []
    batchFilter = LabData.batchSetHistory(data.isTwoHour)
    while True:
        if not semantics.enabled_transitions(net, marking):
            break
        all_enabled_trans = semantics.enabled_transitions(net, marking)
        debug_output(f'{all_enabled_trans}')
        all_enabled_trans = list(all_enabled_trans)
        random.shuffle(all_enabled_trans)
        # check if we hit a rework upper bound
        boundedTrans = []

        # turn this on if you want to animate concurrenly, even off, concurrent will still happen, just not in animation
        concurrent = False
        if concurrent:
            for trans in all_enabled_trans:
                # dont do any work trans if it meets upper bound
                if trans in reworkCountDict and reworkCountDict[trans] == metaData[reworkUppboundTag][trans]:
                    boundedTrans.append(trans)
            for trans in boundedTrans:
                all_enabled_trans.remove(trans)
        # now find paralell enabled transisiont
        # if t is in all endabled before an executation and also in after an enabled transsion, this can happen in paraleel
        # check if an OR transision tuple is here
        # if loop meets constraints, we may reove all transisions with incomplete trail
        if len(all_enabled_trans) == 0:
            isComplete = False
            break
        t = all_enabled_trans[0]
        for orTransision in metaData[multiChoiceTransTag]:
            trans = orTransision.transitions
            inEnabled = True
            for x in trans:
                if x not in all_enabled_trans:
                    inEnabled = False
                    break
            # choose one based on weights
            if inEnabled:
                v = random.choices(trans, weights=orTransision.weights, k=1)
                t = v[0]

        # t= all_enabled_trans[0]
        try_marking = copy(marking)
        debug_output('try execute')
        debug_output(f'{t}')
        paralellevents = []
        paralellevents.append(t)
        try_token_marking = copy_token_marking(tokenMarking)
        try_marking, try_token_marking = try_execute(t, net, try_marking, try_token_marking)

        while concurrent:
            # after executre t, find new enabled transistions
            still_enabled_trans = semantics.enabled_transitions(net, try_marking)
            still_enabled_trans = list(still_enabled_trans)
            debug_output('still enabled')
            debug_output(f'{still_enabled_trans}')
            if (len(still_enabled_trans) == 0):
                concurrent = False
                break
            hasParallel = False
            for x in still_enabled_trans:
                # if x,t both in orTransiions, we have a loop
                skipX = x == t
                if (not skipX):
                    for orTransition in metaData[multiChoiceTransTag]:
                        if x in orTransision.transitions and t in orTransition.transitions:
                            skipX = True
                            break
                if x in all_enabled_trans and (not skipX):
                    # take the first transition
                    paralellevents.append(x)
                    # try again

                    try_marking, try_token_marking = try_execute(x, net, try_marking, try_token_marking)
                    hasParallel = True
                    break
            concurrent = hasParallel
        # now exeute papraleel tranisions
        timeStamp = datetime.now()
        simEvents = []
        for t in paralellevents:
            if t.label in caseDurationInfo:
                duration = caseDurationInfo[t.label]
            else:
                duration = duration_function(counter)


            counter = counter + 1
            if (t in metaData[reworkTransTag]):
                if t not in reworkCountDict:
                    reworkCountDict[t] = 0
                reworkCountDict[t] = reworkCountDict[t] + 1
            marking, tokenMarking, newEvent = execute(t, net, marking, tokenMarking, case_event_id_generator)

            if (newEvent.transition.name == 'tran5'):
                debug_output('im here')

            newEvent.set_duration(duration)
            birthTime = start_time
            for x in newEvent.get_parent():

                if x is None:
                    continue
                birthTime = max(birthTime, x.finish_time())

            newEvent.set_birth(birthTime)

            is_start = t in metaData[startEventTag]

            data = [case_num, selectedCategory, t.label, newEvent.birth, newEvent.finish_time(),
                    format_time(newEvent.birth), format_time(newEvent.finish_time()), newEvent.id, is_start]
            debug_output('new event paretns are')
            parentStr = []
            for x in newEvent.get_parent():

                if x is None:
                    continue
                parentStr.append(x.id)

            data.append(parentStr)

            # lets add batch

            if useFakeDuration:
                batchFilter = ['processing_1']
            data.append(batchFilter)
            data.append(False)
            subtrails.append(data)


            # trace.append(event)
            # all_traces[newEvent] = event
        # r = yield env.all_of(simEvents)
        save_marking(net, marking, final_marking)
    assert (isComplete)
    if isComplete:
        # print(subtrails[-1][1] )

        for data in subtrails:
            results.append(data)
    debug_output(f'genertated events are ')


def generate_event( index, start_time, day, shop_end_time, netToUse, results):
    case_event_id_generator = getEventId(int(index * 1e3))

    rework_counter = 0
    data = history_data(index=index, arrivalTime=int(start_time), isTwoHour=True, speciality='GI',
                        isAdditionalNeeded=False,
                        specimen_spec_type='COLBX',
                        specimen_sub_speciality='GI',
                        case_specimen_count=1,
                        case_cassette_count=1,
                        case_slide_count=1)
    net, initial_marking, tokenMarking, metaData = netToUse(data)
    caseDurationInfo = metaData[durationInfo]
    selectedCategory = metaData[durationTag].selectedCategory
    #selectedCategory = 'a'
    final_marking = Marking()
    # initial_marking[p_2] = 1
    reworkCountDict = {}

    # gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    # pn_visualizer.view(gviz)
    marking = copy(initial_marking)
    # gviz = pn_visualizer.apply(net, marking, final_marking)
    # pn_visualizer.view(gviz)
    case_num = 'Case %d' % index

    counter = 0
    save_marking(net, marking, final_marking)
    isComplete = True
    subtrails = []
    batchFilter = LabData.batchSet(selectedCategory)
    while True:
        if not semantics.enabled_transitions(net, marking):
            break
        all_enabled_trans = semantics.enabled_transitions(net, marking)
        debug_output(f'{all_enabled_trans}')
        all_enabled_trans = list(all_enabled_trans)
        random.shuffle(all_enabled_trans)
        # check if we hit a rework upper bound
        boundedTrans = []

        # turn this on if you want to animate concurrenly, even off, concurrent will still happen, just not in animation
        concurrent = False
        if concurrent:
            for trans in all_enabled_trans:
                # dont do any work trans if it meets upper bound
                if trans in reworkCountDict and reworkCountDict[trans] == metaData[reworkUppboundTag][trans]:
                    boundedTrans.append(trans)
            for trans in boundedTrans:
                all_enabled_trans.remove(trans)
        # now find paralell enabled transisiont
        # if t is in all endabled before an executation and also in after an enabled transsion, this can happen in paraleel
        # check if an OR transision tuple is here
        # if loop meets constraints, we may reove all transisions with incomplete trail
        if len(all_enabled_trans) == 0:
            isComplete = False
            break
        t = all_enabled_trans[0]
        for orTransision in metaData[multiChoiceTransTag]:
            trans = orTransision.transitions
            inEnabled = True
            for x in trans:
                if x not in all_enabled_trans:
                    inEnabled = False
                    break
            # choose one based on weights
            if inEnabled:
                v = random.choices(trans, weights=orTransision.weights, k=1)
                t = v[0]

        # t= all_enabled_trans[0]
        try_marking = copy(marking)
        debug_output('try execute')
        debug_output(f'{t}')
        paralellevents = []
        paralellevents.append(t)
        try_token_marking = copy_token_marking(tokenMarking)
        try_marking, try_token_marking = try_execute(t, net, try_marking, try_token_marking)

        while concurrent:
            # after executre t, find new enabled transistions
            still_enabled_trans = semantics.enabled_transitions(net, try_marking)
            still_enabled_trans = list(still_enabled_trans)
            debug_output('still enabled')
            debug_output(f'{still_enabled_trans}')
            if (len(still_enabled_trans) == 0):
                concurrent = False
                break
            hasParallel = False
            for x in still_enabled_trans:
                # if x,t both in orTransiions, we have a loop
                skipX = x == t
                if (not skipX):
                    for orTransition in metaData[multiChoiceTransTag]:
                        if x in orTransision.transitions and t in orTransition.transitions:
                            skipX = True
                            break
                if x in all_enabled_trans and (not skipX):
                    # take the first transition
                    paralellevents.append(x)
                    # try again

                    try_marking, try_token_marking = try_execute(x, net, try_marking, try_token_marking)
                    hasParallel = True
                    break
            concurrent = hasParallel
        # now exeute papraleel tranisions
        timeStamp = datetime.now()
        simEvents = []
        for t in paralellevents:
            if t.label in caseDurationInfo:
                duration = caseDurationInfo[t.label]
            else:
                duration = duration_function(counter)


            counter = counter + 1
            if (t in metaData[reworkTransTag]):
                if t not in reworkCountDict:
                    reworkCountDict[t] = 0
                reworkCountDict[t] = reworkCountDict[t] + 1
            marking, tokenMarking, newEvent = execute(t, net, marking, tokenMarking, case_event_id_generator)

            if (newEvent.transition.name == 'tran5'):
                debug_output('im here')
            if useFakeDuration:
                duration = 480
            newEvent.set_duration(duration)
            birthTime = start_time
            next_day_start = (day + 1) * day_in_seconds + shop_start_time
            for x in newEvent.get_parent():

                if x is None:
                    continue
                birthTime = max(birthTime, x.finish_time())
            # if birthTime > business hours, flow to next day
            if (useBusinessHours and birthTime >= shop_end_time):
                birthTime = max(birthTime, next_day_start)
            newEvent.set_birth(birthTime)

            is_start = t in metaData[startEventTag]

            data = [case_num, selectedCategory, t.label, newEvent.birth, newEvent.finish_time(),
                    format_time(newEvent.birth), format_time(newEvent.finish_time()), newEvent.id, is_start]
            debug_output('new event paretns are')
            parentStr = []
            for x in newEvent.get_parent():

                if x is None:
                    continue
                parentStr.append(x.id)

            data.append(parentStr)

            # lets add batch

            if useFakeDuration:
                batchFilter = [LabData.batch_filter[1]]
            data.append(batchFilter)
            subtrails.append(data)


            # trace.append(event)
            # all_traces[newEvent] = event
        # r = yield env.all_of(simEvents)
        save_marking(net, marking, final_marking)
    assert (isComplete)
    if isComplete:
        # print(subtrails[-1][1] )

        for data in subtrails:
            results.append(data)
    debug_output(f'genertated events are ')


def deterministicLog(env, netToUse, first_day, last_day, case_id_start, nbCasesPerDay):
    results = []
    source: LabData = LabData()
    counter = case_id_start
    nbCasesPerDay = 1

    #counter = 11
    shop_start_time = 23.65 * seconds_per_hour
    for day in range(first_day, last_day):
        results.clear()
        day_start = day * day_in_seconds + shop_start_time
        day_end = day * day_in_seconds + shop_end_time
        day_start = startDayOffset * day_in_seconds + shop_start_time
        day_end = startDayOffset * day_in_seconds + shop_end_time

        yield env.timeout(day_start - env.now)
        total = 0
        for i in range(nbCasesPerDay):
            next_arrival_time = 120
            if next_arrival_time is None:
                yield env.timeout(60)
                continue
            #print(f'start generate {format_time(day_start)} at {format_time(env.now)}')
            # print(f'get new one at {format_time(env.now)}')
            generate_event(index=counter, start_time=env.now, day=day, shop_end_time=day_end, netToUse=netToUse,
                           results=results)
            counter = counter + 1
            total += 1
            yield env.timeout(next_arrival_time)
            if (useBusinessHours and env.now >= day_end):
                break
        print(f'genrated {total} cases for day {day} at {format_time(env.now)}')
        if onLocal:
            write_log(day, results)
        allResults[day] = copy(results)


def createByHistory( netToUse, nbDays = 100):


    caseGettor = LabData.getNextCase()
    allCaseData = {}
    for i in caseGettor:
        data: history_data = i
        #print(data)
        index = data.index
        arrivalTime = data.arrivalTime
        #print(f'next call arrival at {format_time(arrivalTime)}')
        currentDay = arrivalTime // day_in_seconds

        if currentDay >= nbDays:
            break
        if currentDay not in allCaseData:
            allCaseData[currentDay] = []
            daystr = getDateTime(arrivalTime, '%Y-%m-%d')
            print(f'start generating at {daystr}')
        generate_event_history(index + 1, start_time=arrivalTime, netToUse=netToUse, results=allCaseData[currentDay], data=data)



    for day, results in allCaseData.items():
        if onLocal:
            write_log(day, results)
        allResults[day] = copy(results)
    return sorted(list(allCaseData.keys()))



def createByDensity( netToUse, nbDays = 100):
    job_arrival_dict = LabData.create_jobs(LabData.jobDistribution)
    caseGettor = LabData.create_fake_data_by_density(job_arrival_dict)
    allCaseData = {}
    c = 0
    for i in caseGettor:
        data: history_data = i
        #print(data)
        index = data.index
        arrivalTime = data.arrivalTime
        #print(f'next call arrival at {format_time(arrivalTime)}')
        currentDay = arrivalTime // day_in_seconds
        #print('current day', currentDay)
        if currentDay >= nbDays :
            break
        if currentDay not in allCaseData:
            allCaseData[currentDay] = []
            daystr = getDateTime(arrivalTime, '%Y-%m-%d')
            print(f'start generating at {daystr}')
        #if len(allCaseData[currentDay]) > 2:
        #    continue
        generate_event_history(index + 1, start_time=arrivalTime, netToUse=netToUse, results=allCaseData[currentDay], data=data)
        c += 1






    for day, results in allCaseData.items():
        if onLocal:
            write_log(day, results)
        allResults[day] = copy(results)
    return sorted(list(allCaseData.keys()))


def create(env, netToUse, first_day, last_day, case_id_start, nbCasesPerDay):
    results = []
    source: LabData = LabData()
    counter = case_id_start
    #shop_start_time = 16.95 * seconds_per_hour
    for day in range(first_day, last_day):
        results.clear()
        day_start = day * day_in_seconds + shop_start_time + startDayOffset * day_in_seconds
        day_end = day * day_in_seconds + shop_end_time+ startDayOffset * day_in_seconds

        yield env.timeout(day_start - env.now)
        total = 0

        while startDayOffset + day <= env.now // 60 // 60 // 24 < startDayOffset+ day + 1:
            # if it is saturday or sunday, no call arrival just pass time
            dayOfWeek = (env.now % (day_in_seconds * 7)) // day_in_seconds
            if dayOfWeek in (5,6):
                break
            next_arrival_time = source.get_next_arrive_time(env.now)
            if next_arrival_time is None:
                yield env.timeout(60)
                continue
            #print(f'start generate {format_time(day_start)} at {format_time(env.now)}')
            # print(f'get new one at {format_time(env.now)}')
            generate_event(env, counter, start_time=env.now, day=day, shop_end_time=day_end, netToUse=netToUse,
                           results=results)
            counter = counter + 1
            total += 1
            yield env.timeout(next_arrival_time)
            if (useBusinessHours and env.now >= day_end):
                break
        print(f'genrated {total} cases for day {day} at {format_time(env.now)}')
        if onLocal:
            write_log(day, results)
        allResults[day] = copy(results)


def write_log(day, results):
    filePath = os.path.join(root, str(day) + '.csv')
    f = open(filePath, 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        ['case_id', 'category','activity', 'start', 'finish', 'formated_start_time', 'formated_finish_time', 'event_id',
         'is_start', 'parents', 'batch_filter', 'done'])
    for row in results:
        thewriter.writerow(row)
    f.close()



def startProcessByCaseHistory(netToUse = None,isLocal=True, fakeDuration=False, nbDays = 100, useDensityInput_in = False):
    global useFakeDuration, onLocal, useDensityInput
    onLocal = isLocal
    useFakeDuration = fakeDuration
    useDensityInput = useDensityInput_in
    if useDensityInput:
        days = createByDensity(netToUse, nbDays)
    else:

        days = createByHistory(netToUse, nbDays)
    dateMapping = []
    for day in days:
        dateStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        dateMapping.append([day, dateStr])

    dateMapping = pd.DataFrame(dateMapping, columns=['id', 'date'])
    filePath = os.path.join(root, 'mapping.csv')
    if onLocal:
        dateMapping.to_csv(filePath, index=False)
    return dateMapping

def startProcessByDensity(netToUse = None,isLocal=True, fakeDuration=False, nbDays = 100):
    global useFakeDuration, onLocal
    onLocal = isLocal
    useFakeDuration = fakeDuration
    days = createByDensity(netToUse, nbDays)
    dateMapping = []
    for day in days:
        dateStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        dateMapping.append([day, dateStr])

    dateMapping = pd.DataFrame(dateMapping, columns=['id', 'date'])
    filePath = os.path.join(root, 'mapping.csv')
    if onLocal:
        dateMapping.to_csv(filePath, index=False)
    return dateMapping

def startProcess(netToUse = None, nbDays = 10, isLocal=True, fakeDuration = True, useDensityInput = False):
    global useFakeDuration, onLocal,useDensity

    useDensity = useDensityInput

    onLocal = isLocal
    useFakeDuration = fakeDuration
    subPros = []
    step = nbDays  # how many days in each process
    nbCasesPerDay = 1
    dateMapping = []
    nbWindows = 1
    totalDays = nbWindows * step
    for i in range(nbWindows):
        case_id_start = nbCasesPerDay * i * step + 1

        process = threading.Thread(target=createSim,
                                   args=(i * step, (i + 1) * step, case_id_start, nbCasesPerDay, netToUse))
        # run the process
        subPros.append(process)
        process.start()
        # wait for the process to finish
    print('Waiting for the process...')
    for p in subPros:
        p.join()
    #x = datetime(2021, 5, 17 + startDayOffset)
    for day in range(1):
        dateStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        dateMapping.append([day, dateStr])
    filePath = os.path.join(root, 'mapping.csv')
    dateMapping = pd.DataFrame(dateMapping, columns=['id', 'date'])
    if onLocal:
        dateMapping.to_csv(filePath, index=False)
    return dateMapping





# binData(results)

# show_frames()

# from netwG import  *
# G = nx.complete_graph(5)
# testDraw(G)

def combine_ouput(allResults):
    if onLocal == False: return
    data = []
    for day, results in allResults.items():
        for r in results:
            data.append(r)

    f = open('simulated-logs.csv', 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        ['case_id', 'activity', 'start', 'finish', 'formated_start_time', 'formated_finish_time', 'event_id',
         'is_start', 'parents'])
    for row in data:
        thewriter.writerow(row)
    f.close()

def check(netToUse):
    net, init, final, meta = netToUse()
    gviz = pn_visualizer.apply(net, init, Marking())
    pn_visualizer.view(gviz)

def createSim(first_day=0, last_day=1, case_id_start=0, nbCasesPerDay=10, netToUse=None):
    print(threading.current_thread().name)

    env = simpy.Environment()
    if useFakeDuration:
        env.process(deterministicLog(env, netToUse, first_day, last_day, case_id_start, nbCasesPerDay))
    if useDensity:
        createByDensity(netToUse, last_day)
    else:
        env.process(createByHistory(netToUse))
    start = time.time()

    env.run()
    end = time.time()

    print('time spent', end - start)

def RunMain(isLocal = False, fakeDuration = True,  nbDays = 3, useDensityInput_in = False):
    if not isLocal:
        return
    useFakeDuration = fakeDuration

    log = import_csv('caseArrivalData.csv')
    LabData.loadCaseArrivalData(log)

    LabData.setArrivalRate(504)
    labLog = import_csv('caseProfile.csv')

    LabData.loadSubcategory(labLog)
    arrivalLog = import_csv('arrival-hour.csv')
    LabData.loadArrivalHourRate(arrivalLog)
    roster = import_csv('roster.csv')
    LabData.loadRostering(roster)

    grossingTime = import_csv('grossingTime.csv')
    LabData.loadGrossingTime(grossingTime)

    workstationTime = import_csv('workstation-time.csv')
    LabData.loadWorkStationTime(workstationTime)
    LabData.setUseFixedData(fixed=fakeDuration)
    print(LabData.case_categories)
    netToUse = labProcess
    startProcessByCaseHistory(netToUse=netToUse, isLocal=isLocal, fakeDuration=fakeDuration, nbDays=nbDays, useDensityInput_in = useDensityInput_in)
    #startProcess(netToUse=netToUse, isLocal=isLocal, fakeDuration=fakeDuration, nbDays=nbDays, useDensityInput = useDensityInput)
    #net, initial_marking, tokenMarking, metaData = netToUse()
    #create_resource_file_with_skillset(metaData[resourceTag])
    # check(netToUse)
    combine_ouput(allResults)
    #show_frames()


def viewLabModel():
    subcategoryLog = import_csv('caseProfile-fake.csv')
    print(subcategoryLog.dtypes)
    LabData.loadSubcategory(subcategoryLog)
    arrivalLog = import_csv('arrival-hour.csv')
    print(arrivalLog.dtypes)
    LabData.loadArrivalHourRate(arrivalLog)

    roster = import_csv('roster.csv')
    print(roster.dtypes)
    LabData.loadRostering(roster)

    LabData.setUseFixedData(fixed=True)
    net, initial_marking, tokenMarking, metaData = labProcess()
    final = Marking()
    final['sink'] = 1
    gviz = pn_visualizer.apply(net, initial_marking, final)
    pt_visualizer.view(gviz)


#viewLabModel()

