# For backlog, all finished events assume start,finish ready are at the beging of that arrival day, they are meaningless
# for the next task to do of a backlog job, we assume its ready time is its arrival day..this can be changed with more info

from labEventDistribution import *
from SimulationObject import *

from loadBinData import *


class LabEvent(Event):
    def __init__(self, id=0, start=0, finish=0, parents=[], name='', case_id=0, is_start=False, batch_filter=[],
                 speciality='', isDone=False):
        super().__init__(id=id, start=start, finish=finish, parents=parents, name=name, case_id=case_id,
                         is_start=is_start, isDone=isDone)
        # actual is the event with id
        # name is the event name without id, representing the group, for exmaple, grossing_0 -> grossing (group)
        self.duration = finish - start
        # self.duration = 35
        self.actualName = name

        if strip_suffix(name) in event_to_resrouce:
            self.group = event_to_resrouce[strip_suffix(name)]
        # default no resource is needed
        else:
            self.group = noResNeeded

        self.batch_filter = batch_filter

        self.speciality = speciality

    def __str__(self):
        return f'{self.case_id}, activity = {self.actualName}, id={self.id} start = {format_time(self.start)} duration {self.duration / 60}'

    def is_in_batch(self, batchTag):

        for b in self.batch_filter:
            if b == batchTag:
                return True
        return False


class LabRosterSimulation(Simulation):


    def __init__(self, isLocalSim=True):
        super().__init__(isLocalSim)



    def event_row_process_new_cases(self, row, new_cases):
        case_id = row.case_id
        activity = row.activity
        start = row.start
        duration = row.finish - row.start
        finish = row.finish
        event_id = row.event_id
        speciality = row.category
        isDone = row.done

        event_parents = list()

        if not self.isLocalSim:
            res = row.parents
            batchFilter = row.batch_filter
        else:
            if fromdBRegression:
                a = row.parents.find('[')
                b = row.parents.find(']')
                x = row.parents[a + 1:b]
                l = x.split(' ')
                print(row.event_id, l)
                assert (False)
                if l == ['']:
                    res = []
                else:
                    res = [int(s) for s in l]

                a = row.batch_filter.find('[')
                b = row.batch_filter.find(']')
                x = row.batch_filter[a + 1:b]
                l = x.split(' ')
                batchFilter = []
                for v in l:
                    e = v.find("'")
                    k = v[e + 1:]
                    f = k.find("'")
                    m = k[:f]
                    batchFilter.append(m)

                print(res)
                print(batchFilter)
            else:
                res = ast.literal_eval(row.parents)
                batchFilter = ast.literal_eval(row.batch_filter)

        res = list(filter(None, res))
        batchFilter = list(filter(None, batchFilter))

        for value in res:
            event_parents.append(int(value))

        newEvent = LabEvent(event_id, start, finish, event_parents, activity, case_id, row.is_start, batchFilter,
                            speciality, isDone)
        assert (event_id not in self.allEvents)
        self.allEvents[event_id] = newEvent

        if row.case_id not in self.allCases:
            self.allCases[case_id] = []
            new_cases.append(case_id)
        if row.is_start:
            self.caseArrivals[case_id] = start
        self.allCases[case_id].append(newEvent)

    def monitorQueue(self, waitPeriod=0):
        # every hour + 0.1s, monitor the queue
        yield self.env.timeout(waitPeriod)

        # print(self.env.now, format_time(self.env.now - 0.1))
        for stage, res in self.priority_res.items():
            # print(stage,res)
            allRequests = (res.put_queue)

            jobInQueue = list(filter(lambda request: isinstance(request.event, NewJobEvent), allRequests))

            self.queueMonitor[stage].append([stage, format_time(self.env.now - 0.1), len(jobInQueue)])

        for stage, res in self.batch_res.items():
            allRequests = (res.put_queue)

            jobInQueue = list(filter(lambda request: isinstance(request.event, BatchJobEvent), allRequests))
            if stage not in self.queueMonitor:
                self.queueMonitor[stage] = []
            self.queueMonitor[stage].append([stage, format_time(self.env.now - 0.1), len(jobInQueue)])

    def create_batch_resource_single_break(self):
        # break this team by half

        self.batch_res = {}
        events = ['processing']
        members = [f'processing_{i}' for i in range(total_processing * 2)]
        firstTeam = members[0: len(members) // 2]
        secondTeam = members[len(members) // 2:]
        batchTeams = [firstTeam, secondTeam]
        batch_roster = LabData.batch_roster()
        df = rosterDF(batch_roster)
        print(df.info())
        for batchIdx, t in enumerate(batchTeams):
            tag = f'{t[0]}'
            # print(batch_roster[batchIdx])
            shift_start = LabData.batchTeamShift[batchIdx][0]
            shift_end = LabData.batchTeamShift[batchIdx][1]
            current_stage_workers = WorkerCollection(tag, shift_start, shift_end)
            capacity = len(t)

            for i in range(capacity):
                current_stage_workers.add(Worker(i, shift_start, shift_end, t[i]))
            self.batch_res[tag] = BatchPriorityResource(self.env, current_stage_workers)
            # lets try for a given break, just one break event is enough
            for i in range(1):
                worker: Worker = current_stage_workers[i]

                debug_output(f'worker is {worker} with info {batchIdx}   {batch_roster[batchIdx]}')
                staffInfo: StaffShift = batch_roster[batchIdx]

                # this is shifrt for the first cycle, move on to next cycle
                for currentCycle in range(self.totalCycles):
                    sortedTimekey = sorted(list(staffInfo.shifts.keys()))
                    for idx, timeKey in enumerate(sortedTimekey):
                        shift = staffInfo.shifts[timeKey]

                        debug_output(f'at {timeKey}  shift is {shift}')
                        day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                            shift)
                        temp = df.loc[df.Task == staffInfo.name]
                        originalShiftStartTime = datetime(year, month, day, math.floor(start_hour), start_min)
                        currentShiftStartTime = originalShiftStartTime + timedelta(
                            days=currentCycle * self.rosterCycleDays)

                        currentShiftEndTime = temp.loc[(temp.Start == originalShiftStartTime), 'Finish'].to_list()[
                                                  0] + timedelta(days=currentCycle * self.rosterCycleDays)

                        currentShiftStartTimeFromSimStartInSeconds = (
                                    currentShiftStartTime - SimSetting.start_time_sim).total_seconds()
                        currentShiftEndTimeFromSimStartInSeconds = (
                                    currentShiftEndTime - SimSetting.start_time_sim).total_seconds()
                        if (idx == 0 and currentCycle == 0):
                            currentDayInSeconds = (SimSetting.start_time_sim + timedelta(
                                days=currentCycle * self.rosterCycleDays) - SimSetting.start_time_sim).total_seconds()
                            durationBeforeStart = currentShiftStartTimeFromSimStartInSeconds - currentDayInSeconds
                            debug_output(f'start at {format_time(currentDayInSeconds + durationBeforeStart)} end at '
                                         f'{format_time(currentShiftEndTimeFromSimStartInSeconds)}')
                            self.env.process(self.staff_break_process(self.batch_res[tag], stage=tag,
                                                                      worker_id=i, start=currentDayInSeconds,
                                                                      duration=durationBeforeStart))

                        if idx < len(sortedTimekey) - 1:

                            nextShiftStart = staffInfo.shifts[sortedTimekey[idx + 1]]
                            day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                                nextShiftStart)
                            nextShiftStartTime = datetime(year, month, day, math.floor(start_hour), start_min)
                            nextShiftStartTimeFromSimStartInSeconds = (
                                                                              nextShiftStartTime - SimSetting.start_time_sim).total_seconds() + timedelta(
                                days=currentCycle * self.rosterCycleDays).total_seconds()
                            durationBeforeNextShiftStart = nextShiftStartTimeFromSimStartInSeconds - currentShiftEndTimeFromSimStartInSeconds
                            debug_output(
                                f'break at last shift at {format_time(currentShiftEndTimeFromSimStartInSeconds)} end at '
                                f'{format_time(currentShiftEndTimeFromSimStartInSeconds + durationBeforeNextShiftStart)}')
                            self.env.process(self.staff_break_process(self.batch_res[tag], stage=tag,
                                                                      worker_id=i,
                                                                      start=currentShiftEndTimeFromSimStartInSeconds,
                                                                      duration=durationBeforeNextShiftStart))
                        elif idx == len(sortedTimekey) - 1 and currentCycle < self.totalCycles - 1:
                            # go back to the beining
                            nextCycleFirstShift = staffInfo.shifts[sortedTimekey[0]]

                            day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                                nextCycleFirstShift)
                            nextCycleShiftStartTime = datetime(year, month, day, math.floor(start_hour),
                                                               start_min) + timedelta(
                                days=(currentCycle + 1) * self.rosterCycleDays)
                            debug_output(f'next cycle start at {nextCycleShiftStartTime}')
                            durationBeforeNextShiftStart = (
                                        nextCycleShiftStartTime - currentShiftEndTime).total_seconds()
                            debug_output(
                                f'break at last shift at {format_time(currentShiftEndTimeFromSimStartInSeconds)} end at '
                                f'{format_time(currentShiftEndTimeFromSimStartInSeconds + durationBeforeNextShiftStart)}')
                            self.env.process(self.staff_break_process(self.batch_res[tag], stage=tag,
                                                                      worker_id=i,
                                                                      start=currentShiftEndTimeFromSimStartInSeconds,
                                                                      duration=durationBeforeNextShiftStart))

            for event in events:
                if event not in self.eventResourceQuery:
                    self.eventResourceQuery[event] = {}
                self.eventResourceQuery[event][tag] = self.batch_res[tag]

    def create_batch_resource_new(self):
        # break this team by half
        weekDayList = self.rosterData['roster_week']
        # df['Finish'] = df['Finish'].map(lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%s').date())

        rosterCycleDays = len(weekDayList) * 7
        self.batch_res = {}
        events = ['processing']
        members = [f'processing_{i}' for i in range(total_processing * 2)]
        firstTeam = members[0: len(members) // 2]
        secondTeam = members[len(members) // 2:]
        batchTeams = [firstTeam, secondTeam]
        batch_roster = LabData.batch_roster()
        df = LabData.rosterDF(batch_roster)
        print(df.info())
        for batchIdx, t in enumerate(batchTeams):
            tag = f'{t[0]}'
            # print(batch_roster[batchIdx])
            shift_start = LabData.batchTeamShift[batchIdx][0]
            shift_end = LabData.batchTeamShift[batchIdx][1]
            current_stage_workers = WorkerCollection(tag, shift_start, shift_end)
            capacity = len(t)

            for i in range(capacity):
                current_stage_workers.add(Worker(i, shift_start, shift_end, t[i]))
            self.batch_res[tag] = BatchPriorityResource(self.env, current_stage_workers)

            for i in range(capacity):
                worker: Worker = current_stage_workers[i]

                debug_output(f'worker is {worker} with info {batchIdx}   {batch_roster[batchIdx]}')
                staffInfo: StaffShift = batch_roster[batchIdx]

                # this is shifrt for the first cycle, move on to next cycle
                for currentCycle in range(self.totalCycles):
                    sortedTimekey = sorted(list(staffInfo.shifts.keys()))
                    for idx, timeKey in enumerate(sortedTimekey):
                        shift = staffInfo.shifts[timeKey]

                        debug_output(f'at {timeKey}  shift is {shift}')
                        day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                            shift)
                        temp = df.loc[df.Task == staffInfo.name]
                        originalShiftStartTime = datetime(year, month, day, math.floor(start_hour), start_min)
                        currentShiftStartTime = originalShiftStartTime + timedelta(days=currentCycle * rosterCycleDays)

                        currentShiftEndTime = temp.loc[(temp.Start == originalShiftStartTime), 'Finish'].to_list()[
                                                  0] + timedelta(days=currentCycle * rosterCycleDays)

                        currentShiftStartTimeFromSimStartInSeconds = (
                                    currentShiftStartTime - SimSetting.start_time_sim).total_seconds()
                        currentShiftEndTimeFromSimStartInSeconds = (
                                    currentShiftEndTime - SimSetting.start_time_sim).total_seconds()
                        if (idx == 0 and currentCycle == 0):
                            currentDayInSeconds = (SimSetting.start_time_sim + timedelta(
                                days=currentCycle * rosterCycleDays) - SimSetting.start_time_sim).total_seconds()
                            durationBeforeStart = currentShiftStartTimeFromSimStartInSeconds - currentDayInSeconds
                            debug_output(f'start at {format_time(currentDayInSeconds + durationBeforeStart)} end at '
                                         f'{format_time(currentShiftEndTimeFromSimStartInSeconds)}')
                            self.env.process(self.staff_break_process(self.batch_res[tag], stage=tag,
                                                                      worker_id=i, start=currentDayInSeconds,
                                                                      duration=durationBeforeStart))

                        if idx < len(sortedTimekey) - 1:

                            nextShiftStart = staffInfo.shifts[sortedTimekey[idx + 1]]
                            day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                                nextShiftStart)
                            nextShiftStartTime = datetime(year, month, day, math.floor(start_hour), start_min)
                            nextShiftStartTimeFromSimStartInSeconds = (
                                                                              nextShiftStartTime - SimSetting.start_time_sim).total_seconds() + timedelta(
                                days=currentCycle * rosterCycleDays).total_seconds()
                            durationBeforeNextShiftStart = nextShiftStartTimeFromSimStartInSeconds - currentShiftEndTimeFromSimStartInSeconds
                            debug_output(
                                f'break at last shift at {format_time(currentShiftEndTimeFromSimStartInSeconds)} end at '
                                f'{format_time(currentShiftEndTimeFromSimStartInSeconds + durationBeforeNextShiftStart)}')
                            self.env.process(self.staff_break_process(self.batch_res[tag], stage=tag,
                                                                      worker_id=i,
                                                                      start=currentShiftEndTimeFromSimStartInSeconds,
                                                                      duration=durationBeforeNextShiftStart))
                        elif idx == len(sortedTimekey) - 1 and currentCycle < self.totalCycles - 1:
                            # go back to the beining
                            nextCycleFirstShift = staffInfo.shifts[sortedTimekey[0]]

                            day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                                nextCycleFirstShift)
                            nextCycleShiftStartTime = datetime(year, month, day, math.floor(start_hour),
                                                               start_min) + timedelta(
                                days=(currentCycle + 1) * rosterCycleDays)
                            debug_output(f'next cycle start at {nextCycleShiftStartTime}')
                            durationBeforeNextShiftStart = (
                                        nextCycleShiftStartTime - currentShiftEndTime).total_seconds()
                            debug_output(
                                f'break at last shift at {format_time(currentShiftEndTimeFromSimStartInSeconds)} end at '
                                f'{format_time(currentShiftEndTimeFromSimStartInSeconds + durationBeforeNextShiftStart)}')
                            self.env.process(self.staff_break_process(self.batch_res[tag], stage=tag,
                                                                      worker_id=i,
                                                                      start=currentShiftEndTimeFromSimStartInSeconds,
                                                                      duration=durationBeforeNextShiftStart))

            for event in events:
                if event not in self.eventResourceQuery:
                    self.eventResourceQuery[event] = {}
                self.eventResourceQuery[event][tag] = self.batch_res[tag]

    def singleResoruceProcess(self, eventsArray, dependentEventDict):

        allFinished = []
        for key, finishSignals in eventsArray.items():
            # to start reqeust for batching, all grouped events must be ready
            event: LabEvent = key
            if event.isDone:
                event.set_ready_time(self.env.now)
                event.set_sim_start(self.env.now)
                event.set_sim_end(self.env.now)
                event.set_resource('backlog')
                # immediately tell its children ready
                self.notifyChildren(dependentEventDict, event)
                continue
            for f in finishSignals:
                allFinished.append(f)
        if len(allFinished) > 0:
            # print(event)
            debug_output(f'try do single resoruce at {format_time(self.env.now)}')
            # for each node, all predessors need to finish before continue
            yield self.env.all_of(allFinished)

            # these events require the same resource worker, so just do them on by one then release the resoruce
            event = key

            events = []
            for e in eventsArray.keys():
                if e.isDone: continue
                events.append(e)
                e.set_ready_time(self.env.now)
            # events = [e for e in eventsArray.keys()]
            debug_output(f'single, parents are fnished')
            debug_output(f'want resource do this task {events[0]}   at time {format_time(self.env.now)}')

            priority = 0

            freeResource, freeResourceRequest, w = yield self.env.process(
                self.GetFreeRosterResource(event, priority))

            # self.overTimeRequest(totalDuation, self.env, resoruceTimeIndex, w)
            for event in events:
                debug_output(f' I can do this node {event} at time {format_time(self.env.now)} by {w}')
                event.set_resource(w.name)
                # event.set_resource_time_index(resoruceTimeIndex)
                event.set_sim_start(self.env.now)
                duration = event.duration
                yield self.env.timeout(duration)
                debug_output(f'finish this task {event} at time {format_time(self.env.now)}')
                event.set_sim_end(self.env.now)
                self.notifyChildren(dependentEventDict, event)

            freeResource.release(freeResourceRequest)

    # for a finished event, notify all its children ready to start
    def notifyChildren(self, dependentEventDict, event):
        for key, eventList in dependentEventDict.items():
            if (len(eventList) == 0): continue
            for parent in eventList:
                if event.id == parent[0]:
                    debug_output(f' for node {key}, the dependend event is succed {event}')
                    parent[1].succeed()

    def dayCycleIndex(self, env):
        # dayIndex in a roster cycle
        return int((self.env.now % self.rosterDayCycleInSeconds) // day_in_seconds)




    def batchProcess(self, eventsArray, dependentEventDict):

        allFinished = []
        for key, finishSignals in eventsArray.items():
            event: LabEvent = key
            if event.isDone:
                event.set_ready_time(self.env.now)
                event.set_sim_start(self.env.now)
                event.set_resource('backlog')
                event.set_sim_end(self.env.now)
                self.notifyChildren(dependentEventDict, event)
                continue
            if key.is_start:
                # must wait for its arrival
                arrival = self.caseArrivals[key.case_id]
                yield self.env.timeout(arrival - self.env.now)
            # to start reqeust for batching, all grouped events must be ready
            for f in finishSignals:
                allFinished.append(f)
        events = [e for e in eventsArray.keys() if not e.isDone]

        # print(event)
        if len(allFinished) > 0:
            debug_output(f'try do batch at {format_time(self.env.now)}')
            # for each node, all predessors need to finish before continue
            yield self.env.all_of(allFinished)
            debug_output(f'batch, parents are fnished')
            debug_output(f'want resource do this task  batching at time {format_time(self.env.now)}')

            # now make a batch requiest
            # how do I find taht batch resoruce
            # try dellee unused memory
            # del eventsArray[node]
            # del dependentEventDict[node]
            # pick any event in this batch to get the correct resource, since they require the same resources, so it is ok

            event: LabEvent = key

            transitionName = event.group
            priority = 0
            # get skill set resurces

            queryResources = self.eventResourceQuery[transitionName]

            # for batching, I need to find the next avaiable batch by testing my current time

            nextBatch: MyPriorityResource = None

            nextBatchDuration = None
            assignedWorkers: list[Worker] = None
            success = False
            # sort batch resources  start time and only batch start after current time is ok to request

            jobs = []
            for e in events:
                jobs.append(e.id)
                e.set_ready_time(self.env.now)
            # jobs = [e.id for e in events]
            while not success:
                dayOfWeek = (self.env.now // day_in_seconds) % 7
                values = []
                for key, value in queryResources.items():
                    if event.is_in_batch(
                            key) and value.get_workers().get_shift_start() * seconds_per_hour + self.getCurrentDaySec() >= self.env.now:
                        values.append(value)
                if len(values) == 0:
                    # all batch have started and we must wait for next day, sleep until the next day the first shift start
                    # at 00:00:04, we do I do?
                    # sleep until today start, not tmr

                    # at 21:30, sleep until tmr shift start, not today
                    # how do I get the correct start?
                    hourAtNow = int(getDateTime(self.env.now, '%H'))
                    offset = 0
                    if hourAtNow <= 23:
                        offset = 1
                    sleepTime = self.getCurrentDaySec() + offset * day_in_seconds + \
                                6 * seconds_per_hour - self.env.now

                    yield self.env.timeout(sleepTime)

                    debug_output(f'wait next batch for {event} is {format_time(self.env.now)} ')
                else:
                    values.sort(key=lambda res: res.get_workers().get_shift_start())
                    nextBatch = values[0]
                    workers: WorkerCollection = nextBatch.get_workers()
                    nextBatchStartTime = workers.get_shift_start() * seconds_per_hour + self.getCurrentDaySec()
                    nextBatchDuration = workers.get_duration_in_hours() * seconds_per_hour
                    # just take the first earliest batch now

                    debug_output(f'next batch for {event} is {format_time(nextBatchStartTime)} ')
                    # if we request 3am, we can still make it to the night batch if no constraint, so I put a constraint
                    hourAtNow = int(getDateTime(self.env.now, '%H'))
                    if hourAtNow < 5:
                        yield self.env.timeout((5 - hourAtNow) * seconds_per_hour)

                    debug_output(f'request batch for {event} at {format_time(self.env.now)}')
                    quitEvent = self.env.timeout(nextBatchStartTime - self.env.now + 1)
                    newRequest = nextBatch.request(event=BatchJobEvent(worker_id=None, jobArray=jobs),
                                                   priority=priority)
                    result = yield newRequest | quitEvent
                    if quitEvent in result:
                        # too much waiting, quit
                        success = False
                        debug_output(f'quit wait at {format_time(self.env.now)} for {event.case_id}')
                        newRequest.cancel()
                        # request night batch
                    else:
                        # job is done
                        success = True
                        assignedWorkers = result[newRequest]
                        debug_output(f'success  at {format_time(self.env.now)} for {event.case_id}')

            debug_output(f' I can do this node {event} at time {format_time(self.env.now)} by {assignedWorkers}')
            for idx, e in enumerate(events):
                e.set_resource(assignedWorkers[idx].name)
                e.set_sim_start(self.env.now)
            yield self.env.timeout(nextBatchDuration)

            debug_output(f'finish this task {event} at time {format_time(self.env.now)}')
            for idx, e in enumerate(events):
                e.set_sim_end(self.env.now)
            nextBatch.release(newRequest)
        # if another node depends this node to fnish, notify that node this node is finished
        for event in events:
            self.notifyChildren(dependentEventDict, event)

        # this request needs to know the associated resource

    # return index in roster cycle
    def getRosterDayIndex(self, offset=0):
        dayIndex = (self.env.now // day_in_seconds + offset) % (self.rosterDayCycleInSeconds / day_in_seconds)
        return int(dayIndex)

    # return day index from 0 in simulation
    def getDayIndex(self):
        return int(self.env.now // day_in_seconds)



    def nodeProcess(self, event, eventsArray, dependentEventDict):

        if event.isDone:
            event.set_ready_time(self.env.now)
            event.set_sim_start(self.env.now)
            event.set_sim_end(self.env.now)
            event.set_resource('backlog')


        else:

            if event.is_start:
                # must wait for its arrival
                arrival = self.caseArrivals[event.case_id]
                yield self.env.timeout(arrival - self.env.now)
            # print(event)
            debug_output(f'try do {event} at {format_time(self.env.now)}')
            # for each node, all predessors need to finish before continue
            yield self.env.all_of(eventsArray[event])
            event.set_ready_time(self.env.now)
            debug_output(f'{event.id}, parents are fnished, {eventsArray[event]}')
            debug_output(f'want resource do this task {event} at time {format_time(self.env.now)}')
            # try dellee unused memory
            # del eventsArray[node]
            # del dependentEventDict[node]
            transitionName = event.group
            priority = 0
            # get skill set resurces
            # get time index
            duration = event.duration
            isResourceNotNeeded = event.group == noResNeeded

            if not isResourceNotNeeded:
                freeResource, freeResourceRequest, w = yield self.env.process(
                    self.GetFreeRosterResource(event, priority))
                event.set_resource(w.name)
                # event.set_resource_time_index(resoruceTimeIndex)
                # if this worker is working in the next shift, need to make sure overtime will be requested as timeout after next shift start,
                # so it will not do two things at the same time

                # check if after duration leads to overtime
                # self.overTimeRequest(duration, self.env, resoruceTimeIndex, w)

            event.set_sim_start(self.env.now)

            debug_output(f'I can do this node {event} at time {format_time(self.env.now)}')
            yield self.env.timeout(duration)
            event.set_sim_end(self.env.now)
            if not isResourceNotNeeded:
                freeResource.release(freeResourceRequest)
            debug_output(f' I can finish this node {event} at time {format_time(self.env.now)}')

        self.notifyChildren(dependentEventDict, event)

    def overTimeRequest(self, duration, arrayIndex, w):

        timeToFinish = self.env.now + duration
        hourAtFinish = int(getDateTime(timeToFinish, '%H'))
        # how to calcualte if we need over time?
        # if (hourAtFinish > int(getDateTime(self.env.now, '%H'))):
        #    print('fh')
        dayIndex = self.dayCycleIndex(self.env)
        currentDay = self.env.now // day_in_seconds
        currentShiftFinishWithBreak = currentDay * day_in_seconds + self.dayTimeWindowMaps[dayIndex][
            arrayIndex].end * seconds_per_hour - self.secondsBreakPerShiftPeriod
        # finish time is after max working iitem per hour model it as overtime and make up the time in the next hour
        if currentShiftFinishWithBreak < timeToFinish:
            overtime = timeToFinish - (
                    currentDay * day_in_seconds + self.dayTimeWindowMaps[dayIndex][
                arrayIndex].end * seconds_per_hour - self.secondsBreakPerShiftPeriod)
            timeIndex = self.dayTimeWindowMaps[dayIndex][arrayIndex].timeIndex
            staffTimeTable = self.dayStaffRostering[dayIndex][w.name]

            debug_output(f'overtime for this event is {overtime / 60} mins')
            if overtime < 1e-10: return
            # if this staff still works next hour, count over time as flow to next hour as a sudo algorithm
            # for overtime across midnite, how do I handle
            # I think resoruceTimeIndex =  timeIndex
            assert (timeIndex == arrayIndex)

            nextArrayIndex = arrayIndex + 1
            offset = 0
            if nextArrayIndex == len(self.dayTimeWindowMaps[dayIndex]):
                # roll to next day:

                dayIndex = self.getRosterDayIndex(self.env, offset=1)

                staffTimeTable = self.dayStaffRostering[dayIndex][w.name]
                offset = day_in_seconds
                nextArrayIndex = 0

            if nextArrayIndex in staffTimeTable:
                res: staff_rostering = staffTimeTable[nextArrayIndex]
                # what is start do? time to start next shift
                start = currentDay * day_in_seconds + offset + self.dayTimeWindowMaps[dayIndex][
                    nextArrayIndex].start * seconds_per_hour - self.env.now
                self.env.process(self.break_process(env=self.env, resource=res.resource,
                                                    worker_id=res.worker_id, stage=res.resource.tag,
                                                    start=start, duration=overtime))




    def dynamic_resource_row_process(self, row, resourceData):
        day = row.day
        self.update_roster_cycle(day)
        timeIndex = row.timeIndex
        if day not in resourceData:
            resourceData[day] = {}
        if timeIndex not in resourceData[day]:
            resourceData[day][timeIndex] = {}
        transition = row.transition

        resource_set = row.resource_set
        if day not in self.rosterEventSkillSet:
            self.rosterEventSkillSet[day] = {}
        if timeIndex not in self.rosterEventSkillSet[day]:
            self.rosterEventSkillSet[day][timeIndex] = {}

        self.rosterEventSkillSet[day][timeIndex][transition] = resource_set
        # resourceType  = resource_type(transition=transition, capacity=capacity, resource = resource)
        # for each resource, create list of events that require this resource
        for r in resource_set:
            if r not in resourceData[day][timeIndex]:
                resourceData[day][timeIndex][r] = []
            if transition not in resourceData[day][timeIndex][r]:
                resourceData[day][timeIndex][r].append(transition)
        # group resoruces that only serve unique events into a single resource

    def find_dynamic_teams(self, resourceData):
        teams = {}
        for day in resourceData:
            teams[day] = {}
            for timeIndex, res in resourceData[day].items():
                teams[day][timeIndex] = {}
                processedMembers = []
                for r, trans in res.items():
                    if r in processedMembers: continue
                    processedMembers.append(r)
                    sameTeam = [r]
                    for s, sTrans in res.items():
                        if r == s: continue
                        if trans == sTrans:
                            sameTeam.append(s)
                            processedMembers.append(s)
                    teams[day][timeIndex][r] = team_type(teamMember=sameTeam, events=trans)

        return teams

    def RunSim(self, jobs=None, rosterData=None, isLocal=True):
        self.clear()

        global onLocal
        onLocal = isLocal
        if jobs is not None:
            job_bin = jobs
        else:
            print('bin event import')
            # day_data_windows, job_bin = bin_events(log)

            job_bin = loadData()
            print('bin event import finished')
        if rosterData is not None:
            self.set_roster(rosterData)
            self.create_resource_from_roster(rosterData)
        self.create_batch_resource_single_break()
        start = time.time()
        print(f'start sim at {format_time(10)}')
        self.env.process(self.load_next_job_set_by_day(job_bin=job_bin))
        for i in range(24 * self.runUntil):
            self.env.process(self.monitorQueue(seconds_per_hour * i + 0.1))
        # self.env.process(self.monitorQueue(env=self.env, transitionName='accessioning'))
        if self.runUntil is not None:
            self.env.run(until=self.runUntil * day_in_seconds)
        else:
            self.env.run()
        end = time.time()
        print(f'finish sim at {end}')
        print('time spent', end - start)

        breakLog = pd.DataFrame(self.breakDebug, columns=['stage', 'start', 'expected start', 'finish'])
        batchLog = breakLog.sort_values(['start'], ascending=(True,))
        batchLog.to_csv('breakLog.csv', index=False)

    def get_utilisation(self):
        utilisationByDay = {}
        for case, events in self.allCases.items():

            for event in events:
                event: LabEvent = event
                if 'processing' in event.group or self.isEventNoUseResource(event.group):
                    continue
                day = event.get_sim_start() // day_in_seconds

                if day not in utilisationByDay:
                    utilisationByDay[day] = {}

                dayIndex = day % (self.rosterDayCycleInSeconds / day_in_seconds)
                shiftWindow: timeWindow_type = self.dayTimeWindowMaps[dayIndex][event.get_resource_time_index()]

                # print(shiftWindow)

                if shiftWindow.timeIndex not in utilisationByDay[day]:
                    utilisationByDay[day][shiftWindow.timeIndex] = {}
                usedRes = event.get_resource()
                duration = event.get_sim_end() - event.get_sim_start()
                if usedRes not in utilisationByDay[day][shiftWindow.timeIndex]:
                    utilisationByDay[day][shiftWindow.timeIndex][usedRes] = UtilisationCount(group=event.group)
                utilisationByDay[day][shiftWindow.timeIndex][usedRes].accumulate(
                    duration / ((shiftWindow.end - shiftWindow.start) * seconds_per_hour))

        # print(utilisationByDay)
        results = []
        for day, day_utilisation in utilisationByDay.items():
            for timeIndex, utilisations in day_utilisation.items():
                for worker, utilisation in utilisations.items():
                    results.append([getDateTime(day * day_in_seconds, '%Y-%m-%d'), timeIndex, worker, utilisation.group,
                                    utilisation.value])

        dataLog = pd.DataFrame(results, columns=['day', 'time', 'resource', 'group', 'utilisation'])
        dataLog = dataLog.sort_values(["day", "time", "resource"], ascending=(True, True, True))
        dataLog.to_csv('utilisation.csv', index=False)
        return dataLog

    def collect_stats(self, finishTag='finish', startStats=1, endStats=28):
        grouped_roster = self.rosterData['grouped_roster']
        eventLog = []
        queueCount = []
        totalCount = {}
        singleRound = []
        doubleRound = []
        singleRoundSpanByDay = {}
        doubleRoundSpanByDay = {}
        makeSpans = []
        totalSpan = []
        # I want to plot the wait time before going to next stage
        # [case_id, event, wait time]
        waiTimePerCasePerEventSingleRound = []
        waiTimePerCasePerEventDoubleRound = []
        # I want to count number of items each worker done for a particular day, and hour
        resourceProductivity = {}  # key = date, hour, resource name, value is count
        for case, events in self.allCases.items():
            dataAppender = singleRound
            spanByDay = singleRoundSpanByDay
            waitTimeAppender = waiTimePerCasePerEventSingleRound
            start_event = None
            arrival = self.caseArrivals[case]
            day = arrival // day_in_seconds
            sortedEvents: list[Event] = sorted(events, key=lambda e: e.id)

            if sortedEvents[-1].actualName == 'finish_second':
                dataAppender = doubleRound
                spanByDay = doubleRoundSpanByDay
                waitTimeAppender = waiTimePerCasePerEventDoubleRound
            if day not in spanByDay:
                spanByDay[day] = []

            if True:
                startTime = sortedEvents[0].start
                # calcualte the upper bound for finish time
                # if this job arrive befor 12 and is a lunch batch, upper bound is 2pm
                # if this job arrive after 12 and is a lunch batch, upper bound is next day 5am, not sure about weekend
                # if this job arrive after 12 and is a night batch, upper bound is next day 5am
                # if this job arrive before 12 and is a night batch, upper bound is next day 5am
                firstEvent: LabEvent = sortedEvents[0]
                arrivalHour = int(getDateTime(firstEvent.start, '%H'))
                # use average waiting time for access and grossing to estimate the minimum time
                accessMeanWaitHour = 1
                grossingMeanWaitHour = 1
                if firstEvent.batch_filter == ['processing_0',
                                               'processing_1200'] and arrivalHour < 12 - accessMeanWaitHour - grossingMeanWaitHour:
                    currentDayInSec = firstEvent.start // day_in_seconds * day_in_seconds
                    todayAtTwoPM = currentDayInSec + 14 * seconds_per_hour
                    lowerBoundDuration = todayAtTwoPM - firstEvent.start + grossingMeanWaitHour * seconds_per_hour
                    if sortedEvents[-1].actualName == 'finish_second':
                        lowerBoundDuration += 2 * day_in_seconds
                    deltaBound = timedelta(seconds=int(lowerBoundDuration))
                    converted_delta = str(deltaBound)

                    debug_output(
                        f'arrive at {format_time(firstEvent.start)} finish at {format_time(firstEvent.start + lowerBoundDuration)}, duration={converted_delta}')

                if firstEvent.batch_filter == ['processing_0',
                                               'processing_1200'] and arrivalHour >= 12 - accessMeanWaitHour - grossingMeanWaitHour and arrivalHour < 20 - accessMeanWaitHour - grossingMeanWaitHour:
                    currentDayInSec = firstEvent.start // day_in_seconds * day_in_seconds
                    tmrAtFive = currentDayInSec + day_in_seconds + 5 * seconds_per_hour
                    lowerBoundDuration = tmrAtFive - firstEvent.start + grossingMeanWaitHour * seconds_per_hour
                    if sortedEvents[-1].actualName == 'finish_second':
                        lowerBoundDuration += 2 * day_in_seconds
                    deltaBound = timedelta(seconds=int(lowerBoundDuration))
                    converted_delta = str(deltaBound)

                    debug_output(
                        f'arrive at {format_time(firstEvent.start)} finish at {format_time(firstEvent.start + lowerBoundDuration)}, duration={converted_delta}')

                if firstEvent.batch_filter == ['processing_0',
                                               'processing_1200'] and arrivalHour >= 20 - accessMeanWaitHour - grossingMeanWaitHour:
                    currentDayInSec = firstEvent.start // day_in_seconds * day_in_seconds
                    tmrAt2PM = currentDayInSec + day_in_seconds + 14 * seconds_per_hour
                    lowerBoundDuration = tmrAt2PM - firstEvent.start + grossingMeanWaitHour * seconds_per_hour
                    if sortedEvents[-1].actualName == 'finish_second':
                        lowerBoundDuration += 2 * day_in_seconds
                    deltaBound = timedelta(seconds=int(lowerBoundDuration))
                    converted_delta = str(deltaBound)

                    debug_output(
                        f'arrive at {format_time(firstEvent.start)} finish at {format_time(firstEvent.start + lowerBoundDuration)}, duration={converted_delta}')

                if firstEvent.batch_filter == [
                    'processing_1200'] and arrivalHour < 20 - accessMeanWaitHour - grossingMeanWaitHour:
                    currentDayInSec = firstEvent.start // day_in_seconds * day_in_seconds
                    tmrAtFive = currentDayInSec + day_in_seconds + 5 * seconds_per_hour
                    lowerBoundDuration = tmrAtFive - firstEvent.start + grossingMeanWaitHour * seconds_per_hour
                    if sortedEvents[-1].actualName == 'finish_second':
                        lowerBoundDuration += 2 * day_in_seconds
                    deltaBound = timedelta(seconds=int(lowerBoundDuration))
                    converted_delta = str(deltaBound)

                    debug_output(
                        f'arrive at {format_time(firstEvent.start)} finish at {format_time(firstEvent.start + lowerBoundDuration)}, duration={converted_delta}')

                if firstEvent.batch_filter == [
                    'processing_1200'] and arrivalHour >= 20 - accessMeanWaitHour - grossingMeanWaitHour:
                    currentDayInSec = firstEvent.start // day_in_seconds * day_in_seconds
                    theDayAfterTmrAtFive = currentDayInSec + 2 * day_in_seconds + 5 * seconds_per_hour
                    lowerBoundDuration = theDayAfterTmrAtFive - firstEvent.start + grossingMeanWaitHour * seconds_per_hour
                    if sortedEvents[-1].actualName == 'finish_second':
                        lowerBoundDuration += 2 * day_in_seconds
                    deltaBound = timedelta(seconds=int(lowerBoundDuration))
                    converted_delta = str(deltaBound)

                    debug_output(
                        f'arrive at {format_time(firstEvent.start)} finish at {format_time(firstEvent.start + lowerBoundDuration)}, duration={converted_delta}')

                finishTime = sortedEvents[-1].get_sim_end()
                duration = finishTime - startTime
                conversion = timedelta(seconds=int(duration))
                converted_time = str(conversion)
                totalSpan.append(duration)
                spanByDay[day].append(duration)

                # makeSpans.append([sortedEvents[0].case_id, format_time(startTime), converted_time, converted_delta])

                for e in sortedEvents:
                    event: LabEvent = e

                    datesStr = getDateTime(n=event.get_sim_start(), out_date_format_str='%m/%d/%Y')
                    hourStr = getDateTime(n=event.get_sim_start(), out_date_format_str='%H')
                    useResource = event.useResource
                    if useResource is not None:
                        k: product_count_type = product_count_type(day=datesStr, time=hourStr, resource=useResource)
                        if resourceProductivity.get(k) is None:
                            resourceProductivity[k] = UserCount(key=k, group=event.group)
                        resourceProductivity[k].accumulate()

                    eventDuration = event.get_sim_end() - event.get_sim_start()
                    data = [event.case_id, event.actualName, format_time(event.get_sim_start()),
                            format_time(event.get_sim_end()),
                            event.get_resource(), event.id, event.parents, event.group, eventDuration, event.speciality, event.batch_filter]
                    #print(event)
                    if event.ready_time < event.get_sim_start():
                        # this event is queued for processing
                        eventLog.append([event.group, format_time(event.ready_time), 'ready'])
                        # get it from the queue
                        eventLog.append([event.group, format_time(event.get_sim_start()), 'start'])
                    if event.group not in totalCount:
                        totalCount[event.group] = 0
                    totalCount[event.group] += 1
                    dataAppender.append(data)

                    if len((event.parents)) > 0:
                        # for this event, I know its resource, if the rouse group has ppl on the weekend, don't minus weekend
                        # otherwise, basically minus days that ppl are not around on the weekend
                        waitDif = timedelta(seconds=int(event.get_sim_start() - event.ready_time))

                        job_start_time = SimSetting.start_time_sim + timedelta(seconds=event.get_sim_start())
                        job_ready_time = SimSetting.start_time_sim + timedelta(seconds=event.ready_time)
                        job_start_week = job_start_time.isocalendar()[1]
                        job_ready_week = job_ready_time.isocalendar()[1]
                        if event.group != 'processing' and job_start_week - job_ready_week == 1:

                            resource_group = event.group
                            job_ready_week_start_monday = job_ready_time - timedelta(days=job_ready_time.weekday() % 7)
                            job_ready_week_start_sat = job_ready_week_start_monday + timedelta(days=5)
                            job_ready_week_start_sat = job_ready_week_start_sat.replace(minute=0, second=0, hour=0)
                            job_ready_week_start_sun = job_ready_week_start_monday + timedelta(days=6)
                            job_ready_week_start_sun = job_ready_week_start_sun.replace(minute=0, second=0, hour=0)
                            # print('ready week', job_ready_time, job_ready_week, 'start processing week',  job_start_time, job_start_week, 'ready week monday',
                            #      job_ready_week_start_monday, job_ready_week_start_sat, job_ready_week_start_sun)
                            isWorkingOnSat = job_ready_week_start_sat in grouped_roster[resource_group]
                            isWorkingOnSun = job_ready_week_start_sun in grouped_roster[resource_group]
                            if not isWorkingOnSat:
                                waitDif = waitDif - timedelta(days=1)
                            if not isWorkingOnSun:
                                waitDif = waitDif - timedelta(days=1)
                        # print(job_ready_week_start_sat in grouped_roster[resource_group])

                        # print(job_ready_week_start_sun in grouped_roster[resource_group],waitDif)

                        waitTime = [event.case_id, event.group,
                                    waitDif, waitDif.total_seconds(),
                                    format_time(self.caseArrivals[event.case_id])]
                        waitTimeAppender.append(waitTime)
        outputs = {}

        for stage, queueData in self.queueMonitor.items():
            queueData = pd.DataFrame(queueData, columns=['stage', 'time', 'queue'])
            queueData = queueData.sort_values(['time'], ascending=(True,))
            queueData.to_csv(f'{stage}_queue.csv', index=False)
            outputs[f'{stage}_queue']= queueData


        productCountData = []
        for key, value in resourceProductivity.items():
            key: product_count_type = key
            value: UserCount = value
            dateStr = key.day
            timeStr = key.time
            resource = key.resource
            group = value.group
            productCountData.append([dateStr, timeStr, resource, group, value.count])

        eventLog = pd.DataFrame(eventLog, columns=['event', 'time', 'type'])
        eventLog = eventLog.sort_values(['time'], ascending=(True,))
        eventLog.to_csv('eventReplay.csv', index=False)

        breakLog = pd.DataFrame(self.breakDebug, columns=['stage', 'start', 'expected start', 'finish'])
        batchLog = breakLog.sort_values(['start'], ascending=(True,))
        batchLog.to_csv('breakLog.csv', index=False)

        batchLog = pd.DataFrame(self.batchDebug, columns=['id', 'start', 'finish'])
        batchLog = batchLog.sort_values(['start'], ascending=(True,))
        batchLog.to_csv('batchLog.csv', index=False)

        productCountDataLog = pd.DataFrame(productCountData, columns=['day', 'timeIndex', 'name', 'group', 'count'])
        productCountDataLog = productCountDataLog.sort_values(["day", "timeIndex", 'group'],
                                                              ascending=(True, True, True))

        # Use GroupBy() to compute the sum

        productCountDataLog.to_csv('productivity.csv', index=False)

        for key, value in self.queueStat.items():
            queueCount.append([key[0], key[1], key[2], value])
        queueLog = pd.DataFrame(queueCount, columns=['day', 'timeIndex', 'event', 'count'])
        queueLog = queueLog.sort_values(["day", "timeIndex"], ascending=(True, True))

        dataLog = pd.DataFrame(singleRound,
                                   columns=['case_id', 'event', 'start', 'finish', 'resource', 'id', 'parents', 'group',
                                            'duration', 'speciality','priority'])
        dataLog = dataLog.sort_values(["id"], ascending=(True))
        # dataLog = dataLog.drop(columns=['parents'])
        dataLog.to_csv('single-round.csv', index=False)

        doubleDataLog = pd.DataFrame(doubleRound,
                                         columns=['case_id', 'event', 'start', 'finish', 'resource', 'id', 'parents',
                                                  'group', 'duration', 'speciality'])
        doubleDataLog = doubleDataLog.sort_values(["id"], ascending=(True))
        doubleDataLog = doubleDataLog.drop(columns=['parents'])
        doubleDataLog.to_csv('double-round.csv', index=False)

        frames = [dataLog, doubleDataLog]
        allDataLog = pd.concat(frames)
        allDataLog = allDataLog.sort_values(["id"], ascending=(True))
        allDataLog.to_csv('allDataLog.csv', index=False)


        totalSpan = statistics.mean(totalSpan)
        conversion = timedelta(seconds=int(totalSpan))
        converted_time = str(conversion)
        # print(converted_time)

        waitDataLog = pd.DataFrame(waiTimePerCasePerEventSingleRound,
                                       columns=['case_id', 'event', 'wait', 'waitInSec', 'arrival'])
        waitDataLog.to_csv('wait-single.csv', index=False)

        doubleWaitDataLog = pd.DataFrame(waiTimePerCasePerEventDoubleRound,
                                             columns=['case_id', 'event', 'wait', 'waitInSec', ' arrival'])
        doubleWaitDataLog.to_csv('wait-double.csv', index=False)

        # print(f'total duration is {converted_time}')


        outputs['single-wait'] = waitDataLog
        outputs['double-wait'] = doubleWaitDataLog
        outputs['single-data'] = dataLog
        outputs['double-data'] = doubleDataLog
        outputs['single-span'] = singleRoundSpanByDay
        outputs['double-span'] = doubleRoundSpanByDay
        outputs['totalCount'] = totalCount
        outputs['queue'] = queueLog
        outputs['productivity'] = productCountDataLog
        outputs['replay'] = eventLog
        outputs['allData'] = allDataLog
        return outputs



def replay_queue(replayLog):
    replayLog = replayLog[(replayLog.event != 'waitAdditional')]
    queueCount = {}
    queueData = []
    columns = ['accessioning', 'grossing', 'processing', 'embedding', 'section', 'signout', 'time']
    for index, row in replayLog.iterrows():
        if row.type == 'ready':
            if row.event not in queueCount:
                queueCount[row.event] = 0
            queueCount[row.event] += 1
        if row.type == 'start':
            queueCount[row.event] -= 1
        queueCount['time'] = row.time

        data = []
        for column in columns:
            if column not in queueCount:
                data.append(0)
            else:
                data.append(queueCount[column])
        queueData.append(data)

    queueData = pd.DataFrame(queueData, columns=columns)
    queueData.to_csv('event-queue.csv', index=False)
    return queueData


def RunLocalSim(jobs=None, isLocal=True, runUntil=None, resourceRosterLog=None, timeWindowMapLog=None, recorder={}):
    simObj = LabRosterSimulation(isLocalSim=isLocal)
    simObj.secondsBreakPerShiftPeriod = 10 * 60  # 45 mins per hour, 15 mins break
    # this event requests  single resource do all of them for a single case
    singleResoruceEvents = ['grossing']
    batchEvents = ['processing']
    batchWorkerMark = ['processing_0']
    # these events dont require resoruces
    eventsNoUseResource = ['register', 'finish', 'waitAdditional', 'additionalRequest']
    simObj.set_batch_data(batchEvents=batchEvents, batchWorkerMark=batchWorkerMark,
                          singleResourceEvent=singleResoruceEvents, eventsNoUseResource=eventsNoUseResource)
    simObj.set_run_until(days=runUntil)
    assert (resourceRosterLog is not None)
    print(resourceRosterLog.dtypes)
    simObj.RunSim(resourceLog=resourceRosterLog, timeWindowMapLog=timeWindowMapLog)

    outputs = simObj.collect_stats(finishTag='finish')
    recorder['outputs'] = outputs
    singleData = outputs['single-data']
    # check all processing start at 12 or 20
    processingLog = singleData[singleData['group'] == 'processing']
    processingLog = processingLog[(processingLog['start'].str.contains('12:00:00') == False) &
                                  (processingLog['start'].str.contains('20:00:00') == False)]

    processingLog.to_csv('processing.csv', index=False)

    allDataLog = outputs['allData']
    allDataLog = allDataLog[allDataLog.group == 'grossing']
    allDataLogGrossingMean = allDataLog.groupby(['speciality'])['duration'].mean() / 60
    allDataLogGrossingMean = allDataLogGrossingMean.reset_index()
    groupCounts = allDataLog.groupby(['speciality']).size().reset_index(name='counts')
    groupCounts['counts'] = groupCounts['counts'] / groupCounts['counts'].sum()
    print(groupCounts)
    mergedRes = pd.merge(groupCounts, allDataLogGrossingMean, on='speciality')
    mergedRes['weightedDuration'] = mergedRes['counts'] * mergedRes['duration']
    print('average grossing duration is ', mergedRes['weightedDuration'].sum())
    mergedRes.to_csv('gorssing-mean.csv')

    uMean = singleData.groupby(['group', 'speciality'])['duration'].mean() / 60
    uMean = uMean.reset_index()
    print(f'single round event duration stat is {uMean}')
    uMean.to_csv('event_duration_mean.csv', index=False)

    span = outputs['single-span']
    meanLog = showSpan(span, 'single')
    recorder['single-span'] = meanLog
    # ax = meanLog.plot.bar(x='day', y='durationInDays', rot=0)
    # plt.show()

    # checkDiff(file1, file2)
    waitLog = outputs['single-wait']
    waitLog.to_csv('wait.csv', index=False)
    waitMean = showWaitDuration(waitLog)
    recorder['single-wait'] = waitMean
    # show double round
    doubleData = outputs['double-data']
    uMean = doubleData.groupby(['group'])['duration'].mean()

    span = outputs['double-span']
    showSpan(span)

    # checkDiff(file1, file2)
    waitLog = outputs['double-wait']
    showWaitDuration(waitLog)

    print(singleData['group'].value_counts())
    print(doubleData['group'].value_counts())

    queueLog = outputs['queue']
    queueLog.to_csv('queue.csv', index=False)
    for key, value in outputs['totalCount'].items():
        print(f'event {key} count= {value}')
    # heatmap(simObj=simObj)
    productCountDataLog = outputs['productivity']
    print(productCountDataLog.dtypes)
    df2 = productCountDataLog.groupby(['day', 'group'])['count'].mean()
    df2.to_csv('productivity.csv', index=False)
    # count how many cassests processed on each batch of lunch and night of each day
    processingCountLog = productCountDataLog[productCountDataLog['group'] == 'processing']
    processingCountLog = processingCountLog.groupby(['day', 'timeIndex', 'group'])['count'].sum()

    print('processing count', processingCountLog)
    processingCountLog.to_csv('processing_count.csv')

    replay_queue(outputs['replay'])


def showWaitDuration(waitLog):
    print(waitLog.dtypes)
    uMean = waitLog.groupby(['event'])['wait'].mean()
    print(f'event waiting stat is {uMean}')
    return uMean


def showSpan(span, columnName=''):
    meanLog = []
    for day, values in span.items():
        totalSpan = statistics.mean(values)
        conversion = timedelta(seconds=int(totalSpan))
        converted_time = str(conversion)
        dayStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        meanLog.append([dayStr, converted_time, totalSpan / day_in_seconds])

    meanLog = pd.DataFrame(meanLog, columns=['day', f'{columnName}-duration', f'{columnName}-durationInDays'])



    return meanLog


def test(isLocalSim=True, staffRosterDict=None):
    rosterData = {'roster_dict': staffRosterDict, 'roster_df': rosterDF(staffRosterDict),
                  'roster_week': SimSetting.rosterWeekDayList, 'grouped_roster': group_roster_by_date(staffRosterDict)}

    simObj = LabRosterSimulation(isLocalSim=isLocalSim)
    singleResoruceEvents = ['grossing']
    batchEvents = ['processing']
    batchWorkerMark = ['processing_0']
    # these events dont require resoruces
    eventsNoUseResource = ['register', 'finish', 'waitAdditional', 'additionalRequest']
    simObj.set_batch_data(batchEvents=batchEvents, batchWorkerMark=batchWorkerMark,
                          singleResourceEvent=singleResoruceEvents, eventsNoUseResource=eventsNoUseResource)
    simObj.set_run_until(days=70)

    simObj.RunSim(rosterData=rosterData)
    outputs = simObj.collect_stats(finishTag='finish')

    singleData = outputs['single-data']
    # check all processing start at 12 or 20
    processingLog = singleData[singleData['group'] == 'processing']
    processingLog = processingLog[(processingLog['start'].str.contains('12:00:00') == False) &
                                  (processingLog['start'].str.contains('20:00:00') == False)]

    processingLog.to_csv('processing.csv', index=False)

    allDataLog = outputs['allData']
    allDataLog = allDataLog[allDataLog.group == 'grossing']
    allDataLogGrossingMean = allDataLog.groupby(['speciality'])['duration'].mean() / 60
    allDataLogGrossingMean = allDataLogGrossingMean.reset_index()
    groupCounts = allDataLog.groupby(['speciality']).size().reset_index(name='counts')
    groupCounts['counts'] = groupCounts['counts'] / groupCounts['counts'].sum()
    print(groupCounts)
    mergedRes = pd.merge(groupCounts, allDataLogGrossingMean, on='speciality')
    mergedRes['weightedDuration'] = mergedRes['counts'] * mergedRes['duration']
    print('average grossing duration is ', mergedRes['weightedDuration'].sum())
    mergedRes.to_csv('gorssing-mean.csv')

    uMean = singleData.groupby(['group', 'speciality'])['duration'].mean() / 60
    uMean = uMean.reset_index()
    print(f'single round event duration stat is {uMean}')
    uMean.to_csv('event_duration_mean.csv', index=False)
    recorder = {}
    span = outputs['single-span']
    meanLog = showSpan(span, 'single')
    meanLog.to_csv('span.csv', index= False)
    recorder['single-span'] = meanLog

    meanLog = meanLog[meanLog.day > '2021-05-07']
    print(meanLog)
    recorder['overall-mean'] = meanLog['single-durationInDays'].mean()
    # ax = meanLog.plot.bar(x='day', y='durationInDays', rot=0)
    # plt.show()

    # checkDiff(file1, file2)
    waitLog = outputs['single-wait']
    waitLog.to_csv('wait.csv', index=False)
    waitMean = showWaitDuration(waitLog)
    recorder['single-wait'] = waitMean
    # show double round
    doubleData = outputs['double-data']
    uMean = doubleData.groupby(['group'])['duration'].mean()

    span = outputs['double-span']
    showSpan(span)

    # checkDiff(file1, file2)
    waitLog = outputs['double-wait']
    showWaitDuration(waitLog)

    print(singleData['group'].value_counts())
    print(doubleData['group'].value_counts())

    queueLog = outputs['queue']
    queueLog.to_csv('queue.csv', index=False)
    for key, value in outputs['totalCount'].items():
        print(f'event {key} count= {value}')
    # heatmap(simObj=simObj)
    productCountDataLog = outputs['productivity']
    print(productCountDataLog.dtypes)
    df2 = productCountDataLog.groupby(['day', 'group'])['count'].mean()
    df2.to_csv('productivity.csv', index=False)
    # count how many cassests processed on each batch of lunch and night of each day
    processingCountLog = productCountDataLog[productCountDataLog['group'] == 'processing']
    processingCountLog = processingCountLog.groupby(['day', 'timeIndex', 'group'], as_index=False)['count'].sum()

    print('processing count', processingCountLog['count'].sum())
    processingCountLog.to_csv('processing_count.csv')

    replay_queue(outputs['replay'])

    return outputs, recorder

# test(isLocalSim=True, staffRosterDict = staffRosterDict)
