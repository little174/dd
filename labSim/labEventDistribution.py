import collections
import copy
import datetime
import random

from commonParams import *

showRoster = False

embedding = 'embedding'
access = 'accessioning'
section = 'section'
grossing = 'grossing'
signout = 'signout'
processingStr = 'processing'
total_processing = 1200  # number of processing machines
seconds_per_hour = 60 * 60
seconds_per_min = 60
day_in_seconds = seconds_per_hour * 24

resource_data = collections.namedtuple('event_name', 'resource_names')
case_data = collections.namedtuple('case_data', 'total	sub_speciality	two_hour_prob	nbSpecimenPerAccess	accessMinsPerSpecimen	nbCassettePerSpecimen	grossAverageMinsPerCassette	embeddingAverageMinsPerCassette	nbSlidePerSection	sectionAverageMinsPerSlide	signoutAverageMinsPerSlide'
)
history_data = collections.namedtuple('history', 'index arrivalTime speciality isTwoHour isAdditionalNeeded '
                                                 'specimen_spec_type specimen_sub_speciality case_specimen_count case_cassette_count case_slide_count')




# for every event that requires a resoruce, create the map
event_to_resrouce = {embedding: embedding, access: access, section: section,
                     grossing: grossing, signout:signout, processingStr:processingStr}

#revert the map
resource_to_event = {}

priorityMap = {0: 'Normal', -1: 'High'}

for key, value in event_to_resrouce.items():
    if value not in resource_to_event:
        resource_to_event[value] = []
    resource_to_event[value].append(key)
print(resource_to_event)


import numpy as np

SimSetting.setup(backlogWeek=1, rosterWeekBlockSpan=7)

def import_csv(file_path, checkLog=False):
    print(f'start loading {file_path}')

    eventlog = pd.read_csv(file_path, sep=',', skipinitialspace=True)
    return eventlog


def case_row(row, case_data_dict, total_cases):

    case_data_dict[row.sub_speciality] = case_data(sub_speciality=row.sub_speciality,
                                                        total = row.total / total_cases,
                                                        nbSpecimenPerAccess=row.nbSpecimenPerAccess,
                                                   two_hour_prob = row.two_hour_prob,
                                                   accessMinsPerSpecimen = row.accessMinsPerSpecimen,
                                                   nbCassettePerSpecimen = row.nbCassettePerSpecimen,
                                                   grossAverageMinsPerCassette = row.grossAverageMinsPerCassette,
                                                   embeddingAverageMinsPerCassette = row.embeddingAverageMinsPerCassette,
                                                   nbSlidePerSection = row.nbSlidePerSection,
                                                   sectionAverageMinsPerSlide = row.sectionAverageMinsPerSlide,
                                                   signoutAverageMinsPerSlide = row.signoutAverageMinsPerSlide

    )
def arrival_hour_row(row, call_rate):
    start = row.Start
    finish = row.Finish
    percentage = row.Percentage / 100.0
    call_rate[start] = percentage

def work_station_row(row, work_station_time_dict):
    subSpecialty = row.SubSpecialty
    access = int(row.access * 60)
    embedding = int(row.embedding * 60)
    sectionNb = int(row.section * 60)
    signout = int(row.signout * 60)
    work_station_time_dict[subSpecialty,'accessioning'] = access
    work_station_time_dict[subSpecialty, 'embedding'] = embedding
    work_station_time_dict[subSpecialty, 'section'] = sectionNb
    work_station_time_dict[subSpecialty, 'signout'] = signout

def grossing_row(row, grossing_dict):
    sub_speciality = row.sub_speciality.strip()
    spec_type = row.spec_type.strip()
    grossing_time_specimen_secs = int(row.Grossing_Time_Specimen * 60)
    if sub_speciality not in grossing_dict:
        grossing_dict[sub_speciality] = {}
    grossing_dict[sub_speciality][spec_type] = grossing_time_specimen_secs

def roster_row(row, rosters):
    week = int(row.week)
    day = int(row.day)
    timeIndex = int(row.timeIndex)
    nbAccession = int(row.accessioning)
    if nbAccession > 0 and timeIndex > 13 and timeIndex < 19:
        nbAccession = nbAccession
    nbGrossing = int(row.grossing)
    nbEmbedding = int(row.embedding)
    nbSectioning = int(row.sectioning)
    nbSignout = int( row.signout)
    #nbRegister = row.register
    if week not in rosters:
        rosters[week]  = {}
    if day not in rosters[week]:
        rosters[week][day] = {}
    if timeIndex not in rosters[week][day]:
        rosters[week][day][timeIndex] = {}
    rosters[week][day][timeIndex]['accessioning'] = nbAccession
    rosters[week][day][timeIndex]['grossing'] = nbGrossing
    rosters[week][day][timeIndex]['embedding'] = nbEmbedding
    rosters[week][day][timeIndex]['section'] = nbSectioning
    rosters[week][day][timeIndex]['signout'] = nbSignout
    #rosters[day][timeIndex]['register'] = nbRegister




class LabData:
    stations = [access, grossing, embedding, section, signout]

    rosterCopy = {}
    work_station_time_dict = {} # duration for each station by speciality except grossing
    grossing_dict = {}
    case_data_dict = {}
    case_categories = []
    case_weights = []
    total_cases = 0
    useFixedData = False
    daily_rate = 230
    call_rate = {}
    rostering = {}
    arrivalLog = None

    
    jobDistribution = JobPattern()


    # two batches, lunch and evening
    batch_filter = [f'processing_{i*total_processing}' for i in range(2)]
    batchTeamShift = {0: [12, 14], 1: [20, 29]}


    
    def __init__(self):
        self.directive = {'register': self.getRegDuration, 'accessioning': self.getAccessDuration,
                          'grossing': self.getGrossDuration, 'processing': self.getProcessDuration,
                          'embedding': self.getEmbeddingDuration, 'section': self.getSectionDuration,
                          'signout': self.getSignoutDuration, 'finish': self.getFinishDuration,
                          'waitAdditional': self.getWaitAdditionalDuration, 'additionalRequest': self.getAdditionalRequestDuration}


        self.selectedCategory = random.choices(self.case_categories, self.case_weights, k=1)[0]
    def setCategory(self, value):
        if value in self.case_categories:
            self.selectedCategory = value

    @classmethod
    def setArrivalRate(cls, rate):
        cls.daily_rate = rate

    @classmethod
    def loadWorkStationTime(cls, log):
        cls.work_station_time_dict = {}
        log.apply(work_station_row, work_station_time_dict=cls.work_station_time_dict, axis=1)
    @classmethod
    def loadGrossingTime(cls, log):
        cls.grossing_dict = {}
        log.apply(grossing_row, grossing_dict=cls.grossing_dict, axis=1)

    @classmethod
    def loadSubcategory(cls, log):
        cls.case_categories = []
        cls.case_weights = []
        cls.case_data_dict = {}
        print('inject client data')
        cls.case_data_log = log
        cls.total_cases = sum(cls.case_data_log['total'])
        cls.case_data_log.apply(case_row, case_data_dict=cls.case_data_dict, total_cases=cls.total_cases, axis=1)
        for key, case in cls.case_data_dict.items():
            cls.case_categories.append(key)
            cls.case_weights.append(case.total)
            print(f'{key} weight {case.total}')
        assert(len(cls.case_categories) == len(cls.case_weights))
    @classmethod
    def loadArrivalHourRate(cls, log):
        print('get hour rate')
        for i in range(24):
            cls.call_rate[i] = 0
        log.apply(arrival_hour_row, call_rate=cls.call_rate, axis=1)
        total = 0
        for key, value in cls.call_rate.items():
            cls.call_rate[key] = int(cls.daily_rate * value)
            #print(f'hour {key} rate is {cls.call_rate[key]}')
            total += cls.call_rate[key]
        print(f'total rate = {total}')

    @classmethod
    def loadRostering(cls, log = None, rosterModifier = None, modeifyDirty = True):
        results = []
        print('get roster')
        log.fillna(0, inplace=True)
        log = log.astype({'accessioning': 'int32','grossing': 'int32','embedding': 'int32','sectioning': 'int32','signout': 'int32' })

        log.to_csv('baseRoster.csv')
        log.apply(roster_row, rosters=cls.rostering, axis = 1)
        cls.rosterCopy = copy.deepcopy(cls.rostering)
        # now for every row output staffs
        minTimeIndex = 100
        maxTimeIndex= 0
        weeks = sorted(list(cls.rostering.keys()))
        print('weeks are', weeks)
        for idx, week in enumerate(weeks):
            dayCounterOffset = 7 * idx
            for day in cls.rostering[week]:

                for timeIndx in cls.rostering[week][day]:
                    if timeIndx < minTimeIndex:
                        minTimeIndex = timeIndx
                    if timeIndx > maxTimeIndex:
                        maxTimeIndex = timeIndx

                    for station, nbStaff in cls.rosterCopy[week][day][timeIndx].items():
                        if modeifyDirty:
                            nbStaff = cls.modifyDirtyData(day, nbStaff, station, timeIndx, week)
                        #based on roster guide, 13-21, typically has 2 staff all the time for access
                        if rosterModifier is not None:
                            nbStaff = rosterModifier(day, nbStaff, station, timeIndx, week)
                        cls.rostering[week][day][timeIndx][station] = nbStaff
                        #print(f'day {day+dayCounterOffset} time {timeIndx} station {station} nb={nbStaff}')
                        if nbStaff > 0:
                            staffList = []

                            for i in range(nbStaff):
                                staffList.append(f'{station}_{i}')
                            results.append([day + dayCounterOffset, timeIndx-minTimeIndex, station, staffList])
        # I want to restore the modified roster as dataframe
        columns = ['year', 'week','day','timeIndex' ]

        modifiedLog = []
        for idx, week in enumerate(weeks):
            for day in cls.rostering[week]:
                for timeIndx in cls.rostering[week][day]:
                    if timeIndx < minTimeIndex:
                        minTimeIndex = timeIndx
                    if timeIndx > maxTimeIndex:
                        maxTimeIndex = timeIndx
                    data = [2021, week,day, timeIndx]

                    for station in cls.stations :
                        data.append( cls.rostering[week][day][timeIndx][station])
                    modifiedLog.append(data)
        columns = columns + cls.stations
        modifiedLog = pd.DataFrame(modifiedLog, columns=columns)
        modifiedLog = modifiedLog.sort_values(['week','day','timeIndex'], ascending=(True,True,True))
        modifiedLog.to_csv('modified-roster.csv', index=False)
        dayList = list([i for i in range(len(weeks) * 7)])
        timeList = [i for i in range(minTimeIndex, maxTimeIndex + 1)]
        if True:
            processingList = [f'processing_{i}' for i in range(2400)]




            for day in dayList:
                for t in timeList:
                    results.append([day, t - minTimeIndex, 'processing', processingList])
            print('time hour ids are ', timeList)
        print('day ids are', dayList)
        dataLog = pd.DataFrame(results, columns=['day','timeIndex','transition', 'resource_set'])
        dataLog = dataLog.sort_values(["day","timeIndex", "transition"], ascending=(True, True, True))
        dataLog.to_csv('day_roster.csv', index=False)
        # calcualte average maek span




        results.clear()
        for day in dayList:
            for t in timeList:
                results.append([day, t-minTimeIndex, t, t + 1])
        dayTimeLog = pd.DataFrame(results, columns=['day','timeIndex','start', 'end'])
        dayTimeLog = dayTimeLog.sort_values(["day","timeIndex"], ascending=(True, True))
        dayTimeLog.to_csv('dayTimeMap.csv', index=False)
        return dataLog, dayTimeLog

    @classmethod
    def adHocRoster(cls, day, nbStaff, station, timeIndx, week):

        # move grossing staff fom 6-8 to 17-19
        if station == 'grossing' and day >= 0 and day <= 4 and timeIndx >= 6 and timeIndx <= 9:
            nbStaff = 0
        if station == 'grossing' and day >= 0 and day <= 4 and timeIndx >= 16 and timeIndx <= 19:
            nbStaff = nbStaff + cls.rosterCopy[week][day][timeIndx - 10][station]


        return nbStaff

    @classmethod
    def moveSectionToMorning(cls, day, nbStaff, station, timeIndx, week):

        # move grossing staff fom 6-8 to 17-19
        if station == 'grossing' and day >= 0 and day <= 4 and timeIndx >= 6 and timeIndx <= 9:
            nbStaff = 0
        if station == 'grossing' and day >= 0 and day <= 4 and timeIndx >= 16 and timeIndx <= 19:
            nbStaff = nbStaff + cls.rosterCopy[week][day][timeIndx - 10][station]



        if station == 'section' and day >=0 and day <=4 and timeIndx >= 17 and timeIndx <=18:
            nbStaff = 0

        if station == 'section' and day >= 0 and day <= 4 and timeIndx >= 5 and timeIndx <= 6:
            nbStaff = nbStaff + cls.rosterCopy[week][day][timeIndx +12][station]

        return nbStaff

    @classmethod
    def modifyDirtyData(cls, day, nbStaff, station, timeIndx, week):
        if station == 'accessioning' and day >= 0 and day <= 4 and timeIndx >= 13 and timeIndx <= 21:
            nbStaff = 2
        # from 6am to 20 from Monday to Friday, at any time, if there is staff befre current time and staff after current time + 1 or 2 hour, we assume
        # this period also has staff
        if timeIndx >= 6 and timeIndx <= 20:
            if nbStaff == 0 and cls.rosterCopy[week][day][timeIndx - 1][station] > 0 \
                    and (
                    cls.rosterCopy[week][day][timeIndx + 1][station] > 0 or cls.rosterCopy[week][day][timeIndx + 2][
                station] > 0):
                nbStaff = cls.rosterCopy[week][day][timeIndx - 1][station]
        return nbStaff

    @classmethod
    def setUseFixedData(cls, fixed = True):
        cls.useFixedData = fixed

    def __getitem__(self, label):
        for key, value in self.directive.items():
            if key in label:
                return value()
        print(f'wrong label is {label}')
        assert (False)

    def getWaitAdditionalDuration(self):
        return 60 * 60 * 24 * 2

    def getAdditionalRequestDuration(self):
        return 30

    def getRegDuration(self):
        return 0

    def getFinishDuration(self):
        return 0

    def getAccessDuration(self, nbSpecimen):
        if self.useFixedData:
            return 1
        try:
            data = self.work_station_time_dict[self.selectedCategory, 'accessioning']
            # Generate Distribution:
            n = data/ 0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])
        except KeyError:
            n = 120/0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])


    def getGrossDuration(self, nbCassette):
        if self.useFixedData:
            return 1
        data: case_data = self.case_data_dict[self.selectedCategory]
        # Generate Distribution:
        n = data.grossAverageMinsPerCassette * 60 * nbCassette / 0.6
        p = 0.6
        s = np.random.binomial(n, p, 1)
        return int(s[0])


    def getProcessDuration(self):
        return 6 * 60

    def getEmbeddingDuration(self):
        if self.useFixedData:
            return 1
        try:
            data = self.work_station_time_dict[self.selectedCategory, 'embedding']
            # Generate Distribution:
            n = data * 0.7 / 0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])
        except KeyError:
            n = 1.5*60/0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])

    def getSectionDuration(self, nbSlide):
        if self.useFixedData:
            return 1
        try:
            data = self.work_station_time_dict[self.selectedCategory, 'section']
            # Generate Distribution:
            n = data/ 0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])
        except KeyError:
            n = 3*60/0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])

    def getSignoutDuration(self):
        if self.useFixedData:
            return 1
        #data: case_data = self.case_data_dict[self.selectedCategory]
        try:
            data = self.work_station_time_dict[self.selectedCategory, 'signout']
            n = 40 / 0.6

            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])
        except KeyError:
            n = 1 * 60 / 0.6
            p = 0.6
            s = np.random.binomial(n, p, 1)
            return int(s[0])

    @staticmethod
    def resource_data():
        eventResourceSet = {}
        nbStaffs = {'register': 24, 'grossing':14, 'accessioning':14,'processing':2000,
                    'embedding':35, 'section':35, 'sighout':33, 'finish':10}
        for stage, number in nbStaffs.items():
            staffs = []
            for i in range(number):
                staffs.append(f'{stage}_{i}')
            eventResourceSet[stage] = resource_data(resource_names=staffs)
        return eventResourceSet

    def nbSpecimenPerAccess(self):
        data: case_data = self.case_data_dict[self.selectedCategory]
        p = 0.6
        n = data.nbSpecimenPerAccess / p
        s = np.random.binomial(n, p, 1)
        if s[0] < 1:
            s[0] = 1
        return int(s[0])

    def nbCassetPerSpecimen(self):
        if self.useFixedData:
            return 2
        data: case_data = self.case_data_dict[self.selectedCategory]
        p = 0.6
        n = data.nbCassettePerSpecimen / p
        s = np.random.binomial(n, p, 1)
        if s[0] < 1:
            s[0] = 1
        return int(s[0])

    def nbSlidesPerCassette(self):
        if self.useFixedData:
            return 2
        data: case_data = self.case_data_dict[self.selectedCategory]
        p = 0.6
        n = data.nbSlidePerSection / p
        s = np.random.binomial(n, p, 1)
        if s[0] < 1:
            s[0] = 1
        return int(s[0])


    @classmethod
    def batchSet(cls, subCategory):
        two_hour_prob = cls.case_data_dict[subCategory].two_hour_prob
        prob = random.uniform(0, 1)
        if prob < two_hour_prob:
            return cls.batch_filter
        else:
            return [cls.batch_filter[1]]

    @classmethod
    def batchSetHistory(cls, isTwoHour):

        if isTwoHour:
            return cls.batch_filter
        else:
            return [cls.batch_filter[1]]

    @classmethod
    def doSecondRoundOfSection(cls):
        if cls.useFixedData:
            return False
        doSecondSectionRound = random.choices([True, False], weights=[0.2, 0.9],k=1)[0]
        return doSecondSectionRound

    @classmethod
    def nbAdditionalBlock(cls):
        s = random.choices([1, 2, 3], weights=[0.7, 0.2, 0.1],k=1)
        return s[0]

    @classmethod
    def loadCaseArrivalData(cls, log):
        cls.arrivalLog = log
        print(log.info())

    @classmethod
    def fillNanPriority(cls, row):
        if row.speciality == -1:
            row.speciality = random.choices(cls.case_categories, cls.case_weights, k=1)[0]
        if row.priority == -1:
            two_hour_prob = cls.case_data_dict[row.speciality].two_hour_prob
            row.priority = random.choices(['2 Hours', '9 Hours'], [two_hour_prob, 1-two_hour_prob], k=1)[0]
        specimen_sub_speciality = row.specimen_sub_speciality
        specimen_spec_type = row.specimen_spec_type
        if specimen_sub_speciality != -1 and specimen_spec_type != -1 and 'null' not in specimen_sub_speciality and 'null' not in specimen_spec_type :
            a = specimen_sub_speciality.find('[')
            b = specimen_sub_speciality.find(']')
            subString = specimen_sub_speciality[a+1:b]
            specialityList = subString.split(",")
            row.specimen_sub_speciality = [s.strip() for s in specialityList]
            a = specimen_spec_type.find('[')
            b = specimen_spec_type.find(']')
            subString = specimen_spec_type[a+1:b]
            spec_type= subString.split(",")

            row.specimen_spec_type = [s.strip() for s in spec_type]
        else:
            row.specimen_sub_speciality = [row.speciality]
            if row.speciality in cls.grossing_dict:
                row.specimen_spec_type = [list(cls.grossing_dict[row.speciality].keys())[0]]
            else:
                row.specimen_sub_speciality = ['Skin']
                row.specimen_spec_type = ['SKPBX']
            row.case_specimen_count = 1
            row.case_cassette_count = 1
            row.case_slide_count = 1
            # need to modify bad data
        row.case_specimen_count = len(row.specimen_sub_speciality)
        if row.case_cassette_count <= row.case_specimen_count :
            row.case_cassette_count =  row.case_specimen_count
        if row.case_slide_count<= row.case_cassette_count:
            row.case_slide_count = 1* row.case_cassette_count
        assert row.case_specimen_count == len(row.specimen_sub_speciality), f'{row.case_key}'
        return row
    @classmethod
    def get_grossing_from_dict(cls, speciality, spec_type):
        if cls.useFixedData:
            return 1
        if speciality in cls.grossing_dict and spec_type in cls.grossing_dict[speciality]:
            return cls.grossing_dict[speciality][spec_type]
        return 5 * 60

    @classmethod
    def getNextCase(cls):
        # firt fill all null value of speciality
        # then fill all priority values based on specialty
        cls.arrivalLog.fillna(-1, inplace=True)
        cls.arrivalLog = cls.arrivalLog.apply(LabData.fillNanPriority, axis=1, result_type='broadcast')
        assert(cls.arrivalLog['speciality'].isna().sum() == 0)
        assert(cls.arrivalLog['priority'].isna().sum() == 0)
        #cls.arrivalLog['speciality'].fillna(random.choices(cls.case_categories, cls.case_weights, k=1)[0], inplace=True)
        #cls.arrivalLog['priority'].fillna(LabData.fillNanPriority(cls.arrivalLog.speciality), inplace=True)
        #cls.arrivalLog.apply(lambda x: random.choices(cls.case_categories, cls.case_weights, k=1)[0] if np.isnan(x['speciality']) else x['speciality'], axis=1)
        for index, row in cls.arrivalLog.iterrows():
            arrivalTime = datetime(row.year,row.month,row.day,row.hour,row.minutes,row.seconds)
            arrivalTimeInSeconds = int((arrivalTime - SimSetting.start_time_sim).total_seconds())
            isTwoHour = False
            # if speciality is null, choose based on probability
            speciality = row.speciality
            # if priority is null, choose based on probability
            priority = row.priority

            if priority == '2 Hours':
                isTwoHour = True



            if row.case_specimen_count > len(row.specimen_sub_speciality):
                row.case_specimen_count = len(row.specimen_sub_speciality)
            assert(len(row.specimen_sub_speciality) == len(row.specimen_sub_speciality))
            data = history_data(index=index, arrivalTime=int(arrivalTimeInSeconds), isTwoHour=isTwoHour, speciality=speciality,
                                isAdditionalNeeded=row.isAdditionalNeeded,
                                specimen_spec_type=row.specimen_spec_type,
                                specimen_sub_speciality=row.specimen_sub_speciality,
                                case_specimen_count = int(row.case_specimen_count),
                                case_cassette_count = int(row.case_cassette_count),
                                case_slide_count= int(row.case_slide_count))

            yield data

    @classmethod
    def artifical_roster(cls):
        results = []
        # now for every row output staffs
        minTimeIndex = 6
        maxTimeIndex = 24

        weeks = [18, 19, 20, 21]
        days = [0, 1, 2, 3, 4, 5, 6]
        print('weeks are', weeks)
        stations = ['accessioning', 'grossing', 'embedding', 'section', 'signout']
        staffs = {'accessioning': 4, 'grossing': 11, 'embedding': 5, 'section': 15, 'signout': 8}
        timeList = [i for i in range(minTimeIndex, maxTimeIndex)]
        for idx, week in enumerate(weeks):
            dayCounterOffset = 7 * idx
            for day in days:

                for idx, timeIndx in enumerate(timeList):

                    for station in stations:

                        nbStaff = staffs[station]
                        # print(f'day {day+dayCounterOffset} time {timeIndx} station {station} nb={nbStaff}')
                        if nbStaff > 0:
                            staffList = []

                            for i in range(nbStaff):
                                staffList.append(f'{station}_{i}')
                            results.append([day + dayCounterOffset, idx, station, staffList])
        # I want to restore the modified roster as dataframe

        dayList = list([i for i in range(len(weeks) * 7)])

        if True:
            processingList = [f'processing_{i}' for i in range(2400)]

            for day in dayList:
                for idx, t in enumerate(timeList):
                    results.append([day, idx, 'processing', processingList])
            print('time hour ids are ', timeList)
        print('day ids are', dayList)
        dataLog = pd.DataFrame(results, columns=['day', 'timeIndex', 'transition', 'resource_set'])
        dataLog = dataLog.sort_values(["day", "timeIndex", "transition"], ascending=(True, True, True))
        dataLog.to_csv('day_roster.csv', index=False)
        # calcualte average maek span

        results.clear()
        for day in dayList:
            for idx,t in enumerate(timeList):
                results.append([day, idx, t, t + 1])
        dayTimeLog = pd.DataFrame(results, columns=['day', 'timeIndex', 'start', 'end'])
        dayTimeLog = dayTimeLog.sort_values(["day", "timeIndex"], ascending=(True, True))
        dayTimeLog.to_csv('dayTimeMap.csv', index=False)
        return dataLog, dayTimeLog

    @classmethod
    def halfHourRoster(cls):
        results = []

        weeks = [18, 19, 20, 21]
        days = [0, 1, 2, 3, 4, 5, 6]
        print('weeks are', weeks)
        stations = ['accessioning', 'grossing', 'embedding', 'section', 'signout']

        staffs = {'accessioning': 4, 'grossing': 11, 'embedding': 5, 'section': 15, 'signout': 8}
        # create a roster for every 3 hour from 0-24
        step = 0.5


        for idx, week in enumerate(weeks):
            dayCounterOffset = 7 * idx
            for day in days:

                for timeIndx in range(5,40):

                    for station in stations:

                        nbStaff = staffs[station]

                        # print(f'day {day+dayCounterOffset} time {timeIndx} station {station} nb={nbStaff}')
                        if nbStaff > 0:
                            staffList = []

                            for i in range(nbStaff):
                                staffList.append(f'{station}_{i}')
                            results.append([day + dayCounterOffset, timeIndx-5, station, staffList])
        # I want to restore the modified roster as dataframe
        columns = ['year', 'week', 'day', 'timeIndex']

        dayList = list([i for i in range(len(weeks) * 7)])
        timeList = [i for i in range(5,40)]
        if True:
            processingList = [f'processing_{i}' for i in range(2400)]

            for day in dayList:
                for t in timeList:
                    results.append([day, t-5, 'processing', processingList])
            print('time hour ids are ', timeList)
        print('day ids are', dayList)
        dataLog = pd.DataFrame(results, columns=['day', 'timeIndex', 'transition', 'resource_set'])
        dataLog = dataLog.sort_values(["day", "timeIndex", "transition"], ascending=(True, True, True))
        dataLog.to_csv('day_roster.csv', index=False)
        # calcualte average maek span

        results.clear()
        for day in dayList:
            for t in timeList:
                results.append([day, t-5, 5+(t-5) * step, 5+step*(t -5) + 0.5])
        dayTimeLog = pd.DataFrame(results, columns=['day', 'timeIndex', 'start', 'end'])
        dayTimeLog = dayTimeLog.sort_values(["day", "timeIndex"], ascending=(True, True))
        dayTimeLog.to_csv('dayTimeMap.csv', index=False)
        return dataLog, dayTimeLog


    @classmethod
    def chunk_roster(cls):
        results = []

        weeks = [18, 19, 20, 21]
        days = [0, 1, 2, 3, 4, 5, 6]
        print('weeks are', weeks)
        stations = ['accessioning', 'grossing', 'embedding', 'section', 'signout']

        staffs = {'accessioning': 4, 'grossing': 11, 'embedding': 5, 'section': 15, 'signout': 8}
        # create a roster for every 3 hour from 0-24
        step = 1


        for idx, week in enumerate(weeks):
            dayCounterOffset = 7 * idx
            for day in days:

                for timeIndx in range(5,24):

                    for station in stations:

                        nbStaff = staffs[station]

                        # print(f'day {day+dayCounterOffset} time {timeIndx} station {station} nb={nbStaff}')
                        if nbStaff > 0:
                            staffList = []

                            for i in range(nbStaff):
                                staffList.append(f'{station}_{i}')
                            results.append([day + dayCounterOffset, timeIndx-5, station, staffList])
        # I want to restore the modified roster as dataframe
        columns = ['year', 'week', 'day', 'timeIndex']

        modifiedLog = []
        for idx, week in enumerate(weeks):
            for day in days:
                for timeIndx in range(5,24):
                    data = [2021, week, day, timeIndx]

                    for station in stations:
                        data.append(staffs[station])
                    modifiedLog.append(data)
        columns = columns + stations
        modifiedLog = pd.DataFrame(modifiedLog, columns=columns)
        modifiedLog = modifiedLog.sort_values(['week', 'day', 'timeIndex'], ascending=(True, True, True))
        modifiedLog.to_csv('modified-roster.csv', index=False)
        dayList = list([i for i in range(len(weeks) * 7)])
        timeList = [i for i in range(5,24)]
        if True:
            processingList = [f'processing_{i}' for i in range(2400)]

            for day in dayList:
                for t in timeList:
                    results.append([day, t-5, 'processing', processingList])
            print('time hour ids are ', timeList)
        print('day ids are', dayList)
        dataLog = pd.DataFrame(results, columns=['day', 'timeIndex', 'transition', 'resource_set'])
        dataLog = dataLog.sort_values(["day", "timeIndex", "transition"], ascending=(True, True, True))
        dataLog.to_csv('day_roster.csv', index=False)
        # calcualte average maek span

        results.clear()
        for day in dayList:
            for t in timeList:
                results.append([day, t-5, step*t, step*(t + 1)])
        dayTimeLog = pd.DataFrame(results, columns=['day', 'timeIndex', 'start', 'end'])
        dayTimeLog = dayTimeLog.sort_values(["day", "timeIndex"], ascending=(True, True))
        dayTimeLog.to_csv('dayTimeMap.csv', index=False)
        return dataLog, dayTimeLog

    @classmethod
    def staffRosterCovert(cls):
        # add 2 weeks of roster



        print(SimSetting.rosterWeekDayList)


        print(SimSetting.weekBlockStart)


        staffRosterDict = {}


        staff = StaffShift(name='Nerrisa', stage =embedding)

        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 20)
        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict['Nerrisa'] = staff

        staff = StaffShift(name='Binila', stage =embedding)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict['Binila'] = staff

        staff = StaffShift(name='Mital', stage =embedding)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict['Mital'] = staff

        name = 'CV'
        staff = StaffShift(name=name, stage =embedding)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)
        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Mike'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Pearl'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff


        name = 'Annie'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Carol'
        staff = StaffShift(name=name, stage = section)
        for i in range(1, SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'AM'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Jasmine'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Palak'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Natasha'
        staff = StaffShift(name=name, stage = section)
        for i in range(SimSetting.rosterTotalDays):

            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        name = 'Erwyn'
        staff = StaffShift(name=name, stage = access)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff
        
        name = 'Anupama'
        staff = StaffShift(name=name, stage = access)
        for i in range(SimSetting.rosterTotalDays):
            if i % 7 in [6]: continue
            add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

        # staffRosterDict key by staff name, store shift start and finish info

        staffRosterDict[name] = staff

        grossName = 'grossing_'
        for j in range(10):
            name = f'{grossName}{j}'
            staff = StaffShift(name=name, stage =grossing)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in [6]: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)

            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff

        signoutName = 'signout_'
        for j in range(4):
            name = f'{signoutName}{j}'
            staff = StaffShift(name=name, stage =signout)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in [6]: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)
            staffRosterDict[name] = staff

        embedName = 'embed_'
        for j in range(2):
            name = f'{embedName}{j}'
            staff = StaffShift(name=name, stage =embedding)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in [6]: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)
            staffRosterDict[name] = staff

        sectionName = 'section_'
        for j in range(4):
            name = f'{sectionName}{j}'
            staff = StaffShift(name=name, stage=section)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in [6]: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=8), 10)
            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff

        df = rosterDF(staffRosterDict)
        if False:
            cls.show_roster(df)
        return staffRosterDict, df, cls.weekDayList


    @classmethod
    def batch_roster(cls):
        batch_roster_dict = {}
        offDays = [5, 6]
        for idx, value in cls.batchTeamShift.items():
            staff = StaffShift(name=idx, stage='processing')
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=value[0]), value[1] - value[0])
            batch_roster_dict[idx] = staff
        #print(batch_roster_dict)
        return batch_roster_dict

    @classmethod
    def geneticInitialPool(cls, initialSolution=None, stage='grossing', capacity=10, startTimeArray=[5], duration=8):
        # three start times 8, 10 , 12 for every staff, every local solution is to try all three solutions for every staff,
        # if there is an improvment keep it

        initialPool = {}

        for lastStaffStartAtEight in range(capacity - 1):
            for lastStaffStartAtTen in range(lastStaffStartAtEight , capacity):
                staffRosterDict, _, _ = cls.staffRosterLocal(firstStaffDivider=lastStaffStartAtEight,
                                                             secondStaffDivider=lastStaffStartAtTen, stage=stage,
                                                             initialSolution=initialSolution,
                                                             startTimeArray=startTimeArray, duration=duration,
                                                             capacity=capacity)
                initialPool[lastStaffStartAtEight, lastStaffStartAtTen] = staffRosterDict
        return initialPool

    @classmethod
    def staffRosterTest(cls):
        # add 2 weeks of roster

        print(SimSetting.rosterWeekDayList)

        print(SimSetting.weekBlockStart)

        staffRosterDict = {}

        offDays = [5, 6]


        sectionName = f'{access}_'
        for j in range(2):
            name = f'{sectionName}{j}'
            staff = StaffShift(name=name, stage=access)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 12)
            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff


        grossName = f'{grossing}_'

        for j in range(10):
            name = f'{grossName}{j}'
            staff = StaffShift(name=name, stage=grossing)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 8.5)

            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff

        signoutName = f'{signout}_'
        for j in range(2):
            name = f'{signoutName}{j}'
            staff = StaffShift(name=name, stage=signout)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 8)
            staffRosterDict[name] = staff

        embedName = f'{embedding}_'
        for j in range(5):
            name = f'{embedName}{j}'
            staff = StaffShift(name=name, stage=embedding)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 12)
            staffRosterDict[name] = staff

        sectionName = f'{section}_'
        for j in range(5):
            name = f'{sectionName}{j}'
            staff = StaffShift(name=name, stage=section)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 12)
            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff

        df = rosterDF(staffRosterDict)
        if False:
            cls.show_roster(df)
        return staffRosterDict, df, SimSetting.rosterWeekDayList

    @classmethod
    def create_job_pattern_weekly(cls):
        # create job pattern that changes every week
        cls.jobDistribution.job_data.clear()
        nbWeeks = SimSetting.jobTotalDays // 7

        rate_dict = {}
        for w in range(nbWeeks):
            rate_dict[w] = {}

            if w == 0:
                for c in range(6, 18):
                    rate_dict[w][c] = 5
            # for first week, most job arrive in the morning
            if w == 1:
                for c in range(6, 12):
                    rate_dict[w][c] = 10
                rate_dict[w][12] = 40
                rate_dict[w][13] = 40
                rate_dict[w][14] = 70
                rate_dict[w][15] = 80
                rate_dict[w][16] = 70
                rate_dict[w][17] = 90
                # for the following week, most job arrive in the morning
            elif w > 1:
                rate_dict[w][6] = 20
                rate_dict[w][7] = 80
                rate_dict[w][8] = 70
                rate_dict[w][9] = 40
                rate_dict[w][10] = 40
                rate_dict[w][11] = 30
                rate_dict[w][12] = 30
                rate_dict[w][13] = 10
                rate_dict[w][14] = 10
                rate_dict[w][15] = 5
                rate_dict[w][16] = 5
                rate_dict[w][17] = 5

        for i in range(SimSetting.jobTotalDays):
            for j in range(6, 18):
                newDate = SimSetting.start_time_sim + timedelta(days=i)
                weekIndex =  i // 7
                rate = rate_dict[weekIndex][j]
                cls.jobDistribution.upsert_job_distribution(newDate.year, newDate.month, newDate.day, j, rate)
        df = []
        for key, value in cls.jobDistribution.job_data.items():
            df.append([datetime(year=key[0], month=key[1], day=key[2], hour=key[3]), value])

        return cls.jobDistribution, df

    @classmethod
    def create_job_pattern(cls):
        cls.jobDistribution.job_data.clear()
        rate_dict = {}
        for c in range(6,12):
            rate_dict[c] = 10
        rate_dict[12] = 40
        rate_dict[13] = 40
        rate_dict[14] = 70
        rate_dict[15] = 80
        rate_dict[16] = 70
        rate_dict[17] = 90
        for i in range(SimSetting.jobTotalDays):
            for j in range(6,18):
                newDate = SimSetting.start_time_sim + timedelta(days=i)
                rate =  rate_dict[j]
                if newDate < (SimSetting.start_time_sim + timedelta(days=SimSetting.backlogWeek * 7)):
                    rate = 5
                cls.jobDistribution.upsert_job_distribution(newDate.year, newDate.month, newDate.day, j, rate)
        df = []
        for key, value in cls.jobDistribution.job_data.items():

            df.append([datetime(year=key[0], month=key[1], day=key[2], hour=key[3]),value])
            
        return cls.jobDistribution, df



    @classmethod
    def create_jobs(cls, jobHourRate):
        dailyRate = 200
        job_arrival_dict = {}
        hourList = [i for i in range(6,18)]
        for i in range(SimSetting.jobTotalDays):
            job_arrival_dict[i] = []
            #normalise weight at each hour
            newDate = SimSetting.start_time_sim + timedelta(days=i)
            hourRates = [jobHourRate.job_data[newDate.year, newDate.month, newDate.day, h] for h in range(6, 18)]

            for idx, rate in enumerate(hourRates):
                # even distribute jobs in this hour

                job_arrival_interval = int(3600 / rate)
                for job in range(rate):
                    jobTime = SimSetting.start_time_sim + timedelta(days=i, hours=hourList[idx],seconds=job_arrival_interval*job)
                    job_arrival_dict[i].append(jobTime)
        return job_arrival_dict
    @classmethod
    def get_next_job_rate(cls, job_arrival_array):
        for t in job_arrival_array:
            yield t


    @classmethod
    def create_fake_data_by_density(cls, job_arrival_dict):
        index = 0
        for d in range(SimSetting.jobTotalDays):

            for t in cls.get_next_job_rate(job_arrival_array=job_arrival_dict[d]):

                isTwoHour = random.choices([True, False], weights=[0.2, 0.9], k=1)[0]
                if (t.weekday() in [5,6]): continue
                arrivalTimeInSeconds = int((t - SimSetting.start_time_sim).total_seconds())
                data = history_data(index=index, arrivalTime=int(arrivalTimeInSeconds), isTwoHour=isTwoHour,
                                    speciality='Skin',
                                    isAdditionalNeeded=False,
                                    specimen_spec_type='SKSHV',
                                    specimen_sub_speciality='Skin',
                                    case_specimen_count=2,
                                    case_cassette_count=2,
                                    case_slide_count=2)

                index += 1
                yield data

    @classmethod
    def staffRosterLocal(cls, firstStaffDivider = 0, secondStaffDivider = 0,
                         initialSolution = None, stage = 'grossing', startTimeArray = [6, 10, 12], duration = 8.5, capacity = 10):
        # add 2 weeks of roster
        staffRosterDict = dict(initialSolution)

        offDays = [5, 6]



        stageName = f'{stage}_'

        for j in range(capacity):
            name = f'{stageName}{j}'
            staff = StaffShift(name=name, stage=stage)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                if j < firstStaffDivider:
                    hours = startTimeArray[0]
                else:
                    if j < secondStaffDivider:
                        hours = startTimeArray[1]
                    else:
                        hours = startTimeArray[2]
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=hours), duration)

            staffRosterDict[name] = staff




        df = rosterDF(staffRosterDict)
        if showRoster:
            cls.show_roster(df)
        return staffRosterDict, df, SimSetting.rosterWeekDayList

    @classmethod
    def show_roster(cls, df):
        fig = px.timeline(df, x_start="Start", x_end="Finish", y="Task", color="Resource")
        fig.update_yaxes(autorange="reversed")
        fig.show()


# must run this to create job arrival pattern
jobDistribution, df = LabData.create_job_pattern()


#staffRosterDict, df, weekDayList = LabData.staffRosterTest()
#print(weekDayList)
#grouped_roster = LabData.group_roster_by_date(staffRosterDict)
#print(grouped_roster)create_job_pattern
#rostertoCoverage(staffRosterDict, LabData.stations)
#LabData.halfHourRoster()

#covert_df_job_pattern( df)
#job_arrival_dict = LabData.create_jobs(LabData.jobDistribution)
#LabData.create_fake_data_by_density(job_arrival_dict)
#df.to_csv('test.csv', index=False)
#LabData.load_roster_df(df)
#staffRosterDict, df, weekDayList = LabData.staffRosterCovert()

#_,_, df = LabData.rostertoCoverage(staffRosterDict)

#LabData.load_roster_df(df)
#print(staffRosterDict)
#LabData.batch_roster()
x = [2.12, 1.68, 1.687, 1.85, 1.51]
y = []
for i in x:
    y.append(pd.to_timedelta(f'{i}days'))
print(y)
