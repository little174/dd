import pandas as pd
import numpy as np
from simPyExtentionSkillSet import *
td = np.timedelta64(12, 'h')
seconds = td/ np.timedelta64(24, 'h')
def import_csv(file_path):
    print(f'start loading {file_path}')

    eventlog = pd.read_csv(file_path, sep=',', skipinitialspace=True)
    return eventlog
# p = 0, only night batch, p =1, mid and night both
df = import_csv('single-round.csv')

df.loc[:,'start'] = pd.to_datetime(df.start)
df.loc[:,'finish'] = pd.to_datetime(df.finish)

df.loc[:,'p'] = 0
df.loc[df.priority == "['processing_0', 'processing_1200']", 'p'] = 1

df_start = df[df.event == 'register'].copy()
df_start = df_start.set_index('case_id')
df_start = df_start['p']
df_start.to_csv('test1.csv', index=True)
df_finish = df[df.event == 'finish']


d = df.groupby(['case_id']).start.min()
e = df.groupby(['case_id']).finish.max()
d =  d.reset_index()
e = e.reset_index()
d = d.set_index('case_id')
e = e.set_index('case_id')
print(d.index)
case_df = pd.merge(d,e, left_index=True, right_index=True)
print(case_df.index)
case_df = pd.merge(case_df, df_start, left_index=True, right_index=True)
case_df.loc[:,'duration'] = (case_df.finish - case_df.start)


td = case_df.duration.values
seconds = td/ np.timedelta64(24, 'h')
case_df['days'] = seconds
print(seconds)  # Output: 7200.0

case_df = case_df.sort_values(['start'])
case_df.loc[:,'minute'] = case_df.start.dt.minute
case_df.loc[:,'hour'] = case_df.start.dt.hour
case_df.loc[:,'month'] = case_df.start.dt.month
case_df.loc[:,'dayofweek'] = case_df.start.dt.dayofweek
case_df.loc[:,'day'] = case_df.start.dt.day
print(case_df.groupby(['p']).p.count())
print(case_df.info())
case_df.to_csv('test.csv', index=True)

#print(df_start.dayofweek)

features = ['month', 'dayofweek', 'p', ]
