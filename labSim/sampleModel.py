from labEventDistribution import *

from pm4py.visualization.petri_net.variants.token_decoration_frequency import *
from pm4py.objects.petri_net import semantics
from pm4py.objects.petri_net.utils import petri_utils as utils
import csv
from copy import copy
from  pm4py.visualization.petri_net  import visualizer as pn_visualizer
from pm4py.objects.process_tree import obj
from pm4py.algo.discovery.inductive.util import tree_consistency
from pm4py.visualization.process_tree import visualizer as pt_visualizer
from pm4py.objects.conversion.process_tree import converter as pt_converter

task_type = collections.namedtuple('task_type', 'transitions weights')
resource_data = collections.namedtuple('event_name', 'resource_names')

multiChoiceTransTag  = 'multichoice'
reworkTransTag = 'rework'
reworkUppboundTag = 'reworkBound'
startEventTag = 'start'
resourceTag = 'resourceTag'
onSpark = False
durationTag = 'duration'
durationInfo = 'durationInfo'

class MyToken():
    def __init__(self, place: PetriNet.Place = None, parent = None, name = None):


        self.place = place
        self.id = genuid()
        self.parent = parent
        self.name = name
        self.label = None
        # a token comes from a unique tranisions
        self.parentTrans: 'MyTransition' = None
        self.birth_time = None
    def set_birth(self, time):
        self.birth_time = time
    def parent_trans(self, trans):
        self.parentTrans = trans

    def show_parent(self):
        debug_output(f'parents are ')
        for parent in self.parent:
            debug_output(f'parent')
    def __hash__(self):
        return self.id
    # str call repr?
    def __repr__(self):
        if self.label is None:
            return "( id =" + str(self.id) + ' ' + str(self.name) + ")"
        else:
            return "(" + str(self.name) + ", '" + str(self.label) + "')"

class MyTransition():
    def __init__(self, trans : PetriNet.Transition = None, eventIdGenerator = None):
        self.transition = trans
        self.id = next(eventIdGenerator)
        self.parent = None
        self.birth = None
        self.duration = 0

    def set_duration(self, duration):
        self.duration = duration
    def finish_time(self):
        return self.birth + self.duration

    def set_birth(self, time):
        self.birth = time
    def __hash__(self):
        return self.id
    def __repr__(self):
        if self.transition:
            return "(" + self.transition.name +")"
    def set_parent(self, parent):
        self.parent = (parent)
    def get_parent(self):
        return self.parent
    def __str__(self):
        return self.__repr__()
class MyMarking(Marking):
    def __init__(self,   iterable=None, /, **kwds):
        super().__init__()
        self.id = genuid()


def debug_output( str, show = False):
    show =  False
    if show:
        print(str)


def create_resource_file_with_skillset(resources):
    results = []
    for r,value in resources.items():
        x : resource_data = value
        data = [r, x.resource_names]
        results.append(data)

    f = open('resourceSkillSet.csv', 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        ['transition', 'resource_set'])
    for row in results:
        thewriter.writerow(row)
    f.close()


def create_resource_file(resources):
    results = []
    for r,value in resources.items():
        data = [r, value[0], value[1]]
        results.append(data)

    f = open('resource.csv', 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        ['transition', 'resource', 'capacity'])
    for row in results:
        thewriter.writerow(row)
    f.close()

def labProcess(sampleData: history_data = None):
    labDataObj = LabData()
    labDataObj.setCategory(sampleData.speciality)

    metaData = {}
    metaData[durationInfo] = {}
    reworkTransisions = []
    resources = {}
    orTransisions = []  # lit of tuples
    eventResourceSet = {}

    seq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=None, children=None, label=None)
    trans0 = obj.ProcessTree(operator=None, parent=seq, children=None, label='register')
    metaData[durationInfo]['register'] = labDataObj.getRegDuration()
    trans1 = obj.ProcessTree(operator=None, parent=seq, children=None, label='accessioning')
    seq.children.append(trans0)
    seq.children.append(trans1)


    multiSpecimen = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=seq, children=None, label=None)
    seq.children.append(multiSpecimen)
    # nbSpecimen determins the access time,

    # nbCassettes determins grossing time, nbCassettes per specimen 
    # then each cassette go through processing, embedding...sectioning
    # sectioning is determined by number of slides per cassette?
    #nbSpecimen = labDataObj.nbSpecimenPerAccess()
    nbSlides = sampleData.case_slide_count
    nbSpecimen = int(sampleData.case_specimen_count)
    totalCassettes = sampleData.case_cassette_count
    accessDuration = labDataObj.getAccessDuration(nbSpecimen)
    metaData[durationInfo]['accessioning'] = accessDuration
    # each specimen go through a grossing stage to crate multiple casseets
    # need to record section ids and associated slides
    sectionIds = {}
    #work out how many cassesstes are created by each speimen
    cassetteCountEachSpeimen = []
    totalCassettesAssigned= 0
    # first assign the whole chunck for each specimen as much as possible uniformlly
    for i in range(nbSpecimen):
        v = int(sampleData.case_cassette_count // sampleData.case_specimen_count)
        totalCassettesAssigned += v
        cassetteCountEachSpeimen.append(v)
    # assign the left cassettes to each specimen one by one
    specimenIdx = 0
    for j in range(totalCassettes -totalCassettesAssigned):
        cassetteCountEachSpeimen[specimenIdx] = cassetteCountEachSpeimen[specimenIdx] + 1
        specimenIdx += 1
        specimenIdx = specimenIdx % nbSpecimen


    averageSlidePerCassette = nbSlides // totalCassettes
    slideEachCassette = []
    totalSlidesAssigned= averageSlidePerCassette * totalCassettes
    for i in range(totalCassettes):
        slideEachCassette.append(averageSlidePerCassette)

    cassetteIndex = 0
    for i in range(nbSlides - totalSlidesAssigned):
        slideEachCassette[cassetteIndex] += 1
        cassetteIndex +=1
        cassetteIndex = cassetteIndex % totalCassettes


    # I want to know how many slides are created for each cassette

    cassetteIndex = 0
    for s in range(nbSpecimen):
        specimenSeq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multiSpecimen, children=None, label=None)
        multiSpecimen.children.append(specimenSeq)
        #print(s)
        #print(sampleData.specimen_sub_speciality, sampleData.specimen_spec_type)
        grossingDuration = LabData.get_grossing_from_dict(sampleData.specimen_sub_speciality[s], sampleData.specimen_spec_type[s])

        grossing(specimenSeq, index=s, labDataObj = labDataObj, cassetteCount= cassetteCountEachSpeimen[s],cassetteCountIndex=cassetteIndex, slideEachCassette=slideEachCassette, sectionIds = sectionIds,  durationData = metaData[durationInfo],grossDuration =  grossingDuration)
        cassetteIndex += cassetteCountEachSpeimen[s]
    finish = obj.ProcessTree(operator=None, parent=seq, children=None, label='finish')
    metaData[durationInfo]['finish'] = 0
    seq.children.append(finish)

    # the finish is the first round finish, we may have a second round of sections
    doSecondRoundOfSection = labDataObj.doSecondRoundOfSection()
    doSecondRoundOfSection= sampleData.isAdditionalNeeded
    if doSecondRoundOfSection:
        secondRoundSection(tree = seq, labDataObj=labDataObj, sectionIds=sectionIds, durationData= metaData[durationInfo])

    tree = seq
    # tree_consistency.fix_parent_pointers(tree)
    # tree = generic.fold(tree)
    # generic.tree_sort(tree)
    gviz = pt_visualizer.apply(tree)
    #pt_visualizer.view(gviz)
    #pt_visualizer.save(gviz, 'tree.png')
    net, initial_marking, final_marking = pt_converter.apply(tree)
    parameters = {pn_visualizer.Variants.FREQUENCY.value.Parameters.FORMAT: "png"}
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    #pt_visualizer.view(gviz)


    p_1 = None
    for p in net.places:
        place: PetriNet.Place = p
        #print(p.name)
        if place.name == 'source':
            p_1 = place
            break

    capacity = 8
    t_1 = None
    for t in net.transitions:
        trans: PetriNet.Transition = t
        t.name = t.label
        #print(t.label)


        if trans.label == trans0.label:
            t_1 = trans
        resources[trans.label] = (trans.label, capacity)

    staffArrays = []
    counter = 0
    for i in range(len(net.transitions)):
        staffList = []
        for j in range(capacity):
            name = 'Oddo' + str(counter)
            counter += 1
            staffList.append(name)
        #counter = counter - 2
        staffArrays.append(staffList)
    #print('im done')

    counter = 0
    for t in net.transitions:
        # printing n elements from list
        label = t.label
        eventResourceSet[label] = resource_data(resource_names=staffArrays[counter])
        counter += 1


    metaData[resourceTag] = LabData.resource_data()
    metaData[startEventTag] = [t_1]

    initial_marking = Marking()
    final_marking = Marking()
    tokenMarking = MyMarking()
    # create a token at a place
    token1 = MyToken(place=p_1, name='p_1')
    # token2 = MyToken(place=p_2, name='p_2')
    tokenMarking[token1] = token1
    # tokenMarking[token2] = token2
    initial_marking[p_1] = 1

    metaData[multiChoiceTransTag] = orTransisions
    metaData[reworkTransTag] = []
    metaData[reworkUppboundTag] = {}
    metaData[durationTag] = labDataObj
    for trans in reworkTransisions:
        metaData[reworkUppboundTag][trans] = 4
    return net, initial_marking, tokenMarking, metaData

def secondRoundSection(tree = None, labDataObj: LabData = None, sectionIds = {}, durationData= None):
    waitAdditional = obj.ProcessTree(operator=None, parent=tree, children=None, label=f'waitAdditional')
    durationData['waitAdditional'] = labDataObj.getWaitAdditionalDuration()
    tree.children.append(waitAdditional)
    additionalRequest = obj.ProcessTree(operator=None, parent=tree, children=None, label=f'additionalRequest')
    durationData['additionalRequest'] = labDataObj.getAdditionalRequestDuration()
    tree.children.append(additionalRequest)
    # generate a random list of additional reqeuws
    sectionKeys = list(sectionIds.keys())
    #print(sectionKeys)
    # generate a random integer as the nb of additional section blocks
    nbAdditionalBlock = labDataObj.nbAdditionalBlock()
    if nbAdditionalBlock > len(sectionKeys):
        nbAdditionalBlock = len(sectionKeys)
    # randoly select the addtional blocks based on this number
    secondSections = random.sample(sectionKeys,  nbAdditionalBlock)
    #print(secondSections)
    multiSection = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=tree, children=None, label=None)
    tree.children.append(multiSection)
    for s in secondSections:
        newSectionSeq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multiSection, children=None, label=None)
        multiSection.children.append(newSectionSeq)
        section = obj.ProcessTree(operator=None, parent=newSectionSeq, children=None, label=f'{s}_second')
        duration = labDataObj.getSectionDuration(1)
        durationData[f'{s}_second'] = duration
        newSectionSeq.children.append(section)
        # for now just one slide
        x = s.find('_')
        slideIndex = s[x+1:]

        slide = obj.ProcessTree(operator=None, parent=newSectionSeq, children=None, label=f'signout_{slideIndex}_{len(sectionIds[s])}')
        durationData[f'signout_{slideIndex}_{len(sectionIds[s])}'] = labDataObj.getSignoutDuration()
        newSectionSeq.children.append(slide)

    secondFinish = obj.ProcessTree(operator=None, parent=tree, children=None, label=f'finish_second')
    durationData['finish_second'] = 0
    tree.children.append(secondFinish)


def grossing(grossSeq, index = 0, cassetteCount=0, cassetteCountIndex= 0, slideEachCassette=[],labDataObj: LabData = None, sectionIds = {}, durationData = None, grossDuration = 0):
    gross = obj.ProcessTree(operator=None, parent=grossSeq, children=None, label=f'grossing_{index}')
    grossSeq.children.append(gross)

    multiProcessing = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=grossSeq, children=None, label=None)
    grossSeq.children.append(multiProcessing)
    #nbCassette = labDataObj.nbCassetPerSpecimen()
    nbCassette =cassetteCount
    # now this should dominate the grossing time for this specimen gross
    #grossDuration = labDataObj.getGrossDuration(nbCassette=nbCassette)
    durationData[f'grossing_{index}'] = grossDuration

    for c in range(nbCassette):
        cassetSeq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multiProcessing, children=None, label=None)
        multiProcessing.children.append(cassetSeq)
        process = obj.ProcessTree(operator=None, parent=cassetSeq, children=None, label=f'processing_{index}_{c}')
        cassetSeq.children.append(process)
        durationData[f'processing_{index}_{c}'] = 2 * 60 * 60
        embedding = obj.ProcessTree(operator=None, parent=cassetSeq, children=None, label=f'embedding_{index}_{c}')
        durationData[f'embedding_{index}_{c}'] = labDataObj.getEmbeddingDuration()

        cassetSeq.children.append(embedding)
        section = obj.ProcessTree(operator=None, parent=cassetSeq, children=None, label=f'section_{index}_{c}')
        cassetSeq.children.append(section)
        sectionIds[f'section_{index}_{c}'] = []
        slidesParalell = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=cassetSeq, children=None, label=None)
        cassetSeq.children.append(slidesParalell)
        #nbSlides = labDataObj.nbSlidesPerCassette()
        nbSlides = slideEachCassette[cassetteCountIndex + c]
        # this dominate time for section
        sectoinDuration = labDataObj.getSectionDuration(nbSlides)
        durationData[f'section_{index}_{c}'] = sectoinDuration

        for s in range(nbSlides):
            signout = obj.ProcessTree(operator=None, parent=slidesParalell, children=None, label=f'signout_{index}_{c}_{s}')
            duration = labDataObj.getSignoutDuration()
            durationData[f'signout_{index}_{c}_{s}'] = duration
            slidesParalell.children.append(signout)
            sectionIds[f'section_{index}_{c}'].append(f'signout_{index}_{c}_{s}')




def skillSet3():
    metaData = {}
    reworkTransisions = []
    resources = {}
    orTransisions = []  # lit of tuples
    eventResourceSet = {}

    net = PetriNet("new_petri_net")
    p_1 = PetriNet.Place("p_1")
    net.places.add(p_1)

    t_1 = PetriNet.Transition("tran1", "tran1")

    net.transitions.add(t_1)

    newPlaces = {}
    newTrans = {}
    startIndex = 2
    nbStages = 2
    nbEventsPerStage = 2
    for s in range(nbStages):
        newPlaces[s] = []
        newTrans[s] = []
        for e in range(nbEventsPerStage):
            name = 'p_' + str(startIndex)
            p = PetriNet.Place(name)
            net.places.add(p)
            tName = 'trans_' + str(startIndex)
            t = PetriNet.Transition(tName, tName)
            net.transitions.add(t)
            startIndex += 1
            newPlaces[s].append(p)
            newTrans[s].append(t)

    tName = 'trans_' + str(len(net.transitions) + 1)
    finalT = PetriNet.Transition(tName, tName)
    net.transitions.add(finalT)

    newPlaces[nbStages] = []
    for e in range(nbEventsPerStage):
        name = 'p_' + str(startIndex)
        startIndex += 1
        p = PetriNet.Place(name)
        net.places.add(p)
        newPlaces[nbStages].append(p)
        utils.add_arc_from_to(p, finalT, net)

    name = 'p_' + str(startIndex)
    finalP = PetriNet.Place(name)
    net.places.add(finalP)

    utils.add_arc_from_to(finalT, finalP, net)

    # now add links between trans and places
    for s in range(nbStages):
        for e in range(nbEventsPerStage):
            utils.add_arc_from_to(newPlaces[s][e], newTrans[s][e], net)
    for s in range(nbStages - 1):
        for e in range(nbEventsPerStage):
            utils.add_arc_from_to(newTrans[s][e], newPlaces[s + 1][e], net)

    for e in range(nbEventsPerStage):
        utils.add_arc_from_to(newTrans[nbStages - 1][e], newPlaces[nbStages][e], net)
    utils.add_arc_from_to(p_1, t_1, net)
    for e in range(nbEventsPerStage):
        utils.add_arc_from_to(t_1, newPlaces[0][e], net)

    capacity = 8
    for t in net.transitions:
        trans: PetriNet.Transition = t
        resources[trans.label] = (trans.label, capacity)
    create_resource_file(resources)

    staffArrays = []
    counter = 0
    for i in range(len(net.transitions)):
        staffList = []
        for j in range(capacity):
            name = 'Oddo' + str(counter)
            counter += 1
            staffList.append(name)
        #counter = counter - 2
        staffArrays.append(staffList)

    counter = 0
    for t in net.transitions:
        # printing n elements from list
        eventResourceSet[t.name] = resource_data(resource_names=staffArrays[counter])
        counter += 1

    metaData[resourceTag] = eventResourceSet
    metaData[startEventTag] = [t_1]

    initial_marking = Marking()
    final_marking = Marking()
    tokenMarking = MyMarking()
    # create a token at a place
    token1 = MyToken(place=p_1, name='p_1')
    # token2 = MyToken(place=p_2, name='p_2')
    tokenMarking[token1] = token1
    # tokenMarking[token2] = token2
    initial_marking[p_1] = 1

    metaData[multiChoiceTransTag] = orTransisions
    metaData[reworkTransTag] = []
    metaData[reworkUppboundTag] = {}
    for trans in reworkTransisions:
        metaData[reworkUppboundTag][trans] = 4
    return net, initial_marking, tokenMarking, metaData

def load_petri():
    from pm4py.objects.petri_net.importer import importer as pnml_importer
    from pm4py.visualization.petri_net import visualizer as pn_visualizer
    # pnml_exporter.apply(net, initial_marking, "petri.pnml", final_marking)
    net, initial_marking, final_marking = pnml_importer.apply('petri.pnml')
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)


    metaData = {}
    reworkTransisions = []
    resources = {}
    orTransisions = [] # lit of tuples
    for t in net.transitions:
        if t.label == 'tran1':

            metaData[startEventTag] = [t]
            break
    initial_marking = Marking()
    final_marking = Marking()
    tokenMarking = MyMarking()
    # create a token at a place
    p_source = None
    for p in net.places:
        if p.name == 'source':

            token1 = MyToken(place=p, name='source')
    #token2 = MyToken(place=p_2, name='p_2')
            tokenMarking[token1] = token1
            p_source = p
            break
    #tokenMarking[token2] = token2
    initial_marking[p_source] = 1

    metaData[multiChoiceTransTag] = orTransisions
    metaData[reworkTransTag] = reworkTransisions
    metaData[reworkUppboundTag] = {}
    for trans in reworkTransisions:
        metaData[reworkUppboundTag][trans] = 4

    for t in net.transitions:
        trans: PetriNet.Transition = t
        resources[trans.label] = (trans.label,18)
    create_resource_file(resources)
    return net, initial_marking, tokenMarking, metaData


def skillSet5():
    metaData = {}
    reworkTransisions = []
    resources = {}
    orTransisions = []  # lit of tuples
    eventResourceSet = {}

    seq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=None, children=None, label=None)
    trans1 = obj.ProcessTree(operator=None, parent=seq, children=None, label='Arrive')
    seq.children.append(trans1)
    xand = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=seq, children=None, label=None)
    seq.children.append(xand)
    trans2 = obj.ProcessTree(operator=None, parent=xand, children=None, label='Get Food')
    xand.children.append(trans2)
    trans3 = obj.ProcessTree(operator=None, parent=xand, children=None, label='Get Drink')
    xand.children.append(trans3)

    finish = obj.ProcessTree(operator=None, parent=seq, children=None, label='Finish')
    seq.children.append(finish)

    tree = seq
    # tree_consistency.fix_parent_pointers(tree)
    # tree = generic.fold(tree)
    # generic.tree_sort(tree)
    gviz = pt_visualizer.apply(tree)
    #matplotlib_view(gviz)

    net, initial_marking, final_marking = pt_converter.apply(tree)
    parameters = {pn_visualizer.Variants.FREQUENCY.value.Parameters.FORMAT: "png"}
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    # matplotlib_view(gviz)
    #pt_visualizer.save(gviz, "converted-net.png")
    #matplotlib_view(gviz)

    capacity = 5
    t_1 = None
    for t in net.transitions:
        trans: PetriNet.Transition = t
        if t.label is None:
            t.label = 'Finish'

        if trans.label == 'Arrive':
            t_1 = trans
        resources[trans.label] = (trans.label, capacity)
    # create_resource_file(resources)
    p_1 = None
    for p in net.places:
        place: PetriNet.Place = p
        if place.name == 'source':
            p_1 = place
            break

    staffArrays = []
    counter = 0
    for i in range(len(net.transitions)):
        staffList = []
        for j in range(capacity):
            name = 'Oddo' + str(counter)
            counter += 1
            staffList.append(name)
        #counter = counter - 2
        staffArrays.append(staffList)

    counter = 0
    for t in net.transitions:
        # printing n elements from list
        label = t.label
        if t.label is None:
            label = 'hidder' + str(counter)
        eventResourceSet[label] = resource_data(resource_names=staffArrays[counter])
        counter += 1
    if True:
        eventResourceSet['Arrive'] = resource_data(resource_names=['John', 'Mike'])
        eventResourceSet['Get Food'] = resource_data(resource_names=['Allen', 'Jason'])
        eventResourceSet['Get Drink'] = resource_data(resource_names=['AJ', 'Dub'])
        eventResourceSet['finish'] = resource_data(resource_names=['Alice'])
    metaData[resourceTag] = eventResourceSet
    metaData[startEventTag] = [t_1]
    metaData[durationTag] = {'Arrive': 10 * 60, 'Get Food': 15 * 60, 'Get Drink': 10 * 60, 'finish': 5 * 60}

    initial_marking = Marking()
    final_marking = Marking()
    tokenMarking = MyMarking()
    # create a token at a place
    token1 = MyToken(place=p_1, name='p_1')
    # token2 = MyToken(place=p_2, name='p_2')
    tokenMarking[token1] = token1
    # tokenMarking[token2] = token2
    initial_marking[p_1] = 1

    metaData[multiChoiceTransTag] = orTransisions
    metaData[reworkTransTag] = []
    metaData[reworkUppboundTag] = {}
    for trans in reworkTransisions:
        metaData[reworkUppboundTag][trans] = 4
    return net, initial_marking, tokenMarking, metaData

import threading

_uid = threading.local()
def genuid():
    if getattr(_uid, "uid", None) is None:
        _uid.tid = threading.current_thread().ident
        _uid.uid = 0
    _uid.uid += 1
    return _uid.uid

def getEventId(start = 1):
    id = start
    while True:
        yield int(id)
        id += 1







def try_execute(t, pn, m: Marking, tokenMarking:MyMarking):
    if not semantics.is_enabled(t, pn, m):
        return None
    #debug_output('token marking before trnaision')
    #debug_output(f'tokenMarking')
    m_out = copy(m)
    tokenMarkingOut = copy_token_marking(tokenMarking)
    #print(tokenMarkingOut)

    tokenToDelete = []
    for a in t.in_arcs:
        # remove a token from the place in the in arc
        # a is an arc, arc source must be a place
        # can I find A TOKEN in that place?

        for token in tokenMarkingOut.keys():
            if token.place == a.source:
                #debug_output(f'find a token {token} for place {a.source}')
                # remove this token
                tokenToDelete.append(token)

        m_out[a.source] -= a.weight
        if m_out[a.source] == 0:
            # delete this key/value from dict
            del m_out[a.source]

    for x in tokenToDelete:
        del tokenMarkingOut[x]
    #debug_output('token marking after trnaision')

    for a in t.out_arcs:
        m_out[a.target] += a.weight
        # now generate a new token
        newToken = MyToken(place = a.target, name = a.target.name, parent= tokenToDelete)
        tokenMarkingOut[newToken] = newToken
    debug_output(f'tokenMarkingOut')
    return m_out, tokenMarkingOut


def execute(t, pn, m: Marking, tokenMarking:MyMarking, eventIdGenerator = None):
    if not semantics.is_enabled(t, pn, m):
        return None
    debug_output('token marking before trnaision')
    debug_output(f'tokenMarking')
    m_out = copy(m)
    tokenMarkingOut = copy_token_marking(tokenMarking)
    debug_output(f'tokenMarkingOut')
    myTrans = MyTransition(trans=t, eventIdGenerator = eventIdGenerator)

    tokenToDelete = []
    parentTrans = []
    for a in t.in_arcs:
        # remove a token from the place in the in arc
        # a is an arc, arc source must be a place
        # can I find A TOKEN in that place?

        for token in tokenMarkingOut.keys():
            if token.place == a.source:
                debug_output(f'find a token {token} for place {a.source}')
                parentTrans.append(token.parentTrans)

                # this token knows which transition genrated this token
                # remove this token
                tokenToDelete.append(token)

        m_out[a.source] -= a.weight
        if m_out[a.source] == 0:
            # delete this key/value from dict
            del m_out[a.source]
    myTrans.set_parent(parentTrans)
    for x in tokenToDelete:
        del tokenMarkingOut[x]
    debug_output('token marking after trnaision')

    for a in t.out_arcs:
        m_out[a.target] += a.weight
        # now generate a new token
        newToken = MyToken(place = a.target, name = a.target.name, parent= tokenToDelete)
        # know which tranision genrate this token
        # also know which tokens generate this token
        newToken.parent_trans(myTrans)
        tokenMarkingOut[newToken] = newToken
    debug_output(f'{tokenMarkingOut}')
    return m_out, tokenMarkingOut, myTrans

def duration_function(counter):
    duration = 10 * 60
    if counter % 2 == 1:
        duration = 15 * 60
    return duration


def copy_token_marking(original):
    tokenMarkingOut = MyMarking()
    for key, value in original.items():

        tokenMarkingOut[key] = value
    return tokenMarkingOut

