from loadBinData import *
import networkx as nx

def set_backlog(mapLog, backLogNodeStr, df, day):
    graph = nx.DiGraph()
    df['done'] = False
    if mapLog.loc[mapLog.date == day, 'id'].values[0] < 5:
        currentNodeIds = df.event_id.values.tolist()
        print(df.info())
        # print(currentNodeIds)
        parentIds = df.parents.values.tolist()
        for idx, parents in enumerate(parentIds):
            if type(parents) != list:
                parents = eval(parents)
            # print(type(parents))

            # print(parents, currentNodeIds[idx])
            # add an directed arc from child to parent
            for parent in parents:
                graph.add_edge(currentNodeIds[idx], parent)

        # nx.draw_planar(graph, with_labels=True)

        backLogNodeDf = df.loc[df['activity'].str.contains(backLogNodeStr, case=False)]
        backLogNodeList = backLogNodeDf.event_id.values.tolist()
        print(backLogNodeList)
        for currentBackLogId in backLogNodeList:
            descendants = nx.descendants(graph, currentBackLogId)

            for d in descendants:
                df.loc[df.event_id == d, 'done'] = True


def modifyForBackLog():

    job_bin = loadData()
    mapping, mapLog = load_mapping()


    # graph.add_edge(parentNode, newNode)
    backLogNodeStr = 'grossing'
    for day, df in job_bin.items():
        set_backlog(mapLog, backLogNodeStr, df, day)

        # plt.show()
        parentDirectory = os.getcwd()

        filePath = os.path.join(parentDirectory, root, str(mapLog.loc[mapLog.date == day, 'id'].values[0] ) + '.csv')
        print(filePath)
        df.to_csv(filePath, index=False)

