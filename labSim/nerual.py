import numpy as np
from simPyExtentionSkillSet import *
def import_csv(file_path):
    print(f'start loading {file_path}')

    eventlog = pd.read_csv(file_path, sep=',', skipinitialspace=True)
    return eventlog
# p = 0, only night batch, p =1, mid and night both
df = import_csv('test.csv')
print(df.info())

x = df.copy()

one_hot = pd.get_dummies(x['dayofweek'], prefix='dayofweek')

x = x.join(one_hot)

one_hot = pd.get_dummies(x['hour'], prefix='hour')

x = x.join(one_hot)

one_hot = pd.get_dummies(x['month'], prefix='month')

x = x.join(one_hot)



print(x.columns)

x.to_csv('t.csv')

FEATURES = ['hour', 'minute', 'month', 'dayofweek', 'p', 'day']
TARGET = 'days'

def get_feature(df):


    x = df.copy()

    x = x.drop([TARGET], axis=1)
    print(df.info())
    Y = df[TARGET]
    return x, Y

x, Y = get_feature(x)

from sklearn.model_selection import train_test_split


X_train, X_test, y_train, y_test = train_test_split(
   x, Y, test_size=0.33, random_state=42)
print(x.info())
print('sample', len(X_train), len(X_test))

# remove month and day
X_test_copy = X_test[['day','month','hour']]
X_train = X_train.drop(['duration','case_id','start','finish','day','hour', 'month', 'dayofweek', 'minute'], axis=1)
X_test = X_test.drop(['duration','case_id','start','finish','day','hour', 'month', 'dayofweek', 'minute'], axis=1)
print(X_train.info())
from sklearn.metrics import mean_squared_error
from sklearn import linear_model
import matplotlib.pyplot as plt
clf = linear_model.Lasso(alpha=0.06)
clf.fit(X_train, y_train)
loss = mean_squared_error(clf.predict(X_train), y_train)
print('training loss', loss)
y = clf.predict(X_test)
loss = mean_squared_error(y, y_test)
print('predict loss', loss)
X_test = X_test.merge(X_test_copy, left_index=True, right_index=True)
X_test['predict'] = y
X_test['actual'] = y_test
X_test['year'] = 2021
X_test['date'] = pd.to_datetime(X_test[['year', 'month', 'day','hour']])
X_test = X_test[['date','actual', 'predict']]
X_test = X_test.set_index('date')
X_test.plot(kind='line')
plt.show()

