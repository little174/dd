from labplurealroster import *

if True:
    staffRosterDict, _, _  = LabData.staffRosterTest()
    _, recorder = test(staffRosterDict=staffRosterDict)
    print(f''''overall mean  = {recorder['overall-mean']}''')
if False:
    initialSolution, _, _ = LabData.staffRosterTest()
    stages = {access: [2, [6, 8, 9], 12], grossing: [10, [6, 10, 12], 8.5],
              embedding: [5, [6, 8, 12], 12], section: [5, [6, 8, 10], 12], signout: [2, [6, 8, 10], 8]}
    valueHistory = []
    for stage, shiftPattern in stages.items():
        print('local search for ', stage)
        capacity = shiftPattern[0]
        startTimeArray = shiftPattern[1]
        duration = shiftPattern[2]
        initialPool = LabData.geneticInitialPool(initialSolution=initialSolution, stage=stage,
                                                 capacity=capacity, startTimeArray=startTimeArray, duration=duration)
        fitness = {}
        for key, roster in initialPool.items():
            print(key)

            _, recorder = test(staffRosterDict=roster)
            fitness[key] = recorder['overall-mean']
            print(f''''overall mean for {key} = {recorder['overall-mean']}''')
        list_of_tuples = list(fitness.items())

        list_of_tuples.sort(key=lambda a: a[1])
        print(list_of_tuples)
        best_roster = initialPool[list_of_tuples[0][0]]
        valueHistory.append(list_of_tuples[0][1])
        initialSolution = dict(best_roster)

    df = rosterDF(best_roster)
    print('value history ', valueHistory)
    LabData.show_roster(df)

# choose the top two, swap
