
from modelDef import *

from edParams import *
from pm4py.visualization.petri_net.variants.token_decoration_frequency import *

from pm4py.visualization.petri_net import visualizer as pn_visualizer


startDayOffset = 0



seconds_per_hour = 60 * 60

shop_start_time = 7 * seconds_per_hour
shop_end_time = 18 * seconds_per_hour
day_in_seconds = seconds_per_hour * 24
useBusinessHours = False
onLocal = True
useFakeDuration = True
time_bin_type = collections.namedtuple('time_bin', 'start finish')
root = 'edData'
allResults = {}




fig_counter = 0
frames = []


def save_marking(net, initial_marking, final_marking):
    global fig_counter
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    file = 'marking_' + str(fig_counter) + '.png'
    fig_counter = fig_counter + 1
    # pn_visualizer.save(gviz, file)
    frames.append(gviz)


from animate import *


def show_frames(nbFrames = 100):
    for idx, frame in enumerate(frames):
        matplotlib_view(frame)
        if idx >= nbFrames:
            return


def generate_ed_event(index, start_time, netToUse, results):
    case_event_id_generator = getEventId(int(index * 1e3))
    net, initial_marking, tokenMarking, metaData = netToUse()
    caseDurationInfo = metaData[durationInfo]
    final_marking = Marking()
    marking = copy(initial_marking)
    case_num = 'Case %d' % index
    counter = 0
    save_marking(net, marking, final_marking)
    isComplete = True
    subtrails = []
    while True:
        if not semantics.enabled_transitions(net, marking):
            break
        all_enabled_trans = semantics.enabled_transitions(net, marking)
        debug_output(f'{all_enabled_trans}')
        all_enabled_trans = list(all_enabled_trans)
        random.shuffle(all_enabled_trans)
        # check if we hit a rework upper bound

        # now find paralell enabled transisiont
        # if t is in all endabled before an executation and also in after an enabled transsion, this can happen in paraleel
        # check if an OR transision tuple is here
        # if loop meets constraints, we may reove all transisions with incomplete trail
        if len(all_enabled_trans) == 0:
            isComplete = False
            break
        t = all_enabled_trans[0]

        # t= all_enabled_trans[0]
        try_marking = copy(marking)
        debug_output('try execute')
        debug_output(f'{t}')
        paralellevents = []
        paralellevents.append(t)

        for t in paralellevents:
            if t.label in caseDurationInfo:
                duration = caseDurationInfo[t.label]
            else:
                assert False, f'label not exsit {t.label}'
                duration = duration_function(counter)

            counter = counter + 1

            marking, tokenMarking, newEvent = execute(t, net, marking, tokenMarking, case_event_id_generator)


            newEvent.set_duration(duration)
            birthTime = start_time
            for x in newEvent.get_parent():

                if x is None:
                    continue
                birthTime = max(birthTime, x.finish_time())

            newEvent.set_birth(birthTime)

            is_start = t in metaData[startEventTag]
            if (index == 3):
                priority = -1
            else:
                priority = 0
            data = [case_num, t.label, newEvent.birth, newEvent.finish_time(),
                    format_time(newEvent.birth), format_time(newEvent.finish_time()), newEvent.id, is_start, priority]
            debug_output('new event paretns are')
            parentStr = []
            for x in newEvent.get_parent():

                if x is None:
                    continue
                parentStr.append(x.id)

            data.append(parentStr)

            # lets add batch

            subtrails.append(data)

            # trace.append(event)
            # all_traces[newEvent] = event
        # r = yield env.all_of(simEvents)
        save_marking(net, marking, final_marking)
    assert (isComplete)
    if isComplete:
        # print(subtrails[-1][1] )

        for data in subtrails:
            results.append(data)


def createByDensity( netToUse, nbDays = 2):
    job_arrival_dict = EmergencyDataDistribution.create_jobs(EmergencyDataDistribution.jobDistribution)
    caseGettor = EmergencyDataDistribution.create_fake_data_by_density(job_arrival_dict)
    allCaseData = {}
    c = 0
    for i in caseGettor:
        data: history_data = i
        #print(data)
        index = data.index
        arrivalTime = data.arrivalTime
        #print(f'next call arrival at {format_time(arrivalTime)}')
        currentDay = arrivalTime // day_in_seconds
        #print('current day', currentDay)
        if currentDay >= nbDays :
            break
        if currentDay not in allCaseData:
            allCaseData[currentDay] = []
            daystr = getDateTime(arrivalTime, '%Y-%m-%d')
            print(f'start generating at {daystr}')
        #if len(allCaseData[currentDay]) > 2:
        #    continue
        generate_ed_event(index + 1, start_time=arrivalTime, netToUse=netToUse, results=allCaseData[currentDay])

        c += 1





    if onLocal:
        print('curernt', os.getcwd())
        parentDirectory = os.getcwd()

        filePath = os.path.join(parentDirectory, root, '*.csv')

        # Get a list of all the file paths that ends with .txt from in specified directory
        fileList = glob.glob(filePath)
        # Iterate over the list of filepaths & remove each file.
        for filePath in fileList:
            try:
                os.remove(filePath)
            except:
                print("Error while deleting file : ", filePath)
    for day, results in allCaseData.items():
        if onLocal:
            write_log(day, results)
        allResults[day] = copy(results)
    return sorted(list(allCaseData.keys()))


def createByHistory( netToUse, nbDays = 100):
    currentDay = 0
    allCaseData = {}
    arrivalTimeGen = get_next_arrive_time()
    startTime = 6 * seconds_per_hour+next(arrivalTimeGen)
    for index in range(1):
        if currentDay not in allCaseData:
            allCaseData[currentDay] = []
        generate_ed_event(index + 1, start_time=startTime, netToUse=netToUse, results=allCaseData[currentDay])
        startTime +=next(arrivalTimeGen)


    for day, results in allCaseData.items():
        if onLocal:
            write_log(day, results)
        allResults[day] = copy(results)
    return sorted(list(allCaseData.keys()))



def write_log(day, results):
    import  csv
    filePath = os.path.join(root, str(day) + '-ed.csv')
    f = open(filePath, 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        ['case_id','activity', 'start', 'finish', 'formated_start_time', 'formated_finish_time', 'event_id',
         'is_start', 'priority', 'parents'])
    for row in results:
        thewriter.writerow(row)
    f.close()



def combine_ouput(allResults):
    if onLocal == False: return
    data = []
    for day, results in allResults.items():
        for r in results:
            data.append(r)

    f = open('simulated-logs.csv', 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        ['case_id', 'activity', 'start', 'finish', 'formated_start_time', 'formated_finish_time', 'event_id',
         'is_start', 'parents'])
    for row in data:
        thewriter.writerow(row)
    f.close()

def check(netToUse):
    net, init, final, meta = netToUse()
    gviz = pn_visualizer.apply(net, init, Marking())
    pn_visualizer.view(gviz)






#viewLabModel()

#show_frames()
def CreateData(isLocal = True):
    global onLocal
    onLocal = isLocal
    days = createByDensity(netToUse=patientFlow)
    print(days)
    dateMapping = []
    for day in days:
        dateStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        dateMapping.append([day, dateStr])

    dateMapping = pandas.DataFrame(dateMapping, columns=['id', 'date'])
    filePath = os.path.join(root, 'ed-mapping.csv')
    if onLocal:
        dateMapping.to_csv(filePath, index=False)
    return dateMapping

