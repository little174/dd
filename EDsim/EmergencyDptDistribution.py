from commonParams import *
from edParams import *
import random

import pandas
showRoster = False
labRadio = 'labRadio'
walkin = 'Walk In'
register = 'Register'
resuscitation = 'Resuscitation Room'

ambPickup = 'Ambulance Pickup'
ambArrive = 'Ambulance Arrive At ED'
handOver = 'Hand Over'
onHold = 'On Hold For ED Bed'
edTreat = 'Assign ED Treatment Bed'
nurseAccess = 'Nurse Assessment'
physicanAsscess = 'Physician Assessment'
nurseProcess = 'Nurse Process'
getBloodForLab = 'Get Blood For Lab'
sentForLab = 'Blood Sent to Lab'
getLabResult = 'Get Lab Result'
radio = 'Radiology'
getRadio = 'Get Radiology Result'
physicalEval = 'Physician Evaluation Discharge'
transferOrthopedics = 'Transfer To Orthopedics'
transferCardiology = 'Transfer To Cardiology'
bedAtOrthopedics = 'Assign Bed at Orthopedics'
bedAtCardiology  = 'Assign Bed at Cardiology'
discharge = 'Discharge'
finishTag = 'Finish'

nurseRes = 'nurse'
physicanRes ='physician'
registerRes = 'receptionist'

edBedRes = 'ED beds'
labRes = 'LabStaff'
radioRes = 'RadioStaff'
resuscitationRes = 'ResuscitationStaff'
orthopedicsBedRes = 'Orthopedics beds'
cardiologyBedRes = 'Cardiology beds'
# define capacity for beds, these resources dont depend on time
#nonRosterEventResource= {edBedRes: 4, orthopedicsBedRes:100, cardiologyBedRes:100}
#resoures_count = {nurseRes: 2, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
stages = [nurseRes, physicanRes, registerRes, labRes, radioRes]
# for every event that requires a resoruce, create the map
event_to_resrouce = {handOver: nurseRes, nurseAccess: nurseRes, physicanAsscess: physicanRes,
                     physicalEval: physicanRes, nurseProcess:nurseRes, transferOrthopedics: nurseRes,
                     transferCardiology: nurseRes, discharge: nurseRes, edTreat:edBedRes,
                     bedAtOrthopedics:orthopedicsBedRes, bedAtCardiology: cardiologyBedRes,
                     register: registerRes, getBloodForLab: nurseRes, getLabResult: labRes, radio: radioRes}

masterEvents = [labRadio]
edBedReleaseEvent = [discharge, bedAtCardiology, bedAtOrthopedics]
wardBedEvents = [bedAtCardiology, bedAtOrthopedics]
#revert the map
resource_to_event = {}

priorityMap = {0: 'Normal', -1: 'High'}

for key, value in event_to_resrouce.items():
    if value not in resource_to_event:
        resource_to_event[value] = []
    resource_to_event[value].append(key)
print(resource_to_event)


SimSetting.setup(backlogWeek=0, rosterWeekBlockSpan=2)
class EmergencyDataDistribution:
    profile = { 10: 800, 11: 800, 12: 4000, 13:500, 14:600, 15: 300, 16: 90}
    jobDistribution = JobPattern()
    @classmethod
    def event_duration(cls, event):
        if event == handOver:
            return 10 * 60
        if event == edTreat:
            return 5 * 60
        if event == nurseAccess:
            return 10 * 60
        if event == physicanAsscess:
            return 8 * 60
        if event == nurseProcess:
            return 10 * 60
        if event == getLabResult:
            return 15 * 60
        if event ==  physicalEval:
            return 7 * 60
        if event == sentForLab:
            return  5 * 60
        if event == discharge:
            return 5 * 60
        if event == radio:
            return 30 * 60
        if event == getRadio:
            return 30 * 60
        if event == bedAtCardiology:
            return 60 * 60
        if event == bedAtOrthopedics:
            return 60 * 60
        if event == transferCardiology:
            return 10 * 60
        if event == transferOrthopedics:
            return 10 * 60
        if event == getBloodForLab:
            return 10 * 60

        return 0

    @classmethod
    def durationInHourToGetWardBed(cls, hour):
        #hack
        return 1
        keyToDraw = []
        weights = []
        for key, value in cls.profile.items():
            if hour < key:
                keyToDraw.append(key)
                weights.append(value)
        if len(keyToDraw) == 0:
             # no bed to release today, release a bad tmr at 10
            return 24 + 10 - hour
        hourGetWard = random.choices(keyToDraw, weights=weights, k=1)[0]
        return hourGetWard - hour

    @classmethod
    def create_job_pattern(cls):
        cls.jobDistribution.job_data.clear()
        rate_dict = {}
        for c in range(6, 12):
            rate_dict[c] = 1
        rate_dict[12] = 1
        rate_dict[13] = 1
        rate_dict[14] = 1
        rate_dict[15] = 1
        rate_dict[16] = 1
        rate_dict[17] = 1
        for i in range(SimSetting.jobTotalDays):
            for j in range(6, 18):
                newDate = SimSetting.start_time_sim + timedelta(days=i)
                rate = rate_dict[j]
                if newDate < (SimSetting.start_time_sim + timedelta(days=7)):
                    rate = 1
                cls.jobDistribution.upsert_job_distribution(newDate.year, newDate.month, newDate.day, j, rate)
        df = []
        for key, value in cls.jobDistribution.job_data.items():
            df.append([datetime(year=key[0], month=key[1], day=key[2], hour=key[3]), value])

        return cls.jobDistribution, df

    @classmethod
    def create_jobs(cls, jobHourRate):
        dailyRate = 200
        job_arrival_dict = {}
        hourList = [i for i in range(6,18)]
        for i in range(SimSetting.jobTotalDays):
            job_arrival_dict[i] = []
            #normalise weight at each hour
            newDate = SimSetting.start_time_sim + timedelta(days=i)
            hourRates = [jobHourRate.job_data[newDate.year, newDate.month, newDate.day, h] for h in range(6, 18)]

            for idx, rate in enumerate(hourRates):
                # even distribute jobs in this hour

                job_arrival_interval = int(3600 / rate)
                for job in range(rate):
                    jobTime = SimSetting.start_time_sim + timedelta(days=i, hours=hourList[idx],seconds=job_arrival_interval*job)
                    job_arrival_dict[i].append(jobTime)
        return job_arrival_dict

    @classmethod
    def staffRosterTest(cls):
        # add 2 weeks of roster

        print(SimSetting.rosterWeekDayList)

        print(SimSetting.weekBlockStart)

        staffRosterDict = {}

        offDays = [5, 6]



        for j in range(2):
            name = f'{nurseRes}_{j}'
            staff = StaffShift(name=name, stage=nurseRes)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 12)
            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff


        for j in range(10):
            name = f'{physicanRes}_{j}'
            staff = StaffShift(name=name, stage=physicanRes)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=10), 8.5)

            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff


        for j in range(2):
            name = f'{registerRes}_{j}'
            staff = StaffShift(name=name, stage=registerRes)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 8)
            staffRosterDict[name] = staff


        for j in range(5):
            name = f'{labRes}_{j}'
            staff = StaffShift(name=name, stage=labRes)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 12)
            staffRosterDict[name] = staff


        for j in range(5):
            name = f'{radioRes}_{j}'
            staff = StaffShift(name=name, stage=radioRes)
            for i in range(SimSetting.rosterTotalDays):
                if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=6), 12)
            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff

        df = rosterDF(staffRosterDict)
        if showRoster:
            fig = px.timeline(df, x_start="Start", x_end="Finish", y="Task", color="Resource")
            fig.update_yaxes(autorange="reversed")
            fig.show()
        return staffRosterDict, df, SimSetting.rosterWeekDayList

    @classmethod
    def event_to_resource_group(cls,resoures_count):
        results = []
        nbWeeks = 1
        nbDays = 7

        for  key, value in resoures_count.items():
            dayIndex = 0
            for week in range(nbWeeks):
                for day in range(nbDays):

                    for timeIndex in range(5,24):

                        staff_list = [f'{key}_{i}' for  i in range(value) ]
                        data = [ dayIndex, timeIndex, key]
                        data.append(staff_list)
                        results.append(data)
                    dayIndex += 1
        resultsLog = pandas.DataFrame(results, columns=['day', 'timeIndex', 'group', 'resource_set'])
        resultsLog = resultsLog.sort_values([ 'day', 'timeIndex'], ascending=(True, True, ))
        resultsLog.to_csv('edRoster.csv', index=False)

        results.clear()
        dayList = [i for i in range(7 * nbWeeks)]
        timeList = [i for i in range(5,24)]
        for day in dayList:
            for t in timeList:
                results.append([day, t, t, t + 1])
        dayTimeLog = pandas.DataFrame(results, columns=['day', 'timeIndex', 'start', 'end'])
        dayTimeLog = dayTimeLog.sort_values(["day", "timeIndex"], ascending=(True, True))
        dayTimeLog.to_csv('dayTimeMap.csv', index=False)
        return resultsLog, dayTimeLog


    @classmethod
    def create_fake_data_by_density(cls, job_arrival_dict):
        index = 0
        for d in range(SimSetting.jobTotalDays):

            for t in cls.get_next_job_rate(job_arrival_array=job_arrival_dict[d]):

                isTwoHour = random.choices([True, False], weights=[0.2, 0.9], k=1)[0]
                if (t.weekday() in [5,6]): continue
                arrivalTimeInSeconds = int((t - SimSetting.start_time_sim).total_seconds())
                data = history_data(index=index, arrivalTime=int(arrivalTimeInSeconds))

                index += 1
                yield data

    @classmethod
    def get_next_job_rate(cls, job_arrival_array):
        for t in job_arrival_array:
            yield t



jobDistribution, df = EmergencyDataDistribution.create_job_pattern()
