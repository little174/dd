

from EmergencyDptDistribution import *
from SimulationObject import *
from edParams import *
import math





class EdEvent(Event):
    def __init__(self, id=0, start=0, finish=0, parents=[], name='', case_id=0, is_start=False, priority = 0):
        # actual is the event with id
        # name is the event name without id, representing the group, for exmaple, grossing_0 -> grossing (group)
        self.id = id
        self.start = start
        self.finish = finish
        self.parents = parents
        self.duration = finish - start
        # self.duration = 35
        self.actualName = name
        if name in event_to_resrouce:
            self.group = event_to_resrouce[name]
        # default no resource is needed
        else: self.group= noResNeeded
        self.case_id = case_id
        self.is_start = is_start
        self.simulated_start = 0
        self.simulated_end = 0
        self.useResource = None
        self.resourceTimeIndex = None

        self.ready_time = None
        self.priority = priority




class EdRosterSimulation(Simulation):

    policyResults = []

    def __init__(self, isLocalSim=True, nonRosterEventResource=None):
        super().__init__(isLocalSim)
        # for any bed resource that needs to be released later by some other event, put its tag in
        self.ed_bed_release_later = {edBedRes: {}}
        self.nonRosterEventResource =nonRosterEventResource


    @classmethod
    def clear_resutls(cls):
        cls.policyResults = []

    def event_row_process_new_cases(self, row, new_cases):
        case_id = row.case_id
        activity = row.activity
        start = row.start
        priority = row.priority
        finish = row.finish
        event_id = row.event_id

        event_parents = list()



        if not self.isLocalSim:
            res = row.parents

        else:
            if fromdBRegression:
                a = row.parents.find('[')
                b = row.parents.find(']')
                x = row.parents[a + 1:b]
                l = x.split(' ')
                print(row.event_id, l)
                assert(False)
                if l == ['']:
                    res = []
                else:
                    res = [int(s) for s in l]

                a = row.batch_filter.find('[')
                b = row.batch_filter.find(']')
                x = row.batch_filter[a + 1:b]
                l = x.split(' ')
                batchFilter = []
                for v in l:
                    e = v.find("'")
                    k = v[e+1:]
                    f = k.find("'")
                    m = k[:f]
                    batchFilter.append(m)

                print(res)
                print(batchFilter)
            else:
                res = ast.literal_eval(row.parents)


        res = list(filter(None, res))



        for value in res:
            event_parents.append(int(value))

        newEvent = EdEvent(event_id, start, finish, event_parents, activity, case_id, row.is_start, priority)
        assert (event_id not in self.allEvents)
        self.allEvents[event_id] = newEvent

        if row.case_id not in self.allCases:
            self.allCases[case_id] = []
            new_cases.append(case_id)
        if row.is_start:
            self.caseArrivals[case_id] = start
        self.allCases[case_id].append(newEvent)





    def GetFreeResource(self, event, priority, success, transitionName):
        while not success:
            resourceDict = {}
            requestOrEvents = []

            # now get the correct skillset window
            foundResourceSet = False
            quitWait = 0
            while not foundResourceSet:
                dayIndex = int(self.env.now % self.rosterDayCycleInSeconds) // day_in_seconds
                hourAtNow = int(getDateTime(self.env.now, '%H'))
                currentDay_seconds = self.getCurrentDaySec(self.env)
                quitWait = currentDay_seconds
                resoruceTimeIndex = None
                # find the closest time window that has the skill for this event
                if hourAtNow < self.dayTimeWindowMaps[dayIndex][0].start:
                    resoruceTimeIndex = 0
                elif hourAtNow >= self.dayTimeWindowMaps[dayIndex][-1].end:
                    resoruceTimeIndex = 0
                    quitWait += day_in_seconds
                    dayIndex += 1
                    dayIndex = dayIndex % (self.rosterDayCycleInSeconds / day_in_seconds)
                else:
                    for idx, shiftWindow in enumerate(self.dayTimeWindowMaps[dayIndex]):
                        # print(shiftWindow)
                        if hourAtNow >= shiftWindow.start and hourAtNow < shiftWindow.end:
                            # at this hour, look for the resource, if there is no such resource, need to wait until the time window that contains a shift
                            # to handle this event
                            resoruceTimeIndex = idx
                            break
                # if not pass the last shift today, day index is still  today
                # otherwise dayIndex is next day
                # if next day event is not in the first shift
                # wait for the second shift to start next day
                timeIndex = self.dayTimeWindowMaps[dayIndex][resoruceTimeIndex].timeIndex
                if dayIndex in self.rosterEventResourceQuery \
                        and timeIndex in self.rosterEventResourceQuery[dayIndex] \
                        and transitionName in self.rosterEventResourceQuery[dayIndex][timeIndex]:
                    foundResourceSet = True
                if not foundResourceSet:
                    # wait until next shift?
                    debug_output(f'no resrouce set for this {event} at {format_time(self.env.now)}')
                    # next shift start at
                    nextResourceIndex = (resoruceTimeIndex + 1) % len(self.dayTimeWindowMaps[dayIndex])
                    # resoruceTimeIndex = 1, nextResourceIndex = 0,
                    waitTime = self.dayTimeWindowMaps[dayIndex][
                                   nextResourceIndex].start * seconds_per_hour + currentDay_seconds - self.env.now
                    if nextResourceIndex <= resoruceTimeIndex:
                        waitTime += day_in_seconds
                    debug_output(f'next resource for this {event} at {format_time(waitTime + self.env.now)}')
                    # if this is true, we are at next day?
                    yield self.env.timeout(waitTime)
                    debug_output(f'request resource for this {event} at {format_time(self.env.now)}')

            queryResources = self.rosterEventResourceQuery[dayIndex][timeIndex][transitionName]

            quitTime = self.dayTimeWindowMaps[dayIndex][
                           resoruceTimeIndex].end * seconds_per_hour + quitWait - self.env.now
            quitEvent = self.env.timeout(quitTime)

            foundFree = False
            freeResource = None
            req = None
            for key, r in queryResources.items():
                requestedResource: MyPriorityResource = r
                if requestedResource.at_least_one_free():
                    # no need to reqeust more resources
                    foundFree = True
                    freeResource = requestedResource
                    break
            if not foundFree:
                for key, r in queryResources.items():
                    requestedResource: MyPriorityResource = r

                    newRequest = requestedResource.request(event=NewJobEvent(worker_id=None, job_id=event.id),
                                                           priority=priority)
                    resourceDict[newRequest] = requestedResource
                    requestOrEvents.append(newRequest)

                debug_output(f'reqeust for {event} at {format_time(self.env.now)}')
                waitEvents = [quitEvent] + requestOrEvents
                result = yield self.env.any_of(waitEvents)

                if quitEvent in result:
                    # too much waiting, quit
                    debug_output(f'quit wait at {format_time(self.env.now)} for {event.case_id}')
                    for req in requestOrEvents:
                        req.cancel()
                    # request night batch
                else:
                    # job is done
                    success = True
                    debug_output(f'success  at {format_time(self.env.now)} for {event.case_id}')
                    freeResourceRequest: MyPriorityRequest = None
                    for e in result.events:
                        e1: MyPriorityRequest = e
                        if e1.triggered:
                            freeResourceRequest = e1
                            freeResource = resourceDict[e1]
                            break
                    for e in requestOrEvents:
                        e1: MyPriorityRequest = e
                        if e1 != freeResourceRequest and not e1.triggered:
                            # in the queue list
                            e1.cancel()
                        elif e1 != freeResourceRequest:
                            assert (e1.triggered)
                            # remove from the user list
                            resourceDict[e1].release(e1)
                    w: Worker = result[freeResourceRequest]
            else:
                freeResourceRequest = freeResource.request(event=NewJobEvent(worker_id=None, job_id=event.id),
                                                           priority=priority)
                result = yield self.env.any_of([freeResourceRequest, quitEvent])
                if quitEvent in result:
                    # too much waiting, quit
                    debug_output(f'quit wait at {format_time(self.env.now)} for {event.case_id}')
                    freeResourceRequest.cancel()
                    # request night batch
                else:
                    # job is done
                    success = True
                    w = result[freeResourceRequest]
        return freeResource, freeResourceRequest, resoruceTimeIndex, w

    def load_next_job_set_by_day(self, period=step, job_bin=None):
        global totalJobs
        load_next = True
        last_day = (self.nb_days + 1) * day_in_seconds
        while load_next:
            if self.env.now >= last_day:
                return
            at_least_one = False
            job_included = []
            dayIndex = getDateTime(self.env.now, '%Y-%m-%d')
            hasDataForToday = dayIndex in job_bin
            if hasDataForToday:
                jobs = job_bin[dayIndex]
                new_cases = []
                jobs.apply(self.event_row_process_new_cases, new_cases=new_cases, axis=1)
                self.totalJobs = self.totalJobs + len(new_cases)
                for case_id in new_cases:

                    if True:
                        # G = create_dependcy_for_case(case_id)
                        job_included.append(case_id)
                        # eventsArray, dependentEventDict = generate_dependent_signals(G)
                        eventsArray, dependentEventDict = self.generate_dependent_from_dict(case_id, self.env)

                        nonRosterEventArray = {}
                        masterEventArray = {}

                        keysToDelete = []
                        for key in eventsArray:
                            if key.group in self.nonRosterResourceGroups:
                                nonRosterEventArray[key] = eventsArray[key]
                                keysToDelete.append(key)
                            if key.actualName in masterEvents:
                                masterEventArray[key]= eventsArray[key]
                                keysToDelete.append(key)

                        # grop events into batch jobs and non batch jobs

                        for key in eventsArray:
                            if key not in keysToDelete:
                                self.env.process(self.nodeProcess(key, eventsArray, dependentEventDict))
                            if key in nonRosterEventArray:
                                self.env.process(self.nodeNonRoster( key, nonRosterEventArray, dependentEventDict))
                            if key in masterEventArray:
                                self.env.process(self.nodeMasterEVENT(key, masterEventArray, dependentEventDict))
                        #if len(singleResourceEventsArray) > 0:
                        #    self.env.process(self.singleResoruceProcess(self.env, singleResourceEventsArray, dependentEventDict))
                        at_least_one = True

                if not at_least_one and self.env.now >= last_day:
                    load_next = False
                print(f'load jobs {len(job_included)} at {format_time(self.env.now)}')

            yield self.env.timeout(step)
            period = period + step




    def isEventNoUseResource(self, eventName):
        return eventName == noResNeeded

    def nodeMasterEVENT(self,  event, eventsArray, dependentEventDict):
        if event.is_start:
            # must wait for its arrival
            arrival = self.caseArrivals[event.case_id]
            yield self.env.timeout(arrival - self.env.now)
        # print(event)
        debug_output(f'try do {event} at {format_time(self.env.now)}')
        # for each node, all predessors need to finish before continue
        yield self.env.all_of(eventsArray[event])
        event.set_ready_time(self.env.now)
        debug_output(f'{event.id}, parents are fnished, {eventsArray[event]}')
        debug_output(f'want resource do this task {event} at time {format_time(self.env.now)}')
        # try dellee unused memory
        # del eventsArray[node]
        # del dependentEventDict[node]
        event.set_sim_start(self.env.now)

        doLabFirst = False
        if doLabFirst:
            doLab = self.env.process(self.labSubevent(lastId=event.id*10,event_parents=event.parents, case_id=event.case_id))
            # now I can do lab or radio in any order
            # let me do lab first
            # a master event is composed of a serires of actual events, these events ahve actual resource defs
            # the returned paretns contrain the ?
            labId, event_parents = yield  doLab

            radioP = self.env.process(self.radioSubevent( lastEventId=labId, case_id=event.case_id, event_parents=event_parents))
            radioId, event_parents = yield radioP
        else:
            radioP = self.env.process(
                self.radioSubevent( lastEventId=event.id*10, case_id=event.case_id, event_parents=event.parents))
            radioId, event_parents = yield radioP

            doLab = self.env.process(
                self.labSubevent( lastId=radioId, event_parents=event.parents, case_id=event.case_id))
            # now I can do lab or radio in any order
            # let me do lab first
            # a master event is composed of a serires of actual events, these events ahve actual resource defs
            # the returned paretns contrain the ?
            labId, event_parents = yield doLab


        priority = 0
        debug_output(f'{event.actualName} want res {event.group}')
        duration = event.duration
        if event.actualName in wardBedEvents:
            # for ward bed, assume bed will be ok to request based on a time profile
            hourAtNow = int(getDateTime(self.env.now, '%H'))
            durationBeforeRequest = EmergencyDataDistribution.durationInHourToGetWardBed(hourAtNow) * seconds_per_hour
            yield self.env.timeout(durationBeforeRequest)
            # warning: the following is not used yet
            # release the bed in 2 or 3 days at 10am
            # first check 2 days time
            twoDaysFromNow = self.env.now + 2 * day_in_seconds
            # get the hour
            hourAtTwoDays = int(getDateTime(twoDaysFromNow, '%H'))
            if hourAtTwoDays > 10:
                # release in 3 days
                duration = self.getCurrentDaySec(self.env) + 3 * day_in_seconds + 10 * seconds_per_hour - self.env.now
            else:
                # release in 2 days
                duration = self.getCurrentDaySec(self.env) + 2 * day_in_seconds + 10 * seconds_per_hour - self.env.now




        event.set_resource(event.group)


        event.set_sim_end(self.env.now)
        # this master event is finished with all sub events, notify all children
        for key, eventList in dependentEventDict.items():
            if (len(eventList) == 0): continue
            for parent in eventList:
                if event.id == parent[0]:
                    debug_output(f' for node {key}, the dependend event is succed {event}')
                    parent[1].succeed()

    def radioSubevent(self, lastEventId, case_id, event_parents):
        radioId = lastEventId + 1
        start = self.env.now
        finish = self.env.now + 10 * 60
        activity = radio
        is_start = False
        priority = 0

        radioEvent = EdEvent(radioId, start, finish, event_parents, activity, case_id, is_start, priority)
        assert (radioId not in self.allEvents)
        self.allEvents[radioId] = radioEvent
        self.allCases[case_id].append(radioEvent)
        # call the nodeProcess for this new event with correct params
        newEventEventArray = {radioEvent: []}
        newEventDependentEventDict = {}
        radioEventProcess = self.env.process(
            self.nodeProcess(radioEvent, newEventEventArray, newEventDependentEventDict))
        event_parents.append(radioId)
        yield radioEventProcess
        return radioId, event_parents

    def labSubevent(self, lastId, event_parents, case_id):
        transitionName = getBloodForLab
        # create these actual events on the go
        labId = lastId + 1
        start = self.env.now
        finish = self.env.now + 10 * 60

        activity = getBloodForLab
        is_start = False
        priority = 0

        labEvent = EdEvent(labId, start, finish, event_parents, activity, case_id, is_start, priority)
        assert (labId not in self.allEvents)
        self.allEvents[labId] = labEvent
        self.allCases[case_id].append(labEvent)
        # call the nodeProcess for this new event with correct params
        newEventEventArray = {labEvent: []}
        newEventDependentEventDict = {}
        labEventProcess = self.env.process(self.nodeProcess(labEvent, newEventEventArray, newEventDependentEventDict))
        yield labEventProcess
        event_parents.append(labId)
        return labId, event_parents

    def nodeNonRoster(self,  event, eventsArray, dependentEventDict):
        if event.is_start:
            # must wait for its arrival
            arrival = self.caseArrivals[event.case_id]
            yield self.env.timeout(arrival - self.env.now)
        # print(event)
        debug_output(f'try do {event} at {format_time(self.env.now)}')
        # for each node, all predessors need to finish before continue
        yield self.env.all_of(eventsArray[event])
        event.set_ready_time(self.env.now)
        debug_output(f'{event.id}, parents are fnished, {eventsArray[event]}')
        debug_output(f'want resource do this task {event} at time {format_time(self.env.now)}')

        # try dellee unused memory
        # del eventsArray[node]
        # del dependentEventDict[node]
        transitionName = event.actualName
        priority = 0
        debug_output(f'{transitionName} want res {event.group}')
        duration = event.duration
        if event.actualName in wardBedEvents:
            # for ward bed, assume bed will be ok to request based on a time profile
            hourAtNow = int(getDateTime(self.env.now, '%H'))
            durationBeforeRequest = EmergencyDataDistribution.durationInHourToGetWardBed(hourAtNow) * seconds_per_hour
            yield self.env.timeout(durationBeforeRequest)
            # release the bed in 2 or 3 days at 10am
            # first check 2 days time
            twoDaysFromNow = self.env.now + 2 * day_in_seconds
            # get the hour
            hourAtTwoDays = int(getDateTime(twoDaysFromNow, '%H'))
            if hourAtTwoDays > 10:
                # release in 3 days
                duration = self.getCurrentDaySec() + 3 * day_in_seconds + 10 * seconds_per_hour - self.env.now
            else:
                # release in 2 days
                duration = self.getCurrentDaySec() + 2 * day_in_seconds + 10 * seconds_per_hour - self.env.now

        res = self.nonRosterResource[event.group]

        req =  res.request(priority=event.priority)
        yield req
        debug_output(f'get bed at {format_time(self.env.now)}')

        # check if we can release ed bed
        self.release_ed_bed(event)

        # if i get the bed at ED, i want to realease the bed after nurse process
        if event.group in self.ed_bed_release_later:
            self.ed_bed_release_later[event.group][event.case_id] = [res, req, event]

        event.set_sim_start(self.env.now)
        event.set_resource(event.group)


        yield self.env.timeout(duration)
        event.set_sim_end(self.env.now)
        # release the bed immediately, not depend on other events
        if event.group not in self.ed_bed_release_later:

            res.release(req)

        for key, eventList in dependentEventDict.items():
            if (len(eventList) == 0): continue
            for parent in eventList:
                if event.id == parent[0]:
                    debug_output(f' for node {key}, the dependend event is succed {event}')
                    parent[1].succeed()

    def nodeProcess(self,  event, eventsArray, dependentEventDict):
        if event.is_start:
            # must wait for its arrival
            arrival = self.caseArrivals[event.case_id]
            yield self.env.timeout(arrival - self.env.now)
        # print(event)
        debug_output(f'try do {event} at {format_time(self.env.now)}')
        # for each node, all predessors need to finish before continue
        yield self.env.all_of(eventsArray[event])
        event.set_ready_time(self.env.now)
        debug_output(f'{event.id}, parents are fnished, {eventsArray[event]}')
        debug_output(f'want resource do this task {event} at time {format_time(self.env.now)}')
        # try dellee unused memory
        # del eventsArray[node]
        # del dependentEventDict[node]
        transitionName = event.actualName
        priority = 0
        # get skill set resurces
        # get time index
        duration = event.duration

        isResourceNotNeeded = event.group == noResNeeded
        if not isResourceNotNeeded:


            freeResource, freeResourceRequest, w = yield self.env.process(
                self.GetFreeRosterResource(event, priority))

            event.set_resource(w.name)


        event.set_sim_start(self.env.now)

        debug_output(f'I can do this node {event} at time {format_time(self.env.now)}')
        yield self.env.timeout(duration)
        # check if we can release ed bed
        self.release_ed_bed(event)
        event.set_sim_end(self.env.now)
        if not isResourceNotNeeded:
            freeResource.release(freeResourceRequest)
        debug_output(f' I can finish this node {event} at time {format_time(self.env.now)}')

        # if another node depends this node to fnish, notify that node this node is finished
        for key, eventList in dependentEventDict.items():
            if (len(eventList) == 0): continue
            for parent in eventList:
                if event.id == parent[0]:
                    debug_output(f' for node {key}, the dependend event is succed {event}')
                    parent[1].succeed()
        # this request needs to know the associated resource

    def release_ed_bed(self, event):
        if event.actualName in edBedReleaseEvent and event.case_id in self.ed_bed_release_later[edBedRes]:
            self.ed_bed_release_later[edBedRes][event.case_id][0].release(self.ed_bed_release_later[edBedRes][event.case_id][1])

    def collect_stats(self, finishTag='finish', startStats=1, endStats=28):
        grouped_roster = self.rosterData['grouped_roster']
        eventLog = []
        queueCount = []
        totalCount = {}
        singleRound = []

        singleRoundSpanByDay = {}

        totalSpan = []
        # I want to plot the wait time before going to next stage
        # [case_id, event, wait time]
        waiTimePerCasePerEventSingleRound = []

        # I want to count number of items each worker done for a particular day, and hour
        resourceProductivity = {}  # key = date, hour, resource name, value is count
        for case, events in self.allCases.items():
            dataAppender = singleRound
            spanByDay = singleRoundSpanByDay
            waitTimeAppender = waiTimePerCasePerEventSingleRound

            arrival = self.caseArrivals[case]
            day = arrival // day_in_seconds
            sortedEvents: list[Event] = sorted(events, key=lambda e: e.id)


            if day not in spanByDay:
                spanByDay[day] = []

            if True:
                startTime = sortedEvents[0].start
                # calcualte the upper bound for finish time
                # if this job arrive befor 12 and is a lunch batch, upper bound is 2pm
                # if this job arrive after 12 and is a lunch batch, upper bound is next day 5am, not sure about weekend
                # if this job arrive after 12 and is a night batch, upper bound is next day 5am
                # if this job arrive before 12 and is a night batch, upper bound is next day 5am
                firstEvent = sortedEvents[0]
                arrivalHour = int(getDateTime(firstEvent.start, '%H'))
                # use average waiting time for access and grossing to estimate the minimum time

                finishTime = sortedEvents[-1].get_sim_end()
                duration = finishTime - startTime
                conversion = timedelta(seconds=int(duration))
                converted_time = str(conversion)
                totalSpan.append(duration)
                spanByDay[day].append(duration)

                # makeSpans.append([sortedEvents[0].case_id, format_time(startTime), converted_time, converted_delta])

                for e in sortedEvents:
                    event = e

                    datesStr = getDateTime(n=event.get_sim_start(), out_date_format_str='%m/%d/%Y')
                    hourStr = getDateTime(n=event.get_sim_start(), out_date_format_str='%H')
                    useResource = event.useResource
                    if useResource is not None:
                        k: product_count_type = product_count_type(day=datesStr, time=hourStr, resource=useResource)
                        if resourceProductivity.get(k) is None:
                            resourceProductivity[k] = UserCount(key=k, group=event.group)
                        resourceProductivity[k].accumulate()

                    eventDuration = event.get_sim_end() - event.get_sim_start()
                    data = [event.case_id, event.actualName, format_time(event.get_sim_start()),
                            format_time(event.get_sim_end()),
                            event.get_resource(), event.id, event.parents, event.group, eventDuration]
                    #print(event)
                    if event.ready_time < event.get_sim_start():
                        # this event is queued for processing
                        eventLog.append([event.group, format_time(event.ready_time), 'ready'])
                        # get it from the queue
                        eventLog.append([event.group, format_time(event.get_sim_start()), 'start'])
                    if event.group not in totalCount:
                        totalCount[event.group] = 0
                    totalCount[event.group] += 1
                    dataAppender.append(data)

                    if len((event.parents)) > 0:
                        # for this event, I know its resource, if the rouse group has ppl on the weekend, don't minus weekend
                        # otherwise, basically minus days that ppl are not around on the weekend
                        waitDif = timedelta(seconds=int(event.get_sim_start() - event.ready_time))

                        job_start_time = SimSetting.start_time_sim + timedelta(seconds=event.get_sim_start())
                        job_ready_time = SimSetting.start_time_sim + timedelta(seconds=event.ready_time)
                        job_start_week = job_start_time.isocalendar()[1]
                        job_ready_week = job_ready_time.isocalendar()[1]
                        if event.group not in self.nonRosterEventResource and job_start_week - job_ready_week == 1:

                            resource_group = event.group
                            job_ready_week_start_monday = job_ready_time - timedelta(days=job_ready_time.weekday() % 7)
                            job_ready_week_start_sat = job_ready_week_start_monday + timedelta(days=5)
                            job_ready_week_start_sat = job_ready_week_start_sat.replace(minute=0, second=0, hour=0)
                            job_ready_week_start_sun = job_ready_week_start_monday + timedelta(days=6)
                            job_ready_week_start_sun = job_ready_week_start_sun.replace(minute=0, second=0, hour=0)
                            # print('ready week', job_ready_time, job_ready_week, 'start processing week',  job_start_time, job_start_week, 'ready week monday',
                            #      job_ready_week_start_monday, job_ready_week_start_sat, job_ready_week_start_sun)
                            isWorkingOnSat = job_ready_week_start_sat in grouped_roster[resource_group]
                            isWorkingOnSun = job_ready_week_start_sun in grouped_roster[resource_group]
                            if not isWorkingOnSat:
                                waitDif = waitDif - timedelta(days=1)
                            if not isWorkingOnSun:
                                waitDif = waitDif - timedelta(days=1)
                        # print(job_ready_week_start_sat in grouped_roster[resource_group])

                        # print(job_ready_week_start_sun in grouped_roster[resource_group],waitDif)

                        waitTime = [event.case_id, event.actualName,
                                    waitDif, waitDif.total_seconds(),
                                    format_time(self.caseArrivals[event.case_id])]
                        waitTimeAppender.append(waitTime)
        outputs = {}

        for stage, queueData in self.queueMonitor.items():
            queueData = pd.DataFrame(queueData, columns=['stage', 'time', 'queue'])
            queueData = queueData.sort_values(['time'], ascending=(True,))
            queueData.to_csv(f'{stage}_queue.csv', index=False)
            outputs[f'{stage}_queue']= queueData


        productCountData = []
        for key, value in resourceProductivity.items():
            key: product_count_type = key
            value: UserCount = value
            dateStr = key.day
            timeStr = key.time
            resource = key.resource
            group = value.group
            productCountData.append([dateStr, timeStr, resource, group, value.count])

        eventLog = pd.DataFrame(eventLog, columns=['event', 'time', 'type'])
        eventLog = eventLog.sort_values(['time'], ascending=(True,))
        eventLog.to_csv('eventReplay.csv', index=False)

        breakLog = pd.DataFrame(self.breakDebug, columns=['stage', 'start', 'expected start', 'finish'])
        batchLog = breakLog.sort_values(['start'], ascending=(True,))
        batchLog.to_csv('breakLog.csv', index=False)

        batchLog = pd.DataFrame(self.batchDebug, columns=['id', 'start', 'finish'])
        batchLog = batchLog.sort_values(['start'], ascending=(True,))
        batchLog.to_csv('batchLog.csv', index=False)

        productCountDataLog = pd.DataFrame(productCountData, columns=['day', 'timeIndex', 'name', 'group', 'count'])
        productCountDataLog = productCountDataLog.sort_values(["day", "timeIndex", 'group'],
                                                              ascending=(True, True, True))

        # Use GroupBy() to compute the sum

        productCountDataLog.to_csv('productivity.csv', index=False)

        for key, value in self.queueStat.items():
            queueCount.append([key[0], key[1], key[2], value])
        queueLog = pd.DataFrame(queueCount, columns=['day', 'timeIndex', 'event', 'count'])
        queueLog = queueLog.sort_values(["day", "timeIndex"], ascending=(True, True))

        dataLog = pd.DataFrame(singleRound,
                                   columns=['case_id', 'event', 'start', 'finish', 'resource', 'id', 'parents', 'group',
                                            'duration'])
        dataLog = dataLog.sort_values(["id"], ascending=(True))
        # dataLog = dataLog.drop(columns=['parents'])
        dataLog.to_csv('single-round.csv', index=False)


        frames = [dataLog]
        allDataLog = pd.concat(frames)
        allDataLog = allDataLog.sort_values(["id"], ascending=(True))
        allDataLog.to_csv('allDataLog.csv', index=False)


        totalSpan = statistics.mean(totalSpan)
        conversion = timedelta(seconds=int(totalSpan))
        converted_time = str(conversion)
        # print(converted_time)

        waitDataLog = pd.DataFrame(waiTimePerCasePerEventSingleRound,
                                       columns=['case_id', 'event', 'wait', 'waitInSec', 'arrival'])
        waitDataLog.to_csv('wait-single.csv', index=False)


        # print(f'total duration is {converted_time}')


        outputs['single-wait'] = waitDataLog

        outputs['single-data'] = dataLog

        outputs['single-span'] = singleRoundSpanByDay

        outputs['totalCount'] = totalCount
        outputs['queue'] = queueLog
        outputs['productivity'] = productCountDataLog
        outputs['replay'] = eventLog
        outputs['allData'] = allDataLog
        return outputs

    def RunSim(self, jobs=None, rosterData=None, isLocal=True):

        self.clear()

        global onLocal
        onLocal = isLocal
        if jobs is not None:
            job_bin = jobs
        else:
            print('bin event import')
            # day_data_windows, job_bin = bin_events(log)

            job_bin = loadData()
            print('bin event import finished')



        print('bin event import finished')
        if rosterData is not None:
            self.set_roster(rosterData)
            self.create_resource_from_roster(rosterData)
            # print(resourceLog.dtypes)

        # print(self.rosterResoruceData)



        self.create_non_roster_res()
        # print(self.nonRosterResource)

        # debug_output(self.dynamicEventResourceQuery)
        # debug_output(self.eventResourceQuery)
        # I need to know for each worker, the timewidnow it is in and worker id and resoruce it is in, so I can request break when transiioning from one window to next window


        start = time.time()
        print(f'start sim at {format_time(10)}')
        self.env.process(self.load_next_job_set_by_day(job_bin=job_bin))
        # env.process(self.monitorQueue(env=env))
        # env.process(self.monitorQueue(env=env, transitionName='accessioning'))
        if self.runUntil is not None:
            self.env.run(until=self.runUntil * day_in_seconds)
        else:
            self.env.run()
        end = time.time()
        print(f'finish sim at {end}')
        print('time spent', end - start)


def get_average(resourceLog, searchObj, simObj):
    d, span = simObj.collect_stats()
    overallMean = 0
    meanLog = []
    for day, values in span.items():
        averageMean = statistics.mean(values)
        overallMean += averageMean
        conversion = timedelta(seconds=int(averageMean))
        converted_time = str(conversion)
        dayStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        print(f'total duration is day {dayStr} = {converted_time}')
        meanLog.append([dayStr, converted_time])
    overallMean = overallMean / len(span)
    searchObj.append(local_search_value(average=overallMean, resource=resourceLog))
    meanLog = pandas.DataFrame(meanLog, columns=['day', 'duration'])

    return meanLog


def RunLocalSim(jobs=None, isLocal=True, staffRosterDict = None, recorder={}, nonRosterEventResource = None, resoures_count=None):
    runUntil = 20
    rosterData = {'roster_dict': staffRosterDict, 'roster_df': rosterDF(staffRosterDict),
                  'roster_week': SimSetting.rosterWeekDayList, 'grouped_roster': group_roster_by_date(staffRosterDict)}
    simObj = EdRosterSimulation(isLocalSim=isLocal, nonRosterEventResource=nonRosterEventResource)

    simObj.set_run_until(days=runUntil)

    #print(resourceRosterLog.dtypes)
    #eventsNoUseResource = [ambPickup, finishTag, ambArrive, radio, walkin, getRadio]
    nonRosterResGroups = [edBedRes, cardiologyBedRes, orthopedicsBedRes]
    simObj.set_batch_data(nonRosterResGroups=nonRosterResGroups)
    simObj.RunSim(rosterData=rosterData, isLocal=isLocal, jobs = jobs)
    #simObj.RunSim(resourceLog=resourceRosterLog, jobs=jobs, timeWindowMapLog=timeWindowMapLog, isLocal=isLocal)

    outputs = simObj.collect_stats(finishTag='Finish')

    span = outputs['single-span']
    meanLog = showSpan(span, 'single')
    meanLog.to_csv('span.csv', index= False)
    recorder['single-span'] = meanLog

    waitLog = outputs['single-wait']
    waitLog.to_csv('wait.csv', index=False)
    waitMean = showWaitDuration(waitLog)
    recorder['single-wait'] = waitMean

    recorder['outputs'] = outputs
    uMean = showWaitDuration(waitLog)
    a = f'number of nurses = {resoures_count[nurseRes]}, number of ED beds = {simObj.nonRosterEventResource[edBedRes]} '
    conversion = str(timedelta(seconds=int(uMean[edTreat])))
    b = f'wait time for {edTreat} is {conversion}'
    c = f'ward bed profile is {EmergencyDataDistribution.profile}'
    EdRosterSimulation.policyResults.append(
        [resoures_count[nurseRes], simObj.nonRosterEventResource[edBedRes], str(EmergencyDataDistribution.profile),
         conversion])

    print(EdRosterSimulation.policyResults)
    if isLocal:

        with open('result.txt', 'a') as f:
            f.write(a)
            f.write('\n')
            f.write(c)
            f.write('\n')
            f.write(b)
            f.write('\n')
            f.write('\n')
    return recorder




def showWaitDuration(waitLog):
    print(waitLog.dtypes)
    uMean = waitLog.groupby(['event'])['waitInSec'].mean()
    debug_output(f'event waiting stat is {uMean}')
    return uMean


def showSpan(span, columnName=''):
    meanLog = []
    for day, values in span.items():
        totalSpan = statistics.mean(values)
        conversion = timedelta(seconds=int(totalSpan))
        converted_time = str(conversion)
        dayStr = getDateTime(day * day_in_seconds, '%Y-%m-%d')
        #print(f'total duration is day {dayStr} = {converted_time}')
        meanLog.append([dayStr, converted_time, totalSpan / day_in_seconds])

    meanLog = pandas.DataFrame(meanLog, columns=['day', f'{columnName}-duration', f'{columnName}-durationInDays'])
    #print(meanLog)

    return meanLog



def eval(isLocal = True, jobs = None):

    import os
    if isLocal:
        if os.path.exists("result.txt"):
            os.remove("result.txt")
        else:
            pass

    EdRosterSimulation.clear_resutls()

    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 2, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)


    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 4, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)
    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 10, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)
    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 20, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)

    nonRosterEventResource = {edBedRes: 6, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 10, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)

    nonRosterEventResource = {edBedRes: 8, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 10, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)
    # change ward bed profile
    EmergencyDataDistribution.profile[16] = 900000

    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 2, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)

    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 4, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)
    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 8, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)
    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 10, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)

    nonRosterEventResource = {edBedRes: 4, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 20, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)

    nonRosterEventResource = {edBedRes: 6, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 10, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)

    nonRosterEventResource = {edBedRes: 8, orthopedicsBedRes: 200, cardiologyBedRes: 200}
    resoures_count = {nurseRes: 10, physicanRes: 2, registerRes: 2, labRes: 2, radioRes: 2}
    roster, datTimeMap = EmergencyDataDistribution.event_to_resource_group(resoures_count)
    RunLocalSim(isLocal=isLocal, jobs=jobs, resourceRosterLog=roster, runUntil=10,
                timeWindowMapLog=datTimeMap, nonRosterEventResource=nonRosterEventResource,
                resoures_count=resoures_count)
    policyResultsLog = pandas.DataFrame(EdRosterSimulation.policyResults, columns =['no. nurse', 'no. ED beds', 'Ward bed profile', 'Wait to get ED bed'])
    policyResultsLog.to_csv('policy.csv', index=False)
    return policyResultsLog