import collections
import random

from EmergencyDptDistribution import *

from pm4py.visualization.petri_net.variants.token_decoration_frequency import *
from pm4py.objects.petri_net import semantics

from copy import copy, deepcopy
from  pm4py.visualization.petri_net  import visualizer as pn_visualizer
from pm4py.objects.process_tree import obj
from pm4py.visualization.process_tree import visualizer as pt_visualizer

from pm4py.objects.conversion.process_tree import converter as pt_converter

task_type = collections.namedtuple('task_type', 'transitions weights')
resource_data = collections.namedtuple('event_name', 'resource_names')



startEventTag = 'start'
resourceTag = 'resourceTag'
onSpark = False

durationInfo = 'durationInfo'

import threading

_uid = threading.local()
def genuid():
    if getattr(_uid, "uid", None) is None:
        _uid.tid = threading.current_thread().ident
        _uid.uid = 0
    _uid.uid += 1

    return _uid.uid

class MyToken():
    def __init__(self, place: PetriNet.Place = None, parent = None, name = None):


        self.place = place
        self.id = genuid()
        self.parent = parent
        self.name = name
        self.label = None
        # a token comes from a unique tranisions
        self.parentTrans: 'MyTransition' = None
        self.birth_time = None
    def set_birth(self, time):
        self.birth_time = time
    def parent_trans(self, trans):
        self.parentTrans = trans

    def show_parent(self):
        debug_output(f'parents are ')
        for parent in self.parent:
            debug_output(f'parent')
    def __hash__(self):
        return self.id
    # str call repr?
    def __repr__(self):
        if self.label is None:
            return "( id =" + str(self.id) + ' ' + str(self.name) + ")"
        else:
            return "(" + str(self.name) + ", '" + str(self.label) + "')"

class MyTransition():
    def __init__(self, trans : PetriNet.Transition = None, eventIdGenerator = None):
        self.transition = trans
        self.id = next(eventIdGenerator)
        self.parent = None
        self.birth = None
        self.duration = 0

    def set_duration(self, duration):
        self.duration = duration
    def finish_time(self):
        return self.birth + self.duration

    def set_birth(self, time):
        self.birth = time
    def __hash__(self):
        return self.id
    def __repr__(self):
        if self.transition:
            return "(" + self.transition.name +")"
    def set_parent(self, parent):
        self.parent = (parent)
    def get_parent(self):
        return self.parent
    def __str__(self):
        return self.__repr__()
class MyMarking(Marking):
    def __init__(self,   iterable=None, /, **kwds):
        super().__init__()
        self.id = genuid()


def debug_output( str, show = False):
    show =  False
    if show:
        print(str)



def edProcess():


    metaData = {}
    metaData[durationInfo] = {}
    seq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=None, children=None, label=None)


    trans0 = obj.ProcessTree(operator=None, parent=seq, children=None, label=ambPickup)
    metaData[durationInfo][ambPickup] = EmergencyDataDistribution.event_duration(ambPickup)
    trans1 = obj.ProcessTree(operator=None, parent=seq, children=None, label=ambArrive)
    metaData[durationInfo][ambArrive] = EmergencyDataDistribution.event_duration(ambArrive)



    seq.children.append(trans0)
    seq.children.append(trans1)

    p = random.uniform(0, 1)
    if p < 0.5:
        transResuscitation = obj.ProcessTree(operator=None, parent=seq, children=None, label=resuscitation)
        metaData[durationInfo][resuscitation] = EmergencyDataDistribution.event_duration(resuscitation)
        seq.children.append(transResuscitation)

    trans2 = obj.ProcessTree(operator=None, parent=seq, children=None, label=handOver)
    metaData[durationInfo][handOver] = EmergencyDataDistribution.event_duration(handOver)
    seq.children.append(trans2)


    trans4 = obj.ProcessTree(operator=None, parent=seq, children=None, label=edTreat)
    metaData[durationInfo][edTreat] = EmergencyDataDistribution.event_duration(edTreat)

    trans5 = obj.ProcessTree(operator=None, parent=seq, children=None, label=nurseAccess)
    metaData[durationInfo][nurseAccess] = EmergencyDataDistribution.event_duration(nurseAccess)


    trans6 = obj.ProcessTree(operator=None, parent=seq, children=None, label=physicanAsscess)
    metaData[durationInfo][physicanAsscess] = EmergencyDataDistribution.event_duration(physicanAsscess)

    trans7 = obj.ProcessTree(operator=None, parent=seq, children=None, label=nurseProcess)
    metaData[durationInfo][nurseProcess] = EmergencyDataDistribution.event_duration(nurseProcess)


    #seq.children.append(trans3)
    seq.children.append(trans4)
    seq.children.append(trans5)
    seq.children.append(trans6)
    seq.children.append(trans7)
    if False:
        labRadioTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=labRadio)
        metaData[durationInfo][labRadio] = 10 * 60
        seq.children.append(labRadioTrans)
    x = random.choices(['no_lab_radio', 'lab', 'radio', 'both'], [0.1,0.1,0.1,0.7], k=1)[0]
    x = 'both'
    if x == 'both':


        trans8 = obj.ProcessTree(operator=None, parent=seq, children=None, label=getBloodForLab)
        metaData[durationInfo][getBloodForLab] = EmergencyDataDistribution.event_duration(getBloodForLab)
        seq.children.append(trans8)
        #once lab blood is drawn, patient goes to radio, the lab sample in parallel is set to lab to get result




        multipleChecks = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=seq, children=None, label=None)
        seq.children.append(multipleChecks)

        bloodFlowTrans = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multipleChecks, children=None, label=None)
        multipleChecks.children.append(bloodFlowTrans)

        sentForLabTrans = obj.ProcessTree(operator=None, parent=bloodFlowTrans, children=None, label=sentForLab)
        bloodFlowTrans.children.append(sentForLabTrans)
        metaData[durationInfo][sentForLab] = EmergencyDataDistribution.event_duration(sentForLab)

        labResultTrans = obj.ProcessTree(operator=None, parent=bloodFlowTrans, children=None, label=getLabResult)
        bloodFlowTrans.children.append(labResultTrans)

        radioFlowTrans = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multipleChecks, children=None, label=None)
        multipleChecks.children.append(radioFlowTrans)

        trans9 = obj.ProcessTree(operator=None, parent=radioFlowTrans, children=None, label=radio)
        metaData[durationInfo][radio] = EmergencyDataDistribution.event_duration(radio)
        radioFlowTrans.children.append(trans9)

        readioResultTrans = obj.ProcessTree(operator=None, parent=radioFlowTrans, children=None, label=getRadio)
        radioFlowTrans.children.append(readioResultTrans)

        metaData[durationInfo][getLabResult] = EmergencyDataDistribution.event_duration(getLabResult)
        metaData[durationInfo][getRadio] = EmergencyDataDistribution.event_duration(getRadio)



    if x == 'lab':
        trans8 = obj.ProcessTree(operator=None, parent=seq, children=None, label=getBloodForLab)

        metaData[durationInfo][getBloodForLab] = EmergencyDataDistribution.event_duration(getBloodForLab)
        seq.children.append(trans8)


        sentForLabTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=sentForLab)
        seq.children.append(sentForLabTrans)
        metaData[durationInfo][sentForLab] = EmergencyDataDistribution.event_duration(sentForLab)


        labResultTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=getLabResult)
        metaData[durationInfo][getLabResult] = EmergencyDataDistribution.event_duration(getLabResult)
        seq.children.append(labResultTrans)

    if x == 'radio':
        trans9 = obj.ProcessTree(operator=None, parent=seq, children=None, label=radio)
        metaData[durationInfo][radio] = EmergencyDataDistribution.event_duration(radio)
        seq.children.append(trans9)

        readioResultTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=getRadio)
        metaData[durationInfo][getRadio] = EmergencyDataDistribution.event_duration(getRadio)
        seq.children.append(readioResultTrans)

    trans10 = obj.ProcessTree(operator=None, parent=seq, children=None, label=physicalEval)
    metaData[durationInfo][physicalEval] = EmergencyDataDistribution.event_duration(physicalEval)
    seq.children.append(trans10)

    y = random.choices(['transfer', 'discharge'], [0.3, 0.7], k=1)[0]
    #y = 'transfer'
    if y == 'transfer':
        value = random.choices([transferCardiology, transferOrthopedics], [0.5, 0.5], k=1)[0]
        value = transferCardiology
        trans11 = obj.ProcessTree(operator=None, parent=seq, children=None, label=value)
        metaData[durationInfo][value] = EmergencyDataDistribution.event_duration(value)
        seq.children.append(trans11)
        if value == transferCardiology:
            getBedAtCardio = obj.ProcessTree(operator=None, parent=seq, children=None, label=bedAtCardiology)
            metaData[durationInfo][bedAtCardiology] = EmergencyDataDistribution.event_duration(bedAtCardiology)
            seq.children.append(getBedAtCardio)
        if value == transferOrthopedics:
            getBedAtOrthopedics= obj.ProcessTree(operator=None, parent=seq, children=None, label=bedAtOrthopedics)
            metaData[durationInfo][bedAtOrthopedics] = EmergencyDataDistribution.event_duration(bedAtOrthopedics)
            seq.children.append(getBedAtOrthopedics)
    if y == 'discharge':

        trans12 = obj.ProcessTree(operator=None, parent=seq, children=None, label=discharge)
        metaData[durationInfo][discharge] = EmergencyDataDistribution.event_duration(discharge)
        seq.children.append(trans12)
    #print(trans11)
    #transferOrDischarge.children.append(trans11)
    #transferOrDischarge.children.append(trans12)
    #seq.children.append(transferOrDischarge)

    trans13 = obj.ProcessTree(operator=None, parent=seq, children=None, label=finishTag)
    metaData[durationInfo][finishTag] = 0
    seq.children.append(trans13)

    tree = seq
    # tree_consistency.fix_parent_pointers(tree)
    # tree = generic.fold(tree)
    # generic.tree_sort(tree)
    gviz = pt_visualizer.apply(tree)
    #pt_visualizer.view(gviz)

    net, initial_marking, final_marking = pt_converter.apply(tree)
    parameters = {pn_visualizer.Variants.FREQUENCY.value.Parameters.FORMAT: "png"}
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    #pt_visualizer.view(gviz)

    p_1 = None
    for p in net.places:
        place: PetriNet.Place = p
        #print(p.name)
        if place.name == 'source':
            p_1 = place
            break

    for t in net.transitions:
        trans: PetriNet.Transition = t
        t.name = t.label
        if trans.label == trans0.label:
            t_1 = trans
    metaData[startEventTag] = [t_1]

    initial_marking = Marking()
    final_marking = Marking()
    tokenMarking = MyMarking()
    # create a token at a place
    token1 = MyToken(place=p_1, name='p_1')
    # token2 = MyToken(place=p_2, name='p_2')
    tokenMarking[token1] = token1
    # tokenMarking[token2] = token2
    initial_marking[p_1] = 1
    metaData['graph'] = gviz

    return net, initial_marking, tokenMarking, metaData


def walkInProcess():


    metaData = {}
    metaData[durationInfo] = {}
    seq = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=None, children=None, label=None)


    trans0 = obj.ProcessTree(operator=None, parent=seq, children=None, label=walkin)
    metaData[durationInfo][walkin] = EmergencyDataDistribution.event_duration(walkin)
    seq.children.append(trans0)
    
    transReg = obj.ProcessTree(operator=None, parent=seq, children=None, label=register)
    metaData[durationInfo][register] = EmergencyDataDistribution.event_duration(register)
    seq.children.append(transReg)

    trans5 = obj.ProcessTree(operator=None, parent=seq, children=None, label=nurseAccess)
    metaData[durationInfo][nurseAccess] = EmergencyDataDistribution.event_duration(nurseAccess)
    seq.children.append(trans5)

    #edTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=edTreat)
    #metaData[durationInfo][edTreat] = EmergencyDataDistribution.event_duration(edTreat)
    #seq.children.append(edTrans)

    trans6 = obj.ProcessTree(operator=None, parent=seq, children=None, label=physicanAsscess)
    metaData[durationInfo][physicanAsscess] = EmergencyDataDistribution.event_duration(physicanAsscess)

    seq.children.append(trans6)

    trans7 = obj.ProcessTree(operator=None, parent=seq, children=None, label=nurseProcess)
    metaData[durationInfo][nurseProcess] = EmergencyDataDistribution.event_duration(nurseProcess)

    seq.children.append(trans7)

    x = random.choices(['no_lab_radio', 'lab', 'radio', 'both'], [0.25 for i in range(4)], k=1)[0]
    #x = 'no_lab_radio'
    if x == 'both':


        trans8 = obj.ProcessTree(operator=None, parent=seq, children=None, label=getBloodForLab)
        metaData[durationInfo][getBloodForLab] = EmergencyDataDistribution.event_duration(getBloodForLab)
        seq.children.append(trans8)
        #once lab blood is drawn, patient goes to radio, the lab sample in parallel is set to lab to get result




        multipleChecks = obj.ProcessTree(operator=obj.Operator.PARALLEL, parent=seq, children=None, label=None)
        seq.children.append(multipleChecks)

        bloodFlowTrans = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multipleChecks, children=None, label=None)
        multipleChecks.children.append(bloodFlowTrans)

        sentForLabTrans = obj.ProcessTree(operator=None, parent=bloodFlowTrans, children=None, label=sentForLab)
        bloodFlowTrans.children.append(sentForLabTrans)
        metaData[durationInfo][sentForLab] = EmergencyDataDistribution.event_duration(sentForLab)

        labResultTrans = obj.ProcessTree(operator=None, parent=bloodFlowTrans, children=None, label=getLabResult)
        bloodFlowTrans.children.append(labResultTrans)

        radioFlowTrans = obj.ProcessTree(operator=obj.Operator.SEQUENCE, parent=multipleChecks, children=None, label=None)
        multipleChecks.children.append(radioFlowTrans)

        trans9 = obj.ProcessTree(operator=None, parent=radioFlowTrans, children=None, label=radio)
        metaData[durationInfo][radio] = EmergencyDataDistribution.event_duration(radio)
        radioFlowTrans.children.append(trans9)

        readioResultTrans = obj.ProcessTree(operator=None, parent=radioFlowTrans, children=None, label=getRadio)
        radioFlowTrans.children.append(readioResultTrans)

        metaData[durationInfo][getLabResult] = EmergencyDataDistribution.event_duration(getLabResult)
        metaData[durationInfo][getRadio] = EmergencyDataDistribution.event_duration(getRadio)



    if x == 'lab':
        trans8 = obj.ProcessTree(operator=None, parent=seq, children=None, label=getBloodForLab)

        metaData[durationInfo][getBloodForLab] = EmergencyDataDistribution.event_duration(ambPickup)
        seq.children.append(trans8)


        sentForLabTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=sentForLab)
        seq.children.append(sentForLabTrans)
        metaData[durationInfo][sentForLab] = EmergencyDataDistribution.event_duration(sentForLab)


        labResultTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=getLabResult)
        metaData[durationInfo][getLabResult] = EmergencyDataDistribution.event_duration(getLabResult)
        seq.children.append(labResultTrans)

    if x == 'radio':
        trans9 = obj.ProcessTree(operator=None, parent=seq, children=None, label=radio)
        metaData[durationInfo][radio] = EmergencyDataDistribution.event_duration(radio)
        seq.children.append(trans9)

        readioResultTrans = obj.ProcessTree(operator=None, parent=seq, children=None, label=getRadio)
        metaData[durationInfo][getRadio] = EmergencyDataDistribution.event_duration(getRadio)
        seq.children.append(readioResultTrans)

    trans10 = obj.ProcessTree(operator=None, parent=seq, children=None, label=physicalEval)
    metaData[durationInfo][physicalEval] = EmergencyDataDistribution.event_duration(physicalEval)
    seq.children.append(trans10)

    y = random.choices(['transfer', 'discharge'], [0.1, 0.9], k=1)[0]
    if y == 'transfer':
        value = random.choices([transferCardiology, transferOrthopedics], [0.5, 0.5], k=1)[0]

        trans11 = obj.ProcessTree(operator=None, parent=seq, children=None, label=value)
        metaData[durationInfo][value] = EmergencyDataDistribution.event_duration(value)
        seq.children.append(trans11)
        if value == transferCardiology:
            getBedAtCardio = obj.ProcessTree(operator=None, parent=seq, children=None, label=bedAtCardiology)
            metaData[durationInfo][bedAtCardiology] = EmergencyDataDistribution.event_duration(bedAtCardiology)
            seq.children.append(getBedAtCardio)
        if value == transferOrthopedics:
            getBedAtOrthopedics= obj.ProcessTree(operator=None, parent=seq, children=None, label=bedAtOrthopedics)
            metaData[durationInfo][bedAtOrthopedics] = EmergencyDataDistribution.event_duration(bedAtOrthopedics)
            seq.children.append(getBedAtOrthopedics)
    if y == 'discharge':

        trans12 = obj.ProcessTree(operator=None, parent=seq, children=None, label=discharge)
        metaData[durationInfo][discharge] = EmergencyDataDistribution.event_duration(discharge)
        seq.children.append(trans12)
    #print(trans11)
    #transferOrDischarge.children.append(trans11)
    #transferOrDischarge.children.append(trans12)
    #seq.children.append(transferOrDischarge)

    trans13 = obj.ProcessTree(operator=None, parent=seq, children=None, label=finishTag)
    metaData[durationInfo][finishTag] = 0
    seq.children.append(trans13)

    tree = seq
    # tree_consistency.fix_parent_pointers(tree)
    # tree = generic.fold(tree)
    # generic.tree_sort(tree)
    gviz = pt_visualizer.apply(tree)
    #pt_visualizer.view(gviz)

    net, initial_marking, final_marking = pt_converter.apply(tree)
    parameters = {pn_visualizer.Variants.FREQUENCY.value.Parameters.FORMAT: "png"}
    gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    #pt_visualizer.view(gviz)

    p_1 = None
    for p in net.places:
        place: PetriNet.Place = p
        #print(p.name)
        if place.name == 'source':
            p_1 = place
            break

    for t in net.transitions:
        trans: PetriNet.Transition = t
        t.name = t.label
        if trans.label == trans0.label:
            t_1 = trans
    metaData[startEventTag] = [t_1]

    initial_marking = Marking()
    final_marking = Marking()
    tokenMarking = MyMarking()
    # create a token at a place
    token1 = MyToken(place=p_1, name='p_1')
    # token2 = MyToken(place=p_2, name='p_2')
    tokenMarking[token1] = token1
    # tokenMarking[token2] = token2
    initial_marking[p_1] = 1
    metaData['graph'] = gviz

    return net, initial_marking, tokenMarking, metaData







import threading

def getEventId(start = 1):
    id = start
    while True:
        yield int(id)
        id += 1


def try_execute(t, pn, m: Marking, tokenMarking:MyMarking):
    if not semantics.is_enabled(t, pn, m):
        return None
    #debug_output('token marking before trnaision')
    #debug_output(f'tokenMarking')
    m_out = copy(m)
    tokenMarkingOut = copy_token_marking(tokenMarking)
    #print(tokenMarkingOut)

    tokenToDelete = []
    for a in t.in_arcs:
        # remove a token from the place in the in arc
        # a is an arc, arc source must be a place
        # can I find A TOKEN in that place?

        for token in tokenMarkingOut.keys():
            if token.place == a.source:
                #debug_output(f'find a token {token} for place {a.source}')
                # remove this token
                tokenToDelete.append(token)

        m_out[a.source] -= a.weight
        if m_out[a.source] == 0:
            # delete this key/value from dict
            del m_out[a.source]

    for x in tokenToDelete:
        del tokenMarkingOut[x]
    #debug_output('token marking after trnaision')

    for a in t.out_arcs:
        m_out[a.target] += a.weight
        # now generate a new token
        newToken = MyToken(place = a.target, name = a.target.name, parent= tokenToDelete)
        tokenMarkingOut[newToken] = newToken
    debug_output(f'tokenMarkingOut')
    return m_out, tokenMarkingOut


def execute(t, pn, m: Marking, tokenMarking:MyMarking, eventIdGenerator = None):
    if not semantics.is_enabled(t, pn, m):
        return None
    debug_output('token marking before trnaision')
    debug_output(f'tokenMarking')
    m_out = copy(m)
    tokenMarkingOut = copy_token_marking(tokenMarking)
    debug_output(f'tokenMarkingOut')
    myTrans = MyTransition(trans=t, eventIdGenerator = eventIdGenerator)

    tokenToDelete = []
    parentTrans = []
    for a in t.in_arcs:
        # remove a token from the place in the in arc
        # a is an arc, arc source must be a place
        # can I find A TOKEN in that place?

        for token in tokenMarkingOut.keys():
            if token.place == a.source:
                debug_output(f'find a token {token} for place {a.source}')
                parentTrans.append(token.parentTrans)

                # this token knows which transition genrated this token
                # remove this token
                tokenToDelete.append(token)

        m_out[a.source] -= a.weight
        if m_out[a.source] == 0:
            # delete this key/value from dict
            del m_out[a.source]
    myTrans.set_parent(parentTrans)
    for x in tokenToDelete:
        del tokenMarkingOut[x]
    debug_output('token marking after trnaision')

    for a in t.out_arcs:
        m_out[a.target] += a.weight
        # now generate a new token
        newToken = MyToken(place = a.target, name = a.target.name, parent= tokenToDelete)
        # know which tranision genrate this token
        # also know which tokens generate this token
        newToken.parent_trans(myTrans)
        tokenMarkingOut[newToken] = newToken
    debug_output(f'{tokenMarkingOut}')
    return m_out, tokenMarkingOut, myTrans

def duration_function(counter):
    duration = 10 * 60
    if counter % 2 == 1:
        duration = 15 * 60
    return duration


def copy_token_marking(original):
    tokenMarkingOut = MyMarking()
    for key, value in original.items():

        tokenMarkingOut[key] = value
    return tokenMarkingOut

def patientFlow():
    p = random.random()
    if p < 0.5:
        return edProcess()
    else:
        return edProcess()

