import random

import pandas
from simpy.resources.container import *

from simPyExtentionSkillSet import *

seconds_per_hour = 60 * 60

stockStats = {}

results = []
day_in_seconds = seconds_per_hour * 24
patienceTime = 20000 * day_in_seconds


class MyContainer(simpy.Container):
    def __int__( self,
        env: Environment,
        capacity: ContainerAmount = float('inf'),
        init: ContainerAmount = 0,):
        super().__init__(env, capacity, init)

    def _do_put(self, event: ContainerPut) -> Optional[bool]:
        spaceLeft = self.capacity - self._level
        added = min(spaceLeft, event.amount)
        self._level += added
        assert (self._level <= self.capacity)
        event.succeed()
        return True

def format_time(n=15):
    # Given timestamp in string
    time_str = '05/17/2021 00:00:00'
    date_format_str = '%m/%d/%Y %H:%M:%S'
    # create datetime object from timestamp string
    given_time = datetime.strptime(time_str, date_format_str)
    # print('Given timestamp: ', given_time)
    # Add 15 minutes to datetime object
    final_time = given_time + timedelta(seconds=n)
    # print('Final Time (15 minutes after given time ): ', final_time)
    # Convert datetime object to string in specific format
    final_time_str = final_time.strftime('%m/%d/%Y %H:%M:%S')
    # print('Final Time as string object: ', final_time_str)
    return final_time_str

def debug_output( str, show = False, force = False):
    show =  False
    if show or force:
        print(str)
        
def import_csv(file_path, checkLog = True):
    debug_output(f'start loading {file_path}')

    eventlog = pandas.read_csv(file_path, sep=',', skipinitialspace=True)
    shops = eventlog.shop.unique().tolist()
    if checkLog:

        num_shops = len(eventlog.shop.unique().tolist())
        num_events = len(eventlog.shop.to_list())
        debug_output("Number of events: {}\nNumber of unique shops: {}".format(num_events, num_shops))

        debug_output(list(eventlog))
    return shops, eventlog

shops, log = import_csv('usage.csv')
shops, containerLog = import_csv('containers.csv')
debug_output(shops)

env = simpy.Environment()

containers = {}
stores = {}
resources = {}
simResource = {}
rawToProductUsage = {}

product1Store = 'applePie'
product2Store = 'pie-apple'
rawMaterial1 = 'apple'
rawMaterial2 = 'pie'
job_id = 0


def toyData():
    containers[product1Store, rawMaterial1] = simpy.Container(env, init=1e6, capacity=1e10)
    containers[product1Store, rawMaterial2] = simpy.Container(env, init=1e6, capacity=1e10)
    containers[product2Store, rawMaterial1] = simpy.Container(env, init=1e6, capacity=1e10)
    containers[product2Store, rawMaterial2] = simpy.Container(env, init=1e6, capacity=1e10)
    stores[product1Store] = simpy.Store(env, capacity=200)
    stores[product2Store] = simpy.Store(env, capacity=200)


#toyData()


def create_stores(shops):
    for shop in shops:
        stores[shop] = simpy.Store(env, capacity=2000)
        stockStats[shop] = []
        for i in range(560):
            stores[shop].put(shop)

def create_usage(row, rawToProductUsage):
    shop = row.shop
    raw = row.raw
    quantity = row.quantity
    if shop not in rawToProductUsage:
        rawToProductUsage[shop] = {}
    rawToProductUsage[shop][raw] = quantity

def create_container(row, containers):
    shop = row.shop
    raw = row.raw
    capacity = row.capacity
    containers[shop, raw] = MyContainer(env, init=40, capacity=capacity)

create_stores(shops)
log.apply(create_usage, rawToProductUsage=rawToProductUsage, axis=1)
containerLog.apply(create_container, containers=containers, axis=1)
debug_output(rawToProductUsage)
debug_output(containers)



def create_machines_for_shop(shop, nbWorkers):
    resources[shop] = WorkerCollection(shop)
    start = 0
    end = 1e9
    for i in range(nbWorkers):
        resources[shop].add(Worker(i, start, end))
    simResource[shop] = MyPriorityResource(env, resources[shop])

def create_sim():
    # create resources for each store
    for key in stores:
        create_machines_for_shop(key, 10)
    for shop in stores:
        for i in range(resources[shop].len()):
            env.process(produce_item(env, shop))
    for key in containers:
        env.process(container_refill(env, key, containers[key]))

    env.process(customer_arrivals(env))
    env.process(record_stock_level(env))
def produce_item(env: simpy.Environment, shop):
    global job_id
    res = simResource[shop]
    job_id += 1
    while True:
        with res.request(event=NewJobEvent(worker_id=None, job_id=job_id)) as req:
            yield  req
            # now request raw materials
            materials = rawToProductUsage[shop]
            getMaterials = []
            for material, value in materials.items():
                #debug_output(f'shop {shop} 1 item needs {key} use {value} at {format_time(env.now)}')
                #debug_output(f'container {key} has {containers[key].level}')
                getMaterials.append(containers[shop, material].get(value))
            yield env.all_of(getMaterials)
           # debug_output(f'I can start making item for shop {shop} at {format_time(env.now)}')

            yield env.timeout(600)
            # now put item into the shop
            yield stores[shop].put(shop) # shop is the product name
            debug_output(f'put product {shop} to shop {shop} at {format_time(env.now)}', force=False)


def customer_arrivals(env):
    """Create new *moviegoers* until the sim time reaches 120."""
    id = 0
    while id < 3000:
        yield env.timeout(random.randint(3, 600))
        #yield env.timeout(0.5*seconds_per_hour)
        shoppingList = {}
        for shop in shops:

            shoppingList[shop] = random.randint(1, 3)
        env.process(customer(env, id, shoppingList))
        id+=1


def container_refill(env:simpy.Environment ,key, container: simpy.Container):
    refillStep = 0.25 *day_in_seconds
    while env.now < 300*day_in_seconds:
        yield env.timeout(refillStep)
        debug_output(f'refill container {key} at {format_time(env.now)}', force=False)
        yield container.put(500)

def customer(env: simpy.Environment, id, shoppingList):
    # create customer order for every shop
    #debug_output(f'customer {id} arrive at {format_time(env.now)}', force=True)
    arrival = format_time(env.now)
    for store in stores:
        debug_output(f'current stock level at {store} is {len(stores[store].items)}', force=False)
    orderEvents = []
    for key, value in shoppingList.items():
        for i in range(value):
            orderEvents.append(stores[key].get())
    notPaitent = env.timeout(patienceTime)
    leaveEarly = False
    r = yield env.all_of(orderEvents) | notPaitent
    if notPaitent in r:
        debug_output(f'customer {id} leave early at {format_time(env.now)}', force=False)
        leaveEarly = True
    else:
        debug_output(f'customer {id} get order at {format_time(env.now)}', force=False)
    data = [id, arrival, format_time(env.now), leaveEarly]
    for key in shoppingList:
        data.append(shoppingList[key])
    results.append(data)

def record_stock_level(env: simpy.Environment):
    while env.now < 1 * day_in_seconds:
        for key, store in stores.items():
            stockStats[key].append(len(store.items))
            debug_output(f' at time {format_time(env.now)}, {key} has  {len(store.items)}', force=True)
        yield  env.timeout(1 * seconds_per_hour)

create_sim()
print('startsim')
env.run()
print('finish')
#produce_item(env, product1Store)

headers = ['customer', 'arrival', 'leave', 'quitEarly']
for shop in shops:
    headers.append(shop)
import  createcsv
createcsv.create_csv('result.csv', header=headers, data=results)

y = stockStats[shops[0]]
import matplotlib.pyplot as plt

x = [i for i in range(len(y))]


plt.plot(x, y)
plt.title('stock')
plt.xlabel('time')
plt.ylabel('stock')
plt.show()
