import ast
import datetime

import pandas as pd

from simbase import *
import numpy as np
import matplotlib.pyplot as plt


class EnviroTrack(EnviroTrackBase):

    def __init__(self):
        super().__init__()

    # define transport policy : start yard? hours, capacity, when to request transport empty mould? when to request sending full mould
    def get_data(self):

        rosterData = {'roster_dict': staffRosterDict, 'roster_df': rosterDF(staffRosterDict),
                      'roster_week': SimSetting.rosterWeekDayList,
                      'grouped_roster': group_roster_by_date(staffRosterDict)}

        self.set_roster(rosterData)
        self.create_resource_from_roster(rosterData)

        mould_info = import_csv('mould_info.csv')

        locations = mould_info.location.unique().tolist()
        for l in locations:
            Locations.add_location(l)

        uMean = mould_info.groupby(['location', 'sku'])['id'].count()
        uMean = uMean.reset_index()
        print(uMean)

        uMean = mould_info.groupby(['location'])['id'].count()
        uMean = uMean.reset_index()
        print(uMean)

        print(mould_info.info())

        self.define_mould(mould_info)

        self.define_order()

        print(Locations.location_dict)

        # for each mould, define it as a resource
        for location, moulds in self.mould_collection.items():
            for key, mould in self.mould_collection[location].items():
                current_stage_workers = WorkerCollection(key)
                counter = 0
                current_stage_workers.add(Worker(id=key, start=0, end=0, name=key))
                counter += 1
                self.mould_res[key] = MyPriorityResource(self.env, current_stage_workers, mould.sku)
        # print(self.mould_res)

        self.define_collect()

        self.load_look_ahead()
        # self.schedule_transport()

        self.env.process(self.daily_transport_request(end_yard=avondale))

        self.free_mould_monitor = {}
        self.full_mould_monitor = {}
        for i in all_yards:
            self.free_mould_monitor[i] = []
            self.full_mould_monitor[i] = []

        self.env.process(self.monitor())

    def monitor(self):
        while True:
            for yard in all_yards:
                end_data = self.get_end_yard_data(yard)
                nbFree = len(end_data[freeAtEndStr])
                self.free_mould_monitor[yard].append(nbFree)
                nbFull = len(end_data[fullMouldStr])
                self.full_mould_monitor[yard].append(nbFull)
            yield self.env.timeout(day_in_seconds)




    def load_look_ahead(self):
        self.look_ahead = {}
        # key by yard, date
        look_ahead = import_csv('look_ahead.csv')
        for a in look_ahead.itertuples():
            yard = a.yard
            date = a.date_str
            date_format = "%Y-%m-%d"

            datetime_object = datetime.strptime(date, date_format)
            look_ahead = a.look_ahead
            self.look_ahead[yard, datetime_object] = look_ahead
        print(self.look_ahead)
        a = 3

    def define_order(self):

        invoice_info = import_csv('invoice.csv')
        # group invoice by id
        for a in invoice_info.itertuples():

            # each invoice for the id make an orderInfo
            old_items = a.ItemCode
            new_items = old_items.replace("null", "'bad'")
            #print(new_items)

            old_quantity = a.Quantity
            new_quantity = old_quantity.replace("null", "0")
            #print(new_quantity)

            items = ast.literal_eval(new_items)
            quantity = ast.literal_eval(new_quantity)
            #print(items)
            #print(quantity)
            date_format = "%Y-%m-%d"

            datetime_object = datetime.strptime(a.date, date_format) + timedelta(hours=10)
            for idx, item in enumerate(items):
                if item in self.product_set_by_mould:
                    if a.id not in self.order_by_id:
                        self.order_by_id[a.id]: Order = Order(id=a.id, date=datetime_object)
                    assert (quantity[idx] > 0)

                    #print(f'get to order {quantity[idx]} {item} at {a.date} ')

        for id, order in self.order_by_id.items():
            order: Order = order

            self.schedule_order(order)

    def schedule_order(self, order: Order):

        self.env.process(self.order_process(order))

    def order_process(self, order: Order):
        # pdd = order.pdd
        wait = (order.date - SimSetting.start_time_sim).total_seconds()
        yield self.env.timeout(wait)
        print(f'order {order.id} time is {format_time(self.env.now)}')
        # print(self.warehouse.items)
        for orderItem in order.orderList:

            orderItem: OrderInfo = orderItem
            quantity = orderItem.quantity
            sku = orderItem.sku
            items_get = []
            for q in range(quantity):
                items_get.append(self.warehouse.get(lambda item: item.sku == sku))
            res = yield self.env.all_of(items_get)
            items = list(res.values())
            print(f'all items are now ready to deliver to customer {order.id} {items} at {format_time(self.env.now)}')
            # self.show_mould_by_ranking(avondale)

    def define_mould(self, mould_info):
        print(mould_info.groupby(['location', 'sku']))
        # these mould can be moved around
        for a in mould_info.itertuples():
            m = Mould(serial_no=a.serial_no, sku=a.sku, current_location=a.location, capacity=a.volume)
            self.product_set_by_mould.add(a.sku)
            if a.location not in self.mould_collection:
                self.mould_collection[a.location] = {}
            self.mould_collection[a.location][m.serial_no] = m
        print(self.mould_collection)
        self.set_mould_ranking()

    def define_collect(self):
        invoice_info = import_csv('collect.csv')

        self.collection = {}
        nbDays = 30
        for i in range(nbDays):
            collect_time = SimSetting.start_time_sim + timedelta(days=i,hours= 12)
            for yard in all_yards:
                v = EnviroDataDistribution.draw_sample_collection(l=location_map[yard])
                collect = Collection(volume=v,
                                     time_stamp=collect_time,
                                     location=yard)
                self.collection[len(self.collection)] = collect


        self.schedule_collect()

    # every collect, monitor the nb of full moulds at each site except penrose, if its number is more then 3, request one or two transporataion per day
    def daily_transport_request(self, end_yard=avondale):
        # at 6pm, try to do one or two round trip from penrose to a site
        start_time_wait = (SimSetting.roster_start_time - SimSetting.start_time_sim).total_seconds()
        yield self.env.timeout(start_time_wait + 19 * seconds_per_hour)
        start_yard = penrose

        # figure out nb of demould
        # lets assume one trip first


        while True:
            debug_output(f'daily transport at {format_time(self.env.now)}')
            left_over = {}
            stable_free_moulds = {}
            end_data = {}
            for yard in all_yards:
                data = self.get_end_yard_data(yard)
                end_data[yard] = data
                left_over[yard] = data[lookaheadStr]
                if yard == penrose:

                    free_moulds_at_penrose = data[freeAtEndStr]


                stable_free_moulds[yard] = data[freeAtEndStr]
            #look_ahead_penrose = penrose_data[lookaheadStr]
            # one trip can take 12 free moulds.
            # first work out how much of stable moulds can consume the lookahead
            nb_free_moulds = len(free_moulds_at_penrose)
            if float(nb_free_moulds)/float(Transport.mould_capacity) > 1:
                # two trips can be done
                # stable ones at penrose are all minus two trucks of full..
                # assume to first 24 moulds are free to move
                x = [1,2,3,4,5]
                print(x[2:])
                if nb_free_moulds < 24:
                    print('f')
                stable_free_moulds[penrose] = free_moulds_at_penrose[2 * Transport.mould_capacity:]
                for yard in all_yards:
                    for m in stable_free_moulds[yard]:
                        if left_over[yard] > m.capacity:
                            left_over[yard] -= m.capacity



                moulds_to_move = free_moulds_at_penrose[:Transport.mould_capacity]
                target_yard, left_over = self.single_trip(moulds_to_move, left_over)
                yield self.env.process(self.start_trip(end_data, moulds_to_move, target_yard))
                print(f'current time {getDateTime(self.env.now)}')
                end_data = {}
                for yard in all_yards:
                    data = self.get_end_yard_data(yard)
                    end_data[yard] = data


                #pernose is confusing
                moulds_to_move = free_moulds_at_penrose[Transport.mould_capacity:2*Transport.mould_capacity]
                print(f'new left over after first trip is {left_over}')
                target_yard, left_over = self.single_trip(moulds_to_move, left_over)
                yield self.env.process(self.start_trip(end_data, moulds_to_move, target_yard))
                print(f'current time {getDateTime(self.env.now)}')

            else:
                # only enough do one round trip
                stable_free_moulds[yard] = free_moulds_at_penrose[Transport.mould_capacity:]
                moulds_to_move = free_moulds_at_penrose[:Transport.mould_capacity]
                target_yard, left_over  =self.single_trip(moulds_to_move, left_over)
                yield self.env.process(self.start_trip(end_data, moulds_to_move, target_yard))
                print(f'current time {getDateTime(self.env.now)}')



            # next_day at 19
            current_time =  SimSetting.start_time_sim + timedelta(seconds=self.env.now)
            next_day_current_time = SimSetting.start_time_sim + timedelta(days=1, seconds=self.env.now)
            next_day = datetime(year=next_day_current_time.year, month=next_day_current_time.month,
                                day=next_day_current_time.day, hour=19)
            yield self.env.timeout((next_day -current_time).total_seconds() )
            print(f'current time {getDateTime(self.env.now)}')
            a = 3

    def start_trip(self, end_data, moulds_to_move, target_yard):
        if len(moulds_to_move) > 0:
            if target_yard != penrose:
                # travel to target and back with fullMoulds
                taken_moulds = end_data[target_yard][fullMouldStr]
                taken_moulds = taken_moulds[:Transport.mould_capacity]
                for m in taken_moulds:

                    assert(m.mould_state == MouldState.full)
                    m.set_location_status(Mould.in_transit)
                for m in moulds_to_move:
                    m.set_location_status(Mould.in_transit)
                yield self.env.process(
                    self.transport_round_trip(start_yard=penrose, end_yard=target_yard,
                                              first_taken_moulds=moulds_to_move,
                                              second_taken_moulds=taken_moulds))

            else:
                # target is penrose itself, just go pick up full moulds
                full_mould_data = {}
                for yard, data in end_data.items():
                    if yard == penrose: continue
                    fullMoulds = data[fullMouldStr]

                    full_mould_data[yard] = fullMoulds

                ranked_full_moulds = list(full_mould_data.items())
                ranked_full_moulds.sort(key=lambda a: len(a[1]), reverse=True)
                # conduct the first round trip
                first_target_yard = ranked_full_moulds[0][0]
                taken_moulds = ranked_full_moulds[0][1][:Transport.mould_capacity]
                for m in taken_moulds:
                    m.set_location_status(Mould.in_transit)
                if len(taken_moulds) > 0:
                    yield self.env.process(self.transport_round_trip(start_yard=penrose, end_yard=first_target_yard,
                                                               first_taken_moulds=[],
                                                               second_taken_moulds=taken_moulds))


        else:
            # no free to transport from penrose
            # decide which site to visit
            full_mould_data = {}
            for yard, data in end_data.items():
                if yard == penrose:continue
                fullMoulds = data[fullMouldStr]

                full_mould_data[yard] = fullMoulds

            ranked_full_moulds = list(full_mould_data.items())
            ranked_full_moulds.sort(key=lambda a: len(a[1]), reverse=True)
            a = 3
            # conduct the first round trip
            first_target_yard = ranked_full_moulds[0][0]
            taken_moulds = ranked_full_moulds[0][1][:Transport.mould_capacity]
            if len(taken_moulds) > 0:
                yield self.env.process(
                    self.transport_round_trip(start_yard=penrose, end_yard=first_target_yard,
                                              first_taken_moulds=[],
                                              second_taken_moulds=taken_moulds))
        return

    def single_trip(self, moulds_to_move, left_over):

        # now decide x,y,z
        # with left over at each site, and one trip with free moulds for transport, lets do some programming
        # rank the left over, fullfill them in order
        end_data = {}
        left_over_copy = left_over.copy()
        for end_yard in all_yards:

            for m in moulds_to_move:
                m: Mould = m
                if left_over_copy[end_yard] > m.capacity:
                    left_over_copy[end_yard] -= m.capacity

        rank_lost_volume = list(left_over_copy.items())
        rank_lost_volume.sort(key=lambda a: a[1], reverse=True)
        target_yard = rank_lost_volume[0][0]
        target_lost_volume = rank_lost_volume[0][1]
        target_lost_money = target_lost_volume // EnviroDataDistribution.most_common_capacity * EnviroDataDistribution.block_cost
        # its ok to run empty from penrose
        print(f'ranked lost is {rank_lost_volume} ')
        self.log.append(f'ranked lost is {rank_lost_volume} at {format_time(self.env.now)} ')

        #update left over based on this decisoin
        target_yard = rank_lost_volume[0][0]
        for m in moulds_to_move:
            m: Mould = m
            if left_over[target_yard] > m.capacity:
                left_over[target_yard] -= m.capacity
        return rank_lost_volume[0][0], left_over

    def get_end_yard_data(self, end_yard):

        nbFullAtEnd, freeMoulds, fullMoulds = self.count_mould(end_yard)
        # at 10am, check the full moulds, and if no request has been made by collect, request

        # take empty mould to end yard, take back full mould
        # two transport request
        # try lookahead at this date for each end yard
        next_day_current_time = SimSetting.start_time_sim + timedelta(days=1, seconds=self.env.now)
        next_day = datetime(year=next_day_current_time.year, month=next_day_current_time.month,
                            day=next_day_current_time.day)
        # if the closest look ahead is within 3 days, use that value, otherwise assume no collect at that site
        keys = list(self.look_ahead.keys())
        # filter the list to keep only tuples where the second element is 'apple'
        filtered_lst = [tup[1] for tup in keys if tup[0] == end_yard]
        print(filtered_lst)
        time_diff = [f - next_day for f in filtered_lst]
        time_diff.sort()
        look_ahead_value = 0
        # generate collect forcast by density function for 3 days
        nbDays = 3

        for i in range(nbDays):
            collect = EnviroDataDistribution.draw_sample_collection(l=location_map[end_yard])
            #collect = 2.4
            look_ahead_value += collect

        print(f'at {end_yard} next day {next_day}, expect {look_ahead_value} to collect in 3 days')
        data = {nbFullAtEndStr: nbFullAtEnd, freeAtEndStr: freeMoulds, lookaheadStr: look_ahead_value, fullMouldStr: fullMoulds}
        return data

    def transport_round_trip(self, start_yard, end_yard, first_taken_moulds, second_taken_moulds):
        yield self.env.process(self.transport_leg(start_yard=start_yard, end_yard=end_yard, taken_moulds=first_taken_moulds))

        yield self.env.process(self.transport_leg(start_yard=end_yard, end_yard=start_yard, taken_moulds=second_taken_moulds))

    def transport_leg(self, start_yard, end_yard, taken_moulds):
        print(f'transport request from {start_yard} to {end_yard} is triggered at {format_time(self.env.now)} with {len(taken_moulds)} moulds')

        currentHour = (SimSetting.start_time_sim + timedelta(seconds=self.env.now)).hour
        # wait until 12am of the day, cancel the reqeust
        wait = (24 - currentHour) * seconds_per_hour
        res = self.priority_res[trusckRes]
        quitEvent = self.env.timeout(wait)
        id = f'{start_yard}_{end_yard}_{next(self.transport_id_gen)}'
        #newRequest = res.request(event=NewJobEvent(worker_id=None, job_id=id),  priority=0)
        print(f'request driver at {format_time(self.env.now)}')
        #yield newRequest
        print(f'got  driver at {format_time(self.env.now)}')
        # job is done
        self.log.append(f'transport request from {start_yard} to {end_yard} is triggered at '
                        f'{format_time(self.env.now)} with {len(taken_moulds)} moulds')



        if len(taken_moulds) > 0:
            print(f'all taken moulds are {taken_moulds}')
            for m in taken_moulds:
                print(m)
                if end_yard == penrose:
                    assert (m.mould_state == MouldState.full)
                # remove from current location for transportatoin
                if m.serial_no == 'P28':
                    print('f')
                if (start_yard != end_yard):
                    del self.mould_collection[start_yard][m.serial_no]
            # these moulds are not at this location,need to re-rank
            self.set_mould_ranking(start_yard)
            print(f'transport ids at {start_yard} to {end_yard} are {taken_moulds}')

            yield self.env.timeout(20 * 60)
            for m in taken_moulds:
                m.set_location(end_yard)
                m.set_location_status(Mould.stable)
                if m.mould_state == MouldState.full:
                    print(f'end yard = {end_yard}')
                    assert (end_yard == penrose)
                self.mould_collection[end_yard][m.serial_no] = m
            self.set_mould_ranking(end_yard)
            # print(f'mould at end_yard {self.mould_collection[end_yard]}')

            debug_output(f'arrive at {end_yard} with {taken_moulds} moulds at {format_time(self.env.now)}')
            if end_yard == penrose:
                for m in taken_moulds:
                    m.at_penrose.succeed()
        else:
            yield self.env.timeout(20 * 60)
        #res.release(newRequest)

    def collect_process(self, secsToCollect, collect: Collection):
        yield self.env.timeout(secsToCollect)
        print(f'start collect at {format_time(self.env.now)} for {collect} ')
        # print the mould list at this location

        start_yard = collect.location
        # print(f'moulds are {self.mould_collection[location]}')
        # print(f'ranked moulds are {self.current_mould_ranking_by_location[location]}')
        ids = self.current_mould_ranking_by_location[start_yard]
        # fill as much as possible
        nbFilled = 0
        for id in ids:
            mould: Mould = self.mould_collection[start_yard][id]
            if  mould.mould_state == MouldState.free:
                if collect.volume > mould.capacity:
                    nbFilled += 1
                    mould.set_full(SimSetting.start_time_sim + timedelta(seconds=self.env.now))
                    collect.volume -= mould.capacity
                    print(f'fill {mould} and updated {collect}')
                    # full mould create arrive to penrose event, it must be there for demoud
                    mould.full_arrive_penrose_event(self.env)

                    self.env.process(self.mould_dry(mould))
                    if start_yard == penrose:
                        mould.at_penrose.succeed()
        debug_output(f'total filled is {nbFilled}')

        # if self.env.now - self.transport_request_last_trigger[(start_yard, penrose)]  > day_in_seconds:

    def schedule_transport(self):
        # schedule transport from one yard to another
        # self.env.process(self.yard_transport(start_yard=avondale, end_yard=penrose))
        # self.env.process(self.yard_transport(start_yard=silverdale, end_yard=penrose))
        self.env.process(self.yard_transport(start_yard=penrose, end_yard=avondale))
        # self.env.process(self.yard_transport(start_yard=penrose, end_yard=silverdale))

    def yard_transport(self, start_yard, end_yard):

        yield self.transport_request[(start_yard, end_yard)]
        print(f'transport request from {start_yard} to {end_yard} is triggered at {format_time(self.env.now)}')
        currentHour = (SimSetting.start_time_sim + timedelta(seconds=self.env.now)).hour
        # wait until 12am of the day, cancel the reqeust
        wait = (24 - currentHour) * seconds_per_hour
        res = self.priority_res[trusckRes]
        quitEvent = self.env.timeout(wait)
        id = f'{start_yard}_{end_yard}_{next(self.transport_id_gen)}'
        newRequest = res.request(event=NewJobEvent(worker_id=None, job_id=id),
                                 priority=0)
        waitEvents = [quitEvent, newRequest]
        result = yield self.env.any_of(waitEvents)
        if quitEvent in result:
            # too much waiting, quit
            debug_output(f'quit wait at {format_time(self.env.now)} for {start_yard}')

            newRequest.cancel()
            # request night batch
        else:
            # job is done

            w = result[newRequest]

            if start_yard == penrose:
                nbDemould, demoulds = self.count_demould()
                if nbDemould > 0:
                    transport = Transport()
                    if nbDemould > transport.mould_capacity:
                        taken_moulds = demoulds[:transport.mould_capacity]
                    else:
                        taken_moulds = demoulds
                    for m in taken_moulds:
                        m.set_location_status(Mould.in_transit)
                        del self.mould_collection[start_yard][m.serial_no]
                    self.set_mould_ranking(start_yard)
                    print(f'transport ids at {start_yard} are {[m.serial_no for m in taken_moulds]}')
                    # transort time
                    yield self.env.timeout(20 * 60)
                    for m in taken_moulds:
                        m.set_location_status(Mould.stable)
                        m.set_location(end_yard)
                        self.mould_collection[start_yard][m.serial_no] = m
                    self.set_mould_ranking(end_yard)

            else:
                full_ids = self.get_full_mould(start_yard)
                nb_full = len(full_ids)
                print(f'full ids at {start_yard} are {full_ids}')
                # transport full mould to penrose
                assert (end_yard == penrose)
                transport = Transport()
                taken_moulds = []
                if nb_full > transport.mould_capacity:
                    taken_ids = full_ids[:transport.mould_capacity]
                else:
                    taken_ids = full_ids
                if len(taken_ids) > 0:
                    for id in taken_ids:
                        m: Mould = self.mould_collection[start_yard][id]
                        m.set_location_status(Mould.in_transit)
                        taken_moulds.append(m)
                        # remove from current location for transportatoin
                        del self.mould_collection[start_yard][id]
                    # these moulds are not at this location,need to re-rank
                    self.set_mould_ranking(start_yard)
                    print(f'transport ids at {start_yard} are {taken_ids}')

                    yield self.env.timeout(20 * 60)
                    for m in taken_moulds:
                        m.set_location(end_yard)
                        if m.is_full:
                            assert (end_yard == penrose)
                        self.mould_collection[end_yard][m.serial_no] = m
                    self.set_mould_ranking(end_yard)
                    # print(f'mould at end_yard {self.mould_collection[end_yard]}')

                    debug_output(f'success truck at {format_time(self.env.now)} for {start_yard}')

                    debug_output(f'arrive at {end_yard} at {format_time(self.env.now)}')
                    if end_yard == penrose:
                        for m in taken_moulds:
                            m.at_penrose.succeed()
                    res.release(newRequest)

    def transport(self, start_yard, end_yard, scheduled_start_elasped_time_in_sec, duration):
        yield self.env.timeout(scheduled_start_elasped_time_in_sec)
        print(f'transport at {format_time(self.env.now)} from {start_yard} to {end_yard}')

        full_ids = self.get_full_mould(start_yard)
        nb_full = len(full_ids)
        print(f'full ids at {start_yard} are {full_ids}')
        if start_yard != penrose:
            # transport full mould to penrose
            assert (end_yard == penrose)
            transport = Transport()
            taken_moulds = []
            if nb_full > transport.mould_capacity:
                taken_ids = full_ids[:transport.mould_capacity]
            else:
                taken_ids = full_ids
            if len(taken_ids) > 0:
                for id in taken_ids:
                    m: Mould = self.mould_collection[start_yard][id]
                    m.set_location_status(Mould.in_transit)
                    m.full_arrive_penrose_event(self.env)
                    taken_moulds.append(m)
                    # remove from current location for transportatoin
                    del self.mould_collection[start_yard][id]
                # these moulds are not at this location,need to re-rank
                self.set_mould_ranking(start_yard)
                print(f'transport ids at {start_yard} are {taken_ids}')
                yield self.env.timeout(duration)
                # full mould arrive at penrose
                print(f'arrive at {end_yard} at {format_time(self.env.now)}')

                for m in taken_moulds:
                    m.set_location(end_yard)
                    if m.is_full:
                        assert (end_yard == penrose)
                        m.at_penrose.succeed()
                    self.mould_collection[end_yard][m.serial_no] = m
                self.set_mould_ranking(end_yard)
                # print(f'mould at end_yard {self.mould_collection[end_yard]}')

    def schedule_collect(self):
        for id, collect in self.collection.items():
            collect: Collection = collect
            totalSeconds = (collect.time_stamp - SimSetting.start_time_sim).total_seconds()

            self.env.process(self.collect_process(secsToCollect=totalSeconds, collect=collect))

    def mould_dry(self, mould):
        # assume each mould takes 7 days to dry
        duration_to_dry = day_in_seconds * 7
        yield self.env.timeout(duration_to_dry)
        print(f'{mould} is dry at {format_time(self.env.now)}')
        mould.set_dry(SimSetting.start_time_sim + timedelta(seconds=self.env.now))
        yield self.env.timeout(seconds_per_hour)

        # cannto demould until it is at penrose
        yield mould.at_penrose
        print(f'demould {mould} at {format_time(self.env.now)}')
        assert (mould.current_location == penrose)
        mould.set_demould(SimSetting.start_time_sim + timedelta(seconds=self.env.now))
        bar = next(self.barcode_gen)
        # print(f'barcode = {bar}')
        storeItem = StoreItem(barcode=bar, sku=mould.sku)
        self.warehouse.put(storeItem)


# group mould by sku
enviroTrack = EnviroTrack()
enviroTrack.get_data()
print(SimSetting.start_time_sim)

enviroTrack.run()


def plot_kpi():
    #print(enviroTrack.mould_monitor)
    df = pd.DataFrame(enviroTrack.free_mould_monitor)
    df.to_csv('test.csv', index=False)
    df.plot(kind='line')
    plt.title('Free moulds at sites')

    df = pd.DataFrame(enviroTrack.full_mould_monitor)
    df.to_csv('test.csv', index=False)
    df.plot(kind='line')
    plt.title('Full moulds at sites')

    plt.show()


plot_kpi()

with open('out.txt', 'w') as f:
    for line in enviroTrack.log:
        f.write(f'{line}\n')

