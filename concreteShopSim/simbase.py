
from strenum import StrEnum

from EnviroData import *
from simPyExtentionSkillSet import *


def transport_id_gen():
    id = 1
    while True:
        yield id
        id +=1

def barcode_gen():
    id = 1
    while True:
        yield id
        id +=1


class LoadType(StrEnum):
    noLoad = 'noLoad'
    emptyMould = 'Empty Moulds'
    fullMould = 'Full Moulds'
    carboardLoad = '"Cardboard Tubes'
    rubberLoad = 'Rubbers'

class MouldState(StrEnum):
    free = 'Free'
    full = 'Full'
    dry = 'Dry'




print(LoadType.noLoad)





class OrderInfo:
    def __init__(self, sku='', quantity=1):
        self.sku = sku
        self.quantity = quantity


    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'orderInfo, {self.sku}, {self.quantity} '


class Order:
    def __init__(self, id=0, date=None, pdd=None):
        self.id = id
        self.date = date # order created
        self.pdd = pdd # preferred due date
        self.orderList = []

    def add_order_info(self, orderInfo: OrderInfo):
        self.orderList.append(orderInfo)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        output = ''
        for orderInfo in self.orderList:
            output += orderInfo.__str__()
            output += '\t'

        return f'order, {output} pdd is {self.pdd}'


class TransportLoad:
    def __int__(self, loadType: LoadType = None, quantity=3):
        self.loadType = loadType
        self.quantity = quantity

# how to model transport?

# step 1 transport some moulde from avondale to penrose
# on every nite by one truck


class Transport:
    mould_capacity = 12
    def __init__(self):
        self.load_array = []
        self.scheduled_time = 0
        self.start_yard = ''
        self.end_yard = ''
        self.transport_status = ''


    def add_load(self, tLoad: TransportLoad):
        self.load_array.append(tLoad)

    def set_transport_time(self, t):
        self.scheduled_time = t


class Collection:
    def __init__(self, volume=10, time_stamp=datetime(year=2022, month=9, day=2, hour=5), location=''):
        self.volume = volume
        self.time_stamp = time_stamp
        self.location = location

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'collect, {self.volume} from {self.location} at {self.time_stamp}'


class Locations:
    location_dict = {}

    @classmethod
    def add_location(cls, location):
        cls.location_dict[location] = location


class StoreItem:
    def __init__(self, barcode=None, sku=None):
        self.barcode=barcode
        self.sku=sku

    def __repr__(self):
        return f'bar {self.barcode} product, {self.sku}'

    def __str__(self):
        return f'bar {self.barcode} product, {self.sku}'

class Product:
    def __init__(self, product_family='Interbloc', product_type='Angle', product_size='600x120', sku=''):
        self.product_family = product_family
        self.product_type = product_type
        self.product_size = product_size
        self.sku = sku

    def __str__(self):
        return f'(product, {self.sku}, {self.product_family}, {self.product_type}, {self.product_size})'

    def __repr__(self):
        return f'(product, {self.product_family}, {self.product_type}, {self.product_size})'


class Mould:
    in_transit = 'in transit'
    stable = 'stable'
    def __init__(self, serial_no='Interbloc', sku='Angle', current_location='location', capacity=0.51):
        self.serial_no = serial_no
        self.sku = sku
        self.current_location = current_location
        self.mould_state = MouldState.free
        # start with empty mould for now
        self.capacity = capacity
        self.mould_last_fill_time = 0
        self.dry_time = 0
        self.location_status = self.stable
        self.last_demould_time = 0
        self.at_penrose = None


    def full_arrive_penrose_event(self, env: simpy.Environment):
        self.at_penrose = env.event()

    def set_full(self, t):

        self.mould_last_fill_time = t

        self.mould_state = MouldState.full


    def set_location_status(self, status):
        self.location_status = status

    def set_location(self, location):
        self.current_location = location
        self.location_status = self.stable

    def set_dry(self, t):

        self.dry_time = t
        self.mould_state = MouldState.dry

    def set_demould(self, t):

        self.last_demould_time = t

        self.mould_state = MouldState.free



    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'mould, {self.serial_no}, {self.sku}, cap= {self.capacity}, {self.mould_state} at {self.current_location}'



class EnviroTrackBase:

    def __init__(self):
        self.env = simpy.Environment()
        self.order_by_id = {}
        self.mould_collection = {}
        self.invoice = {}
        self.mould_res = {}
        self.current_mould_ranking_by_location = {}
        self.collection = {}
        self.barcode_gen = barcode_gen()
        self.transport_id_gen = transport_id_gen()
        self.warehouse = simpy.FilterStore(self.env, capacity=1000)
        self.product_set_by_mould = set()
        self.transport_request = {(avondale, penrose): self.env.event(),
                                  (silverdale, penrose): self.env.event(),
                                  (penrose, avondale): self.env.event(),
                                  (penrose, silverdale): self.env.event()}
        self.transport_request_last_trigger = {(avondale, penrose): [],
                                  (silverdale, penrose): [],
                                  (penrose, avondale):[],
                                  (penrose, silverdale): []}
        self.log = []



    def set_mould_ranking(self, yard = None):
        # default ranking is by id at this stage
        # rank by due date and look-ahead  stock (ready in the near future)

        if yard != None:
            ids = sorted(list(self.mould_collection[yard].keys()))
            #print(f'mould ranks at {yard} are {ids}')
            self.current_mould_ranking_by_location[yard] = ids
        else:

            for location in self.mould_collection.keys():
                ids = sorted(list(self.mould_collection[location].keys()))
                print(f'mould ranks at {location} are {ids}')
                self.current_mould_ranking_by_location[location] = ids

    def show_mould_by_ranking(self, yard=None):
        for idx, id in enumerate(self.current_mould_ranking_by_location[yard]):
            print(f' rank {idx} is {self.mould_collection[yard][id]}')

    def get_full_mould(self, location):
        # todo, this can be cached somehow,
        full_ids = []
        for id, m in self.mould_collection[location].items():
            m: Mould = m
            if m.mould_state == MouldState.full:
                full_ids.append(id)
        return full_ids

    def set_roster(self, rosterData):
        self.rosterData = rosterData
        self.runUntil = SimSetting.rosterTotalDays
        weekDayList = self.rosterData['roster_week']
        finalSimDay = SimSetting.start_time_sim + timedelta(days=self.runUntil)
        roster_start_day = SimSetting.roster_start_time
        # make sure my roster break def go over the last day of sim
        rosterDays = (finalSimDay - roster_start_day).days

        self.rosterCycleDays = len(weekDayList) * 7
        self.totalCycles = math.ceil((rosterDays / self.rosterCycleDays))

    def get_datetime_from_roster(self, shift):
        year = shift[0]
        month = shift[1]
        day = shift[2]
        start_hour = shift[3].hour
        start_min = shift[3].minute
        end_hour = shift[4].hour
        endt_min = shift[4].minute
        return day, end_hour, endt_min, month, start_hour, start_min, year

    def staff_break_process(self, resource=None, worker_id: int = 0, stage=1, start=0, duration=3):

        expcted_break = self.env.now + start
        # start is the time duration from simulation time 0
        yield self.env.timeout(start)
        #debug_output(f'start asking break for {stage} at {format_time(start)}')

        # at the time to reqeust break, we make the request, this request is put into queue at this moment with high priority
        # this high priorit will move break request to the top of the queue and order by request time
        # when any resoruce is freed, it will call back the queue and start from the first event on the queue
        # becoase all break event have high piroirty, ther will be no job events between them
        # by going through all break events  we make sure the specific worker will take the rest for sure when it is free
        with resource.request(priority=-4, event=BreakEvent(worker_id)) as req:
            yield req
            # I want to know which worker start break

            #debug_output(f'stage {stage} worker {worker_id} start break at {format_time(self.env.now)}, expected at {format_time(expcted_break)} finish at {format_time(self.env.now + duration)}', show=True)
            # self.batchDebug.append([worker_id, format_time(expcted_break),format_time(self.env.now+duration) ])
            # if I start a break AND I have job request want to interrupt that request
            # I need to know job_id
            # worker may work a bit of of time, so he may not get the request at the exact time
            # make sure he start the correct time next day

            actualDuration = duration - (self.env.now - expcted_break)


            yield self.env.timeout(actualDuration)


    def create_resource_from_roster(self, rosterData):
        self.priority_res = {}
        staffRosterDict, df, weekDayList = rosterData['roster_dict'], rosterData['roster_df'], rosterData['roster_week']

        nextRosterCycleStart = SimSetting.start_time_sim + timedelta(days=self.rosterCycleDays)
        print(f'next roster cycle start {nextRosterCycleStart}')
        # current_stage_workers = WorkerCollection(tag)
        # for i in range(capacity):
        #    current_stage_workers.add(Worker(i, shift_start, shift_end, members[i]))
        # teamResoruces[day][timeIndex][tag] = MyPriorityResource(self.env, current_stage_workers, tag)

        teams = {}  # key by stage, staff on the same stage belong to the same time

        for staffName, staffInfo in staffRosterDict.items():
            staffInfo: StaffShift = staffInfo
            resource_group = staffInfo.stage
            if resource_group not in teams:
                teams[resource_group] = {}
            teams[resource_group][staffName] = staffInfo
        for resource_group in teams.keys():
            debug_output(f'nb staff, {resource_group}, {len(teams[resource_group])}')
            current_stage_workers = WorkerCollection(resource_group)
            counter = 0
            for staffInfo in teams[resource_group].values():
                staffInfo: StaffShift = staffInfo
                debug_output(f'add {staffInfo.name} to stage {resource_group}')
                current_stage_workers.add(Worker(id=counter, start=0, end=0, name=staffInfo.name))
                counter += 1
            self.priority_res[resource_group] = MyPriorityResource(self.env, current_stage_workers, resource_group)

        # key is stage, value is priority resource, worker has the name
        for key, value in self.priority_res.items():
            simResource: MyPriorityResource = value
            debug_output(f'{key} has {simResource.get_workers()}')
            # nbCycles = math.ceil(self.nb_days * day_in_seconds / self.rosterDayCycleInSeconds)

            # first sleep until the first cycle begin
            # sleepTimeUntilFirstCycle = day * day_in_seconds + shift_start * seconds_per_hour
            capacity = simResource.capacity
            workers = simResource.get_workers()
            for i in range(capacity):
                worker: Worker = workers[i]
                debug_output(f'worker is {worker} with info {staffRosterDict[worker.name]}')
                staffInfo: StaffShift = staffRosterDict[worker.name]

                # this is shifrt for the first cycle, move on to next cycle
                for currentCycle in range(self.totalCycles):
                    sortedTimekey = sorted(list(staffInfo.shifts.keys()))
                    for idx, timeKey in enumerate(sortedTimekey):
                        shift = staffInfo.shifts[timeKey]

                        debug_output(f'at {timeKey}  shift is {shift}')
                        day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                            shift)
                        temp = df.loc[df.Task == staffInfo.name]
                        originalShiftStartTime = datetime(year, month, day, math.floor(start_hour), start_min)
                        currentShiftStartTime = originalShiftStartTime + timedelta(
                            days=currentCycle * self.rosterCycleDays)

                        currentShiftEndTime = temp.loc[(temp.Start == originalShiftStartTime), 'Finish'].to_list()[
                                                  0] + timedelta(days=currentCycle * self.rosterCycleDays)

                        currentShiftStartTimeFromSimStartInSeconds = (
                                    currentShiftStartTime - SimSetting.start_time_sim).total_seconds()
                        currentShiftEndTimeFromSimStartInSeconds = (
                                    currentShiftEndTime - SimSetting.start_time_sim).total_seconds()
                        if (idx == 0 and currentCycle == 0):
                            currentDayInSeconds = (SimSetting.start_time_sim + timedelta(
                                days=currentCycle * self.rosterCycleDays) - SimSetting.start_time_sim).total_seconds()
                            durationBeforeStart = currentShiftStartTimeFromSimStartInSeconds - currentDayInSeconds
                            debug_output(f'start at {format_time(currentDayInSeconds + durationBeforeStart)} end at '
                                         f'{format_time(currentShiftEndTimeFromSimStartInSeconds)}')
                            self.env.process(self.staff_break_process(simResource, stage=key,
                                                                      worker_id=i, start=currentDayInSeconds,
                                                                      duration=durationBeforeStart))

                        if idx < len(sortedTimekey) - 1:

                            nextShiftStart = staffInfo.shifts[sortedTimekey[idx + 1]]
                            day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                                nextShiftStart)
                            nextShiftStartTime = datetime(year, month, day, math.floor(start_hour), start_min)
                            nextShiftStartTimeFromSimStartInSeconds = (
                                                                              nextShiftStartTime - SimSetting.start_time_sim).total_seconds() + timedelta(
                                days=currentCycle * self.rosterCycleDays).total_seconds()
                            durationBeforeNextShiftStart = nextShiftStartTimeFromSimStartInSeconds - currentShiftEndTimeFromSimStartInSeconds
                            debug_output(
                                f'break at last shift at {format_time(currentShiftEndTimeFromSimStartInSeconds)} end at '
                                f'{format_time(currentShiftEndTimeFromSimStartInSeconds + durationBeforeNextShiftStart)}')
                            self.env.process(self.staff_break_process(simResource, stage=key,
                                                                      worker_id=i,
                                                                      start=currentShiftEndTimeFromSimStartInSeconds,
                                                                      duration=durationBeforeNextShiftStart))
                        elif idx == len(sortedTimekey) - 1 and currentCycle < self.totalCycles - 1:
                            # go back to the beining
                            nextCycleFirstShift = staffInfo.shifts[sortedTimekey[0]]

                            day, end_hour, endt_min, month, start_hour, start_min, year = self.get_datetime_from_roster(
                                nextCycleFirstShift)
                            nextCycleShiftStartTime = datetime(year, month, day, math.floor(start_hour),
                                                               start_min) + timedelta(
                                days=(currentCycle + 1) * self.rosterCycleDays)
                            debug_output(f'next cycle start at {nextCycleShiftStartTime}')
                            durationBeforeNextShiftStart = (
                                        nextCycleShiftStartTime - currentShiftEndTime).total_seconds()
                            debug_output(
                                f'break at last shift at {format_time(currentShiftEndTimeFromSimStartInSeconds)} end at '
                                f'{format_time(currentShiftEndTimeFromSimStartInSeconds + durationBeforeNextShiftStart)}')
                            self.env.process(self.staff_break_process(simResource, stage=key,
                                                                      worker_id=i,
                                                                      start=currentShiftEndTimeFromSimStartInSeconds,
                                                                      duration=durationBeforeNextShiftStart))




#im confused about mould state, mould should have empty, full, (when full it will take 7 days to dry), dry, free again


    def count_demould(self):

        demoulds = []
        for key, mould in self.mould_collection[penrose].items():
            if mould.finish_demould:

                demoulds.append(mould)
        return demoulds


    def count_mould(self, yard):
        nbFull = 0
        fullMoulds = []
        freeMoulds = []
        for key, mould in self.mould_collection[yard].items():
            if mould.mould_state == MouldState.full and mould.location_status == Mould.stable:
                nbFull += 1
                fullMoulds.append(mould)
            if mould.mould_state == MouldState.free:
                freeMoulds.append(mould)
        return nbFull, freeMoulds, fullMoulds








    def run(self):
        self.env.run(until=40 * day_in_seconds)
