import csv

def create_csv(file, header, data):


    f = open(file, 'w', newline='')
    thewriter = csv.writer(f)
    thewriter.writerow(
        header)
    for row in data:
        thewriter.writerow(row)
    f.close()

def create_shops():
    nbShops = 4
    headers = ['shop']
    data = []
    for i in range(nbShops):
        name = 'shop_' + str(i)
        data.append([name])
    create_csv('shop.csv', headers, data)

def create_raw_material():
    nbRaws = 2
    headers = ['raw']
    data = []
    for i in range(nbRaws):
        name = 'rawMaterial_' + str(i)
        data.append([name])
    create_csv('raw.csv', headers, data)

def create_shop_raw_usage():
    nbShops = 4
    headers = ['shop']
    shopdata = []
    shops = []
    productList = ['apple', 'bear', 'milk', 'paper', 'tower', 'chocolate']

    for i in range(nbShops):
        name = productList[i]
        shopdata.append([name])
        shops.append(name)
    create_csv('shop.csv', headers, shopdata)

    nbRaws = 1
    headers = ['raw']
    rawdata = []
    raws = []
    for i in range(nbRaws):
        name = 'rawMaterial_' + str(i)
        rawdata.append([name])
        raws.append(name)
    create_csv('raw.csv', headers, rawdata)

    headers = ['shop', 'raw', 'quantity']
    usageData = []
    for shop in shops:
        for raw in raws:
            usageData.append([shop, raw, 10])

    create_csv('usage.csv', headers, usageData)

    headers = ['shop', 'raw', 'capacity']
    containerData = []
    for shop in shops:
        for raw in raws:
            containerData.append([shop, raw, 10000])
    create_csv('containers.csv', headers, containerData)
    # define containers:


#create_shops()
#create_raw_material()
create_shop_raw_usage()