import collections
import random

from commonParams import *
showRoster = False
density_type = collections.namedtuple('density', 'weights values')
def import_csv(file_path):
    print(f'start loading {file_path}')

    eventlog = pd.read_csv(file_path, sep=',', skipinitialspace=True)
    return eventlog

year = 2022

month = 1
day = 17

SimSetting.setup(backlogWeek=0, rosterWeekBlockSpan=4, year=year, month=month, day=day)

avondale = 'Avondale (AKL)'
penrose = 'Penrose (AKL)'
eastTamaki = 'East Tamaki (AKL)'
silverdale ='Silverdale (AKL)'

location_map = {avondale:'Avondale', penrose: 'Penrose', silverdale:'Silverdale'}
trusckRes = 'truck'
all_yards = [penrose, avondale, silverdale]
end_yards = [avondale, silverdale]
lookaheadStr = 'lookahead'
nbFullAtEndStr = 'NbFullAtEnd'
freeAtEndStr = 'freeAtEnd'
fullMouldStr = 'fullMould'
class EnviroDataDistribution:
    kilo_from_penrose_to_avondale = 18
    kilo_from_penrose_to_silver = 46
    fuel_per_kilo = 0.2
    cost_per_liter = 1.22
    cost_per_kilo = 1.22
    cost_from_pen_to_avon = kilo_from_penrose_to_avondale * 2 * cost_per_kilo
    cost_from_pen_to_silver = kilo_from_penrose_to_silver * 2 * cost_per_kilo
    cost = {}
    cost[penrose, avondale] = cost_from_pen_to_avon
    cost[penrose,silverdale] = cost_from_pen_to_silver
    block_cost = 158#1200 6IB1200STD
    most_common_capacity = 0.42 #1200 6IB1200STD

    @classmethod
    def staffRosterTest(cls):
        # add 2 weeks of roster

        print(SimSetting.rosterWeekDayList)

        print(SimSetting.weekBlockStart)

        staffRosterDict = {}

        offDays = [5, 6]



        for j in range(1):
            name = f'{trusckRes}_{j}'
            staff = StaffShift(name=name, stage=trusckRes)
            for i in range(SimSetting.rosterTotalDays):
                #if i % 7 in offDays: continue
                add_shift(i, staff, SimSetting.roster_start_time + timedelta(days=i, hours=17), 6)
            # staffRosterDict key by staff name, store shift start and finish info

            staffRosterDict[name] = staff





        df = rosterDF(staffRosterDict)
        if showRoster:
            fig = px.timeline(df, x_start="Start", x_end="Finish", y="Task", color="Resource")
            fig.update_yaxes(autorange="reversed")
            fig.show()
        return staffRosterDict, df, SimSetting.rosterWeekDayList

    @classmethod
    def load_density(cls):
        df = import_csv('density.csv')
        cls.density = {}
        locations = df.location.unique()
        #print(locations)
        for l in locations:
            binValues = df[df.location == l].bin.values
            weights = df[df.location == l].w.values
            #print(binValues)
            cls.density[l] = density_type(weights=weights, values=binValues)

    @classmethod
    def draw_sample_collection(self, l):
        collect = random.choices(self.density[l].values, self.density[l].weights, k=1)[0]
        #print(collect)
        return collect

staffRosterDict, df, SimSetting.rosterWeekDayList = EnviroDataDistribution.staffRosterTest()
EnviroDataDistribution.load_density()
EnviroDataDistribution.draw_sample_collection(l='Silverdale')


